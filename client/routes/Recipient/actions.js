import { createActionTypes } from '@/lib/utils/helper';

export const recipientActions = createActionTypes('recipient', [
  'SAVE_RECIPIENT',
]);

export function saveRecipient(obj) {
  return {
    type: recipientActions.SAVE_RECIPIENT,
    payload: {
      ...obj,
    },
  };
}
