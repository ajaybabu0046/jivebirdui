import React from 'react';
import { func } from 'prop-types';
import { connect } from 'react-redux';
import { useInjectReducer } from '../../reducers/injectReducer';
import { saveRecipient } from './actions';
import { recipientReducer } from './reducer';
import RecipientTemp from './component';

const Recipient = (props) => {
  useInjectReducer({
    key: 'recipientReducer',
    reducer: recipientReducer,
  });

  return (
    <>
      <RecipientTemp {...props} />
    </>
  );
};

Recipient.propTypes = {
  dispatch: func.isRequired,
};

Recipient.defaultProps = {
  recipient: {
    name: '',
    email: '',
    phoneNo: '+44',
  },
};

function mapStateToProps(props) {
  return {
    recipient: props.recipientReducer,
  };
}

export default connect(mapStateToProps)(Recipient);
