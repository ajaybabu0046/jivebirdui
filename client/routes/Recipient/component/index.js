import React, { useState } from 'react';
import './style.css';
import JivePresent from '@assets/WrappedPresent.png';
import Mail from '@assets/mail.png';
import Call from '@assets/call.png';
import Message from '@assets/message.png';
import { saveRecipient } from '../actions';
import PhoneInput from 'react-phone-input-2';
import 'react-phone-input-2/lib/style.css';
import { firebaseAnalytics } from '../../../firebaseConfig';
import { logEvent } from 'firebase/analytics';

const RecipientTemp = (props) => {
  const { setCurrentScreen, recipient, dispatch } = props;

  const [email, setEmail] = useState(recipient.email);
  const [phone, setPhone] = useState(recipient.phoneNo);
  const [name, setName] = useState(recipient.name);
  const [errorFields, setErrorMessage] = useState('');
  const [nameerror, setNameError] = useState(false);
  const [emailerror, setEmailError] = useState(false);
  const [phoneerror, setPhoneError] = useState(false);

  const handleNext = () => {
    let errorFound = false;
    // console.log(e.target.value,"phone")
    // const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (name != '') {
      let data = name.trim('');
      if (data.length === 0) {
        errorFound = true;
        logEvent(firebaseAnalytics, 'CardRecipient_InvalidForm');
        setErrorMessage('Name field is mandatory');
        setNameError(true);
        return;
      }
    } else {
      errorFound = true;
      logEvent(firebaseAnalytics, 'CardRecipient_InvalidForm');
      setErrorMessage('Name field is mandatory');
      setNameError(true);
      return;
    }
    console.log('phone', phone);
    console.log('phone[0] ', phone[0] != '+');
    if (phone != '') {
      let data = phone.trim('');
      if (data.length === 0) {
        errorFound = true;
        logEvent(firebaseAnalytics, 'CardRecipient_InvalidForm');
        setErrorMessage('Phone field is mandatory');
        setPhoneError(true);
        return;
      }
      // else if (phone[0] != '+') {
      //   errorFound = true;
      //   logEvent(firebaseAnalytics, 'CardRecipient_InvalidForm');
      //   setErrorMessage("Ensure correct country code is used with a '+'");
      //   setPhoneError(true);
      //   return;
      // }
      else if (data.length < 10) {
        errorFound = true;
        logEvent(firebaseAnalytics, 'CardRecipient_InvalidForm');
        setErrorMessage('Ensure correct phone number is provided.');
        setPhoneError(true);
        return;
      }
    } else {
      errorFound = true;
      logEvent(firebaseAnalytics, 'CardRecipient_InvalidForm');
      setErrorMessage('Phone field is mandatory');
      setPhoneError(true);
      return;
    }

    // if (email) {
    //   if (!email.match(mailformat)) {
    //     errorFound = true;
    //     setErrorMessage('Enter valid e-mail');
    //     setEmailError(true);
    //   }
    // }

    if (!errorFound) {
      logEvent(firebaseAnalytics, 'CardRecipient_ValidForm');
      dispatch(
        saveRecipient({
          name: name,
          phone: '+' + phone,
          email: email,
        }),
      );
      setCurrentScreen(2);
    }
  };
  const addName = (e) => {
    setName(e.target.value);
    setErrorMessage('');
  };
  const addPhone = (e) => {
    console.log('eeee', e);
    // var phoneNoFormat = /^[+]{1,1}[0-9]*$/;

    // if (!e.target.value.match(phoneNoFormat)) return;
    // setPhone(e.target.value);
    setPhone(e);
    setErrorMessage('');
  };
  const addEmail = (e) => {
    setEmail(e.target.value);
    setErrorMessage('');
  };

  return (
    <>
      <div className="container-fluid p-0">
        <div className="d-flex eCardRow btns-div sticky1 mt-3">
          <h1 className="mainHeading head1" style={{ color: '#734385' }}>
            Recipient
          </h1>
          <div className="ml-auto head2">
            <button
              type="button"
              name="next"
              className="btn app-btn mt-2"
              onClick={() => handleNext()}
            >
              Next
            </button>
          </div>
        </div>

        <div className="row">
          <div className="col-lg-6 col-md-12 recipientLeft">
            <div className="d-none d-md-flex mb-3">
              <img src={Message} alt="message" />
              <div className="plusIconDiv">
                <i className="fas fa-plus fa-lg"></i>
              </div>
              <img src={Call} alt="Call" />
              <div className="plusIconDiv">
                <i className="fas fa-plus fa-lg"></i>
              </div>
              <img src={Mail} alt="Mail" />
            </div>
            <h2>
              Your JiveBird will be delivered by a link in a Text and also
              email, if an address is provided.
            </h2>
            <h5>
              The recipient will have the option to listen to your message by
              phone call and online.
            </h5>
            <img className="recpient-jb" src={JivePresent} alt="present" />
          </div>

          <div className="col-lg-6 col-md-12 recipientRight">
            <h1>Recipient details</h1>
            <form>
              <div className="form-group">
                <input
                  type="text"
                  className={`form-control ${nameerror === true && 'error'}`}
                  placeholder="Recipient Name"
                  value={name}
                  autoFocus
                  // required="true"
                  // requiredTxt="Name is required"
                  onChange={(e) => {
                    addName(e);
                    setNameError(false);
                  }}
                  onKeyPress={(event) => {
                    if (event.key === 'Enter') {
                      handleNext();
                    }
                  }}
                />
              </div>
              <div className="labelDiv">
                <label>
                  (Correct country MUST be selected from the dropdown below and
                  correct mobile phone number must be entered, e.g. for UK
                  +447xxxxxxxxx)
                </label>
              </div>
              <div className="form-group">
                <PhoneInput
                  enableSearch
                  disableSearchIcon
                  inputClass="phoneInput"
                  dropdownClass="countryList"
                  country={'uk'}
                  value={phone}
                  className={
                    phoneerror
                      ? 'form-control col-lg-12 error'
                      : 'form-control col-lg-12'
                  }
                  onChange={(e) => addPhone(e)}
                  onKeyPress={(event) => {
                    if (event.key === 'Enter') {
                      handleContactUs();
                    }
                  }}
                  countryCodeEditable={false}
                  searchStyle={{
                    width: '90%',
                  }}
                />
              </div>
              <div className="form-group">
                <input
                  type="email"
                  className="form-control"
                  placeholder="Recipient Email (optional)"
                  value={email}
                  // required="false"
                  onChange={(e) => addEmail(e)}
                  onKeyPress={(event) => {
                    if (event.key === 'Enter') {
                      handleNext();
                    }
                  }}
                />
              </div>
              <div className="col-lg-12 linksColor mt-2 mb-4 d-flex justify-content-center">
                {errorFields}
              </div>
            </form>
          </div>
        </div>
      </div>
    </>
  );
};

export default RecipientTemp;
