import { recipientActions } from './actions';

export const initialState = {
  name: '',
  email: '',
  phoneNo: '+44',
};

export const recipientReducer = (state = initialState, action) => {
  const { SAVE_RECIPIENT } = recipientActions;

  switch (action.type) {
    case SAVE_RECIPIENT:
      return {
        ...state,
        name: action.payload.name,
        email: action.payload.email,
        phoneNo: action.payload.phone,
      };

    default:
      return state;
  }
};
