import React from 'react';
import './style.css';
import { changeTab } from '../../CreateJiveBird/actions';
import addegift from '@assets/addegift.png';
import {
  resetEgiftState,
  changeScreen,
  skipEgift,
} from '../../SelectEgift/actions';

const AddEgiftTemp = (props) => {
  const { dispatch } = props;

  return (
    <div className="container-fluid p-0">
      <div className="d-flex eCardRow btns-div mt-3 sticky">
        {/* <div className="col-lg-6 col-md-12 mt-3"> */}

        <h1 className="mainHeading head1">
          Add <span style={{ color: '#734385' }}>eGift</span>
        </h1>
        <div className="ml-auto head2">
          <button
            type="button"
            className="btn app-btn mt-2"
            onClick={() => {
              dispatch(resetEgiftState({}));
              dispatch(skipEgift());
              dispatch(changeTab({ tabId: 5 }));
            }}
          >
            Skip eGift
          </button>

          <button
            type="button"
            className="btn app-btn mr-3 mt-2"
            onClick={() => dispatch(changeScreen({ Id: 1 }))}
          >
            Add eGift
          </button>
        </div>
      </div>
      <div className="d-flex egiftArea">
        <div className="col-lg-6 leftSection">
          <h2 className="subHeading">
            Complete the perfect JiveBird by adding an eGift voucher from one of
            our premium retail partners. With digital delivery, these can be
            redeemed online for delivery or collection at a chosen time.
          </h2>
        </div>
        <div className="col-lg-6 imgDivaddEgift">
          <img className="commonBird" src={addegift} />
        </div>
      </div>

      {/* </div> */}
      {/* <div className="col-lg-6 d-flex-end col-md-12">
            <div className="form-inline mt-3">
              <button
                type="button"
                className="btn app-btn mr-3 mt-2"
                onClick={() => handleSelect()}>
                Add eGift
              </button>
              <button
                type="button"
                className="btn app-btn mt-2"
                onClick={() => dispatch(changeTab({ tabId: 5 }))}>
                Skip eGift
              </button>
            </div>
          </div> */}
    </div>
  );
};

export default AddEgiftTemp;
