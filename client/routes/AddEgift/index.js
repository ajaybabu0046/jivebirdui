import React from 'react';
import { connect } from 'react-redux';
import { useInjectReducer } from '../../reducers/injectReducer';
import { EgiftReducer } from '../SelectEgift/reducer';
import AddEgiftTemp from './component';
import SelectEgift from '../SelectEgift';
import SelectEgiftValue from '../EgiftCardValue';

const AddEgift = (props) => {
  //0: Initial Screen, 1: Select e gift, 2: Select e gift value
  const { vendorlist } = props;
  const { eGiftScreen } = vendorlist;
  useInjectReducer({
    key: 'EgiftReducer',
    reducer: EgiftReducer,
  });

  return (
    <>
      {eGiftScreen === 0 && <AddEgiftTemp {...props} />}
      {eGiftScreen === 1 && <SelectEgift {...props} />}
      {eGiftScreen === 2 && <SelectEgiftValue {...props} />}
    </>
  );
};

AddEgift.defaultProps = {
  vendorlist: {
    categorySelected: false,
    eGiftScreen: 0,
  },
};

function mapStateToProps(props) {
  return {
    vendorlist: props.EgiftReducer,
  };
}

export default connect(mapStateToProps)(AddEgift);
