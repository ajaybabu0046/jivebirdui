import React, { useState, useEffect } from 'react';
import './style.css';
import { loginUserVerify, resetState } from '../actions';
import swal from '@sweetalert/with-react';
import { changeTab } from '../../CreateJiveBird/actions';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEye } from '@fortawesome/free-solid-svg-icons';
import { firebaseAnalytics } from '../../../firebaseConfig';
import { logEvent } from 'firebase/analytics';

import axios from 'axios';
const eye = <FontAwesomeIcon icon={faEye} />;

const LoginTemp = (props) => {
  const {
    loginData,
    dispatch,
    setCurrentScreen,
    setLoginBtnToggle,
    loginBtnToggle,
    JiveBirdReducer,
  } = props;

  const [Toggle, setToggle] = useState(false);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [errorMessage, setErrorMessage] = useState('');

  const [passwordShown, setPasswordShown] = useState(false);
  const [resendeMail, setresendMail] = useState('');

  const handleToggle = () => {
    setToggle(!Toggle);
  };
  const addSendMail = (e) => {
    //  setErrorMessage('');
    setresendMail(e.target.value);
  };

  const ResendHandle = () => {
    // console.log(resendeMail,"resendd")
    let errorFound = false;
    if (!resendeMail) {
      errorFound = true;
      setErrorMessage('Empty Email Field');
      return;
    }
    var config = {
      method: 'post',
      url: `${process.env.BASE_API_PATH}/resend-verification-email/`,
      headers: {
        'Content-Type': 'application/json',
      },
      data: { login: resendeMail },
    };

    axios(config)
      .then((response) => {
        swal(<p>{response}</p>).then((willSuccess) => {
          if (willSuccess) {
            setresendMail('');
            let b1 = document.getElementById('ResendEmailModal');
            b1.click();
          }
        });
      })
      .catch((response) => {
        console.log('Error Resp', response);
      });
  };

  const SendMaleHandle = () => {
    // console.log(resendeMail,"resendd")
    let errorFound = false;
    if (!resendeMail) {
      errorFound = true;
      setErrorMessage('Empty Email Field');
      return;
    }
    var config = {
      method: 'post',
      url: `${process.env.BASE_API_PATH}/accounts/send-reset-password-link/`,
      headers: {
        'Content-Type': 'application/json',
      },
      data: {
        login: resendeMail,
      },
    };

    axios(config)
      .then((response) => {
        swal(<p>{response.data.detail}</p>).then((willSuccess) => {
          if (willSuccess) {
            setresendMail('');
            let b1 = document.getElementById('sendEmailModal');
            b1.click();
          }
        });
      })
      .catch((response) => {
        console.log('Error Resp', response);
      });
  };

  useEffect(() => {
    if (JiveBirdReducer && JiveBirdReducer.MyAccount != undefined) {
      if (
        loginData &&
        loginData.response &&
        loginData.response.detail == 'Login successful'
      ) {
        localStorage.setItem('authToken', loginData.response.token);

        swal(
          <div>
            <p>Login successful.</p>
          </div>,
          {
            icon: 'success',
            closeOnEsc: false,
            closeOnClickOutside: false,
          },
        ).then((willSuccess) => {
          if (willSuccess) {
            window.location.reload();
          }
        });
      }
    } else {
      if (
        loginData &&
        loginData.response &&
        loginData.response.detail == 'Login successful'
      ) {
        localStorage.setItem('authToken', loginData.response.token);
        swal(
          <div>
            <p>Login successful.</p>
          </div>,
          {
            icon: 'success',
            closeOnEsc: false,
            closeOnClickOutside: false,
          },
        ).then((willSuccess) => {
          if (willSuccess) {
            dispatch(changeTab({ tabId: 5, logIn: 'Login' }));
          }
          // else{
          //   dispatch(changeTab({ tabId: 5,logIn:"Login" }));
          // }
        });
      }
    }

    if (loginData && loginData.response && loginData.response.detail) {
      setErrorMessage(loginData.response.detail);
    }
  }, [loginData]);
  // console.log(loginData && loginData.response && loginData.response.detail,"inner html page")

  const loginUser = () => {
    dispatch(resetState());
    let errorFound = false;
    // var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    if (!email) {
      errorFound = true;
      setErrorMessage('Empty Email Field');
      return;
    }
    // else if (!email.match(mailformat)) {
    //   errorFound = true;
    //   setErrorMessage('Invalid Email Format');
    //   return;
    // }

    if (!password) {
      errorFound = true;
      setErrorMessage('Empty Password Field');
      return;
    }

    if (errorFound) {
      dispatch(resetState());
      return;
    }

    if (!errorFound) {
      const emailId = email.toLocaleLowerCase();

      dispatch(
        loginUserVerify({
          login: emailId,
          password: password,
        }),
      );
    }
  };

  const addEmail = (e) => {
    setErrorMessage('');
    setEmail(e.target.value);
    dispatch(resetState());
  };

  const addPassword = (e) => {
    setErrorMessage('');
    setPassword(e.target.value);
    dispatch(resetState());
  };
  const togglePasswordVisiblity = () => {
    setPasswordShown(passwordShown ? false : true);
  };
  const handleLoginToggle = (data) => {
    if (data == 'login') {
      setLoginBtnToggle(true);
      setCurrentScreen(1);
    } else {
      setLoginBtnToggle(false);
      setCurrentScreen(JiveBirdReducer && JiveBirdReducer.MyAccount ? 3 : 0);
    }
  };

  return (
    <div>
      <div className="container Logincontainer  d-flex justify-content-center align-items-center ">
        <div className="LoginBody mt-5">
          <div className="row">
            <div className="col myToggalDiv">
              <span
                role="button"
                onClick={() => handleLoginToggle('login')}
                className={loginBtnToggle ? 'login' : 'default'}
              >
                Login
              </span>
              <span
                role="button"
                onClick={() => handleLoginToggle('regi')}
                className={loginBtnToggle ? 'default' : 'regi'}
              >
                New User
              </span>
            </div>
          </div>

          <form className="px-5">
            <div className="row">
              <div className="col">
                <h3 className="textHeader">Click on New User or Login Below</h3>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-12 mt-3">
                <input
                  type="email"
                  name="email"
                  className="form-control emailicon"
                  placeholder="E-mail"
                  // value={email}
                  onChange={(e) => addEmail(e)}
                  onKeyPress={(event) => {
                    if (event.key === 'Enter') {
                      loginUser();
                    }
                  }}
                />
              </div>
            </div>
            <div className="row">
              <div className="col-lg-12 mt-3">
                <input
                  // type="password"
                  type={passwordShown ? 'text' : 'password'}
                  name="password"
                  className="form-control passwordicon"
                  placeholder="Password"
                  // value={password}
                  onChange={(e) => addPassword(e)}
                  onKeyPress={(event) => {
                    if (event.key === 'Enter') {
                      loginUser();
                    }
                  }}
                />
                <div>
                  <i className="eyeIcon" onClick={togglePasswordVisiblity}>
                    {passwordShown ? eye : <i className="fas fa-eye-slash"></i>}
                  </i>
                </div>
              </div>
              <div className="form-inline" style={{ width: '100%' }}>
                <div className="col-lg-6 mt-4">
                  <div className="passwordreset">
                    <a
                      href="#"
                      data-toggle="modal"
                      data-target="#sendMailModal"
                    >
                      Forgot Your Password?
                    </a>{' '}
                    <br />
                  </div>
                  <div className="resend">
                    <a href="#" data-toggle="modal" data-target="#resendModal">
                      <u>Resend Confirmation Mail</u>
                    </a>
                  </div>
                </div>
                <div className="col-lg-6 ">
                  <button
                    type="button"
                    className="btn btnSubmitlogin"
                    onClick={() => loginUser()}
                  >
                    Login
                  </button>
                </div>
              </div>
              <div className="col-lg-12 linksColor mt-2 mb-4 d-flex justify-content-center">
                {errorMessage}
              </div>
            </div>
          </form>
        </div>
      </div>

      <div
        className="modal fade"
        id="resendModal"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content">
            <div className="modal-header popUpHeading">
              <h5 className="modal-title" id="exampleModalLongTitle">
                Resend Confirmation Mail
              </h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body mt-3">
              <input
                type="email"
                name="email"
                className="form-control swalEmailIcon "
                placeholder="E-mail Address"
                onChange={(e) => {
                  setresendMail(e.target.value);
                }}
                value={resendeMail}
                onKeyPress={(event) => {
                  if (event.key === 'Enter') {
                    ResendHandle();
                  }
                }}
              />
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-secondary btnSendmail"
                id="ResendEmailModal"
                data-dismiss="modal"
              >
                Close
              </button>
              <button
                type="button"
                className="btn btn-primary btnSendmail"
                onClick={() => ResendHandle()}
              >
                Resend
              </button>
            </div>
          </div>
        </div>
      </div>
      <div
        className="modal fade"
        id="sendMailModal"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content">
            <div className="modal-header popUpHeading">
              <h5 className="modal-title " id="exampleModalLongTitle">
                Forgot Your Password?
              </h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body mt-3">
              <input
                type="email"
                name="email"
                className="form-control swalEmailIcon emailDiv"
                placeholder="E-mail Address"
                value={resendeMail}
                onChange={(e) => {
                  setresendMail(e.target.value);
                }}
                // value={resendeMail}
                onKeyPress={(event) => {
                  if (event.key === 'Enter') {
                    SendMaleHandle();
                  }
                }}
              />
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-secondary btnSendmail"
                id="sendEmailModal"
                data-dismiss="modal"
              >
                Close
              </button>
              <button
                type="button"
                className="btn btn-primary btnSendmail"
                onClick={() => SendMaleHandle()}
              >
                Remind
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default LoginTemp;
