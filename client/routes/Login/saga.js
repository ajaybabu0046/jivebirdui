import { call, put, takeLatest } from 'redux-saga/effects';
import { apiResource } from '@/lib/utils/api';
import { LoginAction, loginSuccess, loginFailed } from './actions';
function* loginUserVerify(dObj) {
  try {
    const url = '/accounts/login/';
    const res = yield call(apiResource.post, url, dObj.payload);

    if (res.detail == 'Login successful') {
      yield put(
        loginSuccess({
          loginData: res,
        }),
      );
    } else {
      yield put(
        loginFailed({
          loginData: res,
        }),
      );
    }
  } catch (e) {
    yield put(
      loginFailed({
        loginData: JSON.parse(e.data),
      }),
    );
  }
}
function* loginSaga() {
  yield takeLatest(LoginAction.VERIFY_LOGIN_USER, loginUserVerify);
}
export default loginSaga;
