import { createActionTypes } from '@/lib/utils/helper';

export const LoginAction = createActionTypes('Login', [
  'VERIFY_LOGIN_USER',
  'LOGIN_SUCCESS',
  'LOGIN_FAILED',
  'RESET_STATE',
]);

export function loginUserVerify(obj) {
  return {
    type: LoginAction.VERIFY_LOGIN_USER,
    payload: {
      ...obj,
    },
  };
}

export function loginSuccess(obj) {
  return {
    type: LoginAction.LOGIN_SUCCESS,
    payload: {
      ...obj,
    },
  };
}

export function loginFailed(obj) {
  return {
    type: LoginAction.LOGIN_FAILED,
    payload: {
      ...obj,
    },
  };
}

export function resetState() {
  return {
    type: LoginAction.RESET_STATE,
  };
}
