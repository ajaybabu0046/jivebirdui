import React, { useState } from 'react';
import LoginTemp from './component';
import { shape, func } from 'prop-types';
import { connect } from 'react-redux';
import { useInjectReducer } from '../../reducers/injectReducer';
import { useInjectSaga } from '../../saga/injectSaga';
import loginSaga from './saga';
import { loginReducer } from './reducer';
import { chooseSongReducer } from '../SongsList/reducer';
import { recipientReducer } from '../Recipient/reducer';
import { getCardCategoriesReducer } from '../SelectEcard/reducer';
import { recordingReducer } from '../RecordMessage/reducer';
import { EgiftReducer } from '../SelectEgift/reducer';
import { createJiveBirdReducer } from '../CreateJiveBird/reducer';
import { getPhotoCardCategoriesReducer } from '../EcardWithPhoto/reducer';
import { sendEcardReducer } from '../SendEcard/reducer';

const Login = (props) => {
  const { setCurrentScreen, setLoginBtnToggle, loginBtnToggle } = props;

  useInjectReducer({ key: 'loginReducer', reducer: loginReducer });
  useInjectReducer({
    key: 'chooseSongReducer',
    reducer: chooseSongReducer,
  });
  useInjectReducer({
    key: 'recipientReducer',
    reducer: recipientReducer,
  });
  useInjectReducer({
    key: 'getCardCategoriesReducer',
    reducer: getCardCategoriesReducer,
  });
  useInjectReducer({
    key: 'EgiftReducer',
    reducer: EgiftReducer,
  });
  useInjectReducer({
    key: 'recordingReducer',
    reducer: recordingReducer,
  });
  useInjectReducer({
    key: 'createJiveBirdReducer',
    reducer: createJiveBirdReducer,
  });
  useInjectReducer({
    key: 'getPhotoCardCategoriesReducer',
    reducer: getPhotoCardCategoriesReducer,
  });
  useInjectReducer({
    key: 'sendEcardReducer',
    reducer: sendEcardReducer,
  });

  useInjectSaga({ key: 'loginSaga', saga: loginSaga });

  return (
    <div>
      <LoginTemp
        {...props}
        setCurrentScreen={setCurrentScreen}
        setLoginBtnToggle={setLoginBtnToggle}
        loginBtnToggle={loginBtnToggle}
      />
    </div>
  );
};

Login.propTypes = {
  loginData: shape({}).isRequired,
  dispatch: func.isRequired,
};

Login.defaultProps = {
  loginData: {
    loading: false,
  },
};

function mapStateToProps(props) {
  return {
    loginData: props.loginReducer,
    songDetails: props.chooseSongReducer,
    recipient: props.recipientReducer,
    card: props.getCardCategoriesReducer,
    eGift: props.EgiftReducer,
    voiceMsg: props.recordingReducer,
    JiveBirdReducer: props.createJiveBirdReducer,
    photoCard: props.getPhotoCardCategoriesReducer,
    scheduledTime: props.sendEcardReducer,
  };
}

export default connect(mapStateToProps)(Login);
