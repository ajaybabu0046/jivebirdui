import { LoginAction } from './actions';

export const initialState = {
  loading: false,

  message: '',
  response: {},
};

export const loginReducer = (state = initialState, action) => {
  const { VERIFY_LOGIN_USER, LOGIN_SUCCESS, LOGIN_FAILED, RESET_STATE } =
    LoginAction;

  const { payload } = action;

  switch (action.type) {
    case VERIFY_LOGIN_USER:
      return {
        ...state,
        loading: true,

        // message: "",
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        loading: false,
        response: action.payload.loginData,
      };
    case LOGIN_FAILED:
      return {
        ...state,
        loading: false,

        response: action.payload.loginData,
      };
    case RESET_STATE:
      return {
        ...state,
        loading: false,
        response: '',
        message: '',
      };

    default:
      return state;
  }
};
