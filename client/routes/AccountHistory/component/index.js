import React, { useEffect, useState } from 'react';
import MyAccountToggling from '../../../components/MyAccountToggling';
import './style.css';
import Loader from 'react-loader-spinner';
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';
import Moment from 'moment';

const AccountHisList = (props) => {
  const [responseData, setResponseData] = useState();
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    fetch(`${process.env.BASE_API_PATH}/history/`, {
      method: 'GET',
      headers: {
        Authorization: `Token ${localStorage.getItem('authToken')}`,
      },
    })
      .then((response) => response.json())
      .then((response) => {
        setResponseData(response);
        setLoading(false);

        return response;
      })
      .catch((err) => {
        setLoading(false);
        return err;
      });
  }, []);

  return (
    <>
      <MyAccountToggling {...props} />
      {loading ? (
        <div className="loaderContainer">
          <Loader
            type="Bars"
            color="#734385"
            height={100}
            width={100}
            radius={30}
          />
        </div>
      ) : (
        <div>
          <div className="container-fluid historyCont mt-4">
            <h4>Sent JiveBirds</h4>
            <div className="row">
              <div className="col">
                <div className="table-responsive myTable">
                  <table className="table table-hover table-bordered">
                    <thead>
                      <tr>
                        <th scope="col">Date & Time</th>
                        <th scope="col">Number</th>
                        <th scope="col">Name</th>
                      </tr>
                    </thead>
                    <tbody>
                      {responseData &&
                        responseData.length > 0 &&
                        responseData.map((data) => (
                          <tr>
                            <td className="dt">
                              {Moment(
                                data.created_date != null && data.created_date,
                              ).format('yyyy-MM-DD')}{' '}
                              &nbsp;{' '}
                              {Moment(
                                data.created_date != null && data.created_date,
                              ).format('hh:mm')}
                            </td>
                            <td>
                              {data.receipt_phone != null && data.receipt_phone}
                            </td>
                            <td>
                              {data.receipt_name != null && data.receipt_name}
                            </td>
                          </tr>
                        ))}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default AccountHisList;
