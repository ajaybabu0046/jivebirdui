import { createActionTypes } from '@/lib/utils/helper';

export const sendEcardActions = createActionTypes('recipient', [
  'SAVE_ECARD_TIME',
]);

export function saveCardTime(obj) {
  return {
    type: sendEcardActions.SAVE_ECARD_TIME,
    payload: {
      ...obj,
    },
  };
}
