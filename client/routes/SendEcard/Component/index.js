import 'date-fns';
import React, { useState } from 'react';
import './style.css';
import { saveCardTime } from '../actions';
import { DateTime } from 'luxon';
import { Modal } from 'react-bootstrap';
import { Done } from '@material-ui/icons';
import sendecard from '@assets/sendecard.png';
import moment from 'moment';
import swal from '@sweetalert/with-react';

const SendEcardTemp = (props) => {
  const { setCurrentScreen, dispatch } = props;

  let ssss = moment().add(30, 'minutes');
  let ttt = ssss._d.getHours();
  let ttt2 = ssss._d.getMinutes();
  let minTime = ttt + ':' + ttt2;

  const [open, setOpen] = useState(false);
  const [selectedDate, setSelectedDate] = useState(DateTime.local());
  const [normalDate, setNormalDate] = useState(moment().format('YYYY-MM-DD'));
  const [selectedTime, setSelectedTime] = useState(DateTime.local());
  const [modal2Visible, setModal2Visible] = useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleSchedule = (type) => {
    if (normalDate < moment().format('YYYY-MM-DD')) {
      swal(
        <p>
          You can only schedule your JiveBird for current and future dates.
        </p>,
      );
    } else if (
      normalDate == moment().format('YYYY-MM-DD') &&
      selectedTime < minTime
    ) {
      swal(<p>Please select a time atleast 30 minutes ahead.</p>);
    } else {
      dispatch(
        saveCardTime({
          date: type == 'schedule' ? selectedDate : '',
          time: type == 'schedule' ? selectedTime : '',
          types: type,
        }),
      );
      setCurrentScreen(1);
    }
  };

  const handleDate = (e) => {
    let date = e.target.value;
    setNormalDate(date);
    let luxonDate = DateTime.fromISO(date);
    setSelectedDate(luxonDate);
  };

  const handleTime = (e) => {
    setSelectedTime(e.target.value);
  };

  return modal2Visible === false ? (
    <div className="container-fluid mb-5">
      <div className="row eCardRow btns-div mt-3">
        <h1 className="mainHeading head1">
          When to<span> Deliver</span>
        </h1>
        <div className="ml-auto head2">
          <button
            className="btn app-btn mt-2 button2"
            style={{ cursor: 'pointer' }}
            onClick={() => handleSchedule('immediately')}
          >
            ASAP
          </button>
          <button
            className="btn app-btn mr-3 mt-2 button1"
            style={{ cursor: 'pointer' }}
            onClick={() => handleOpen()}
          >
            Schedule
          </button>
        </div>
      </div>
      <div className="row">
        <div className="col-lg-6 para mt-4 pl-0">
          <h2 className="subHeading">
            Chose when your JiveBird will be delivered, ASAP or schedule it to
            arrive on the special day.
          </h2>
        </div>
        <div className="col-lg-6 imgDivsendecard ">
          <img className="commonBird" src={sendecard} />
        </div>
      </div>

      {/* Modal Start */}
      <Modal
        show={open}
        onHide={() => handleClose()}
        size="lg"
        dialogClassName="scheduleDT"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Body style={{ backgroundColor: '#fff' }}>
          <div className="popUpHeading">
            <h1>Tap on Date or Time to Change</h1>
          </div>
          <div className="popUpBody py-3">
            <div className="d-flex dandT">
              <h1 className="">
                <label for="start">Date:</label>
                <input
                  type="date"
                  className="ml-2"
                  id="start"
                  name="trip-start"
                  defaultValue={normalDate}
                  // value={normalDate}
                  onChange={(e) => handleDate(e)}
                  min={moment().format('YYYY-MM-DD')}
                  style={{
                    color: '#df6329',
                    border: 'transparent',
                  }}
                ></input>
              </h1>

              <h1 className="mb-0">
                <label for="time">Time:</label>
                <input
                  type="time"
                  className="ml-2"
                  id="time"
                  name="time"
                  defaultValue={minTime}
                  // value={selectedTime}
                  onChange={(e) => handleTime(e)}
                  // min={minTime}
                  style={{ color: '#df6329', border: 'transparent' }}
                ></input>
                <br />
              </h1>
              {/* <span className={'validity'}>
                <small>Please select a time atleast 30 minutes ahead.</small>
              </span> */}
            </div>

            <div className="">
              <button
                type="button"
                className="btn btnDone"
                onClick={() => handleSchedule('schedule')}
              >
                Done
              </button>
            </div>
          </div>
        </Modal.Body>
      </Modal>

      {/* End */}
    </div>
  ) : (
    <div className="">
      {modal2visible && (
        <Done setModal2visible={setModal2visible} setOpen={setOpen} />
      )}
    </div>
  );
};

export default SendEcardTemp;
