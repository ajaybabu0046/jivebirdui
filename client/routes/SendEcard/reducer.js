import { sendEcardActions } from './actions';

export const initialState = {
  date: '',
  time: '',
  types: '',
};

export const sendEcardReducer = (state = initialState, action) => {
  const { SAVE_ECARD_TIME } = sendEcardActions;

  switch (action.type) {
    case SAVE_ECARD_TIME:
      return {
        ...state,
        date: action.payload.date,
        time: action.payload.time,
        types: action.payload.types,
      };

    default:
      return state;
  }
};
