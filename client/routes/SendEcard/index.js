import React, { useState } from 'react';
import SendEcardTemp from './Component';
import Recipient from '../Recipient';
import JBSummary from '../JiveBirdSummary';
import { func } from 'prop-types';
import { connect } from 'react-redux';
import { useInjectReducer } from '../../reducers/injectReducer';
import { sendEcardReducer } from './reducer';
import { createJiveBirdReducer } from '../CreateJiveBird/reducer';

const SendEcard = (props) => {
  useInjectReducer({
    key: 'sendEcardReducer',
    reducer: sendEcardReducer,
  });
  useInjectReducer({
    key: 'createJiveBirdReducer',
    reducer: createJiveBirdReducer,
  });
  // console.log(props,"LoginProps")
  const { JiveBirdReducer } = props;
  const [currentScreen, setCurrentScreen] = useState(
    JiveBirdReducer && JiveBirdReducer.loginPage === undefined ? 0 : 2,
  );

  return (
    <>
      {currentScreen === 0 && (
        <SendEcardTemp {...props} setCurrentScreen={setCurrentScreen} />
      )}
      {currentScreen === 1 && (
        <Recipient {...props} setCurrentScreen={setCurrentScreen} />
      )}
      {currentScreen === 2 && (
        <JBSummary {...props} setCurrentScreen={setCurrentScreen} />
      )}
    </>
  );
};

SendEcard.propTypes = {
  dispatch: func.isRequired,
};

SendEcard.defaultProps = {
  schedule: {
    date: '',
    time: '',
    types: '',
  },
};

function mapStateToProps(props) {
  return {
    schedule: props.sendEcardReducer,
    JiveBirdReducer: props.createJiveBirdReducer,
  };
}

export default connect(mapStateToProps)(SendEcard);
