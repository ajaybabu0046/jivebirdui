import React from 'react';
import './style.css';

const FaqTemp = () => {
  return (
    <>
      <div className="container-fluid main">
        <h1 className="mt-5 ml-2 heading">FAQ</h1>
        <div className="question">
          <h3>How Do I Record My Message?</h3>

          <p>
            Tap RECORD. The message you record can be up to 30 seconds in
            duration. You can replay your personal voice message and re record
            it as many times as you like. Your chosen music will play at the end
            of your recording. Tap NEXT when you are happy with your message and
            chosen music. NOTE. The first time that you do this you will be
            asked to give permission to access your microphone.
          </p>
          <br />

          <h3>If I want to send an ECARD how do I choose the card?</h3>

          <p>
            {' '}
            Tap ECARD or ECARD WITH PHOTO to enter the respective library. Find
            the card you want by scrolling up and down. Tap the chosen card to
            select it. If you have chosen an ECARD WITH PHOTO you must then
            choose either your Camera to take a picture or Selfie or select a
            photo from available memory (Files,Gallery,Cloud etc.).
            <br />
            NOTE. The first time that you do this you will be asked to give
            permission to access your Camera/Files etc.
          </p>
          <br />

          <h3>How do I enter my text message</h3>

          <p>
            Tap just above the RE SELECT and NEXT buttons. Here you can enter
            upto 3 lines of text.To close the keypad on Android tap back/down
            button, On iOS tap Done.
          </p>
          <br />

          <h3>How do I add an eGift</h3>

          <p>
            Tap the add eGift button and scroll up and down the displayed
            supplies. Then tap the card of your chosen supplier. Tick the box of
            the required eGift VALUE. When you are happy with your selection tap
            NEXT.
          </p>
          <br />

          <h3>
            {' '}
            How do I send my JiveBird and can I send it automatically at a later
            date?
          </h3>

          <p>
            {' '}
            Tap on IMMEDIATELY if you wish to send it now, or tap SCHEDULE, for
            a later date and time. If you have chosen SCHEDULE, tap on Date or
            Time and choose the required Date and Time. When the required date
            and time is displayed tap DONE
          </p>
          <br />

          <h3> How do I choose my recipient?</h3>

          <p>
            You can either choose your recipient(s) from your device Contacts
            directory or enter their details directly using your device
            keypad/keyboard.
          </p>
          <br />

          <h3> Can I make a change to my completed JiveBird?</h3>

          <p>
            At the Checkout screen you can review and change any of your
            selections by tapping the appropriate Edit/Re-Select button. When
            you are completely satisfied with your selection, tap Send JiveBird.
          </p>
          <br />

          <h3> How do I pay for my JiveBird or eGift Voucher?</h3>
          <p>
            {' '}
            If you have no JiveBird credit or you have chosen an eGift, on
            tapping the Send JiveBird button, you will be directed to our secure
            payment center. Simply enter your credit card details, accept the
            Payment/supplier Terms @Conditions and press the Confirm & Pay
            button. Following successful payment you will be redirected back to
            the JiveBird app. If you are using an Apple device you will receive
            a popup where you must ton Open to return to the JiveBird app.
          </p>
          <br />

          <h3>My JiveBird app is not responsive?</h3>
          <p>
            {' '}
            There could be a Wi-Fi and/or mobile phone data connection problem.
            Please ensure that you have good Wi-Fi or at least a 4g mobile phone
            data connection.
          </p>
          <br />

          <h3>How do I manage my account?</h3>

          <p>
            {' '}
            First press the 3 dots in the top left hand corner of the screen and
            then press My Account. You can Login or Register here and if logged
            in, see details of your sent JiveBirds, Personal Information and how
            many JiveBirds you have in credit.
          </p>
          <br />

          <h3> How do I Log-out</h3>

          <p>
            First press the 3 dots in the top left hand corner of the screen and
            then press Log-out
          </p>
          <br />

          <h3>Can I use JiveBird on my tablet?</h3>

          <p>
            Yes, but it should be used in Portrait mode. You can login with the
            same account details as your phone.
          </p>
          <br />

          <h3>How will my friend receive my JiveBird Greeting?</h3>
          <p>
            They will first receive a text message from JiveBird telling them
            you have bought them a JiveBird and providing them with a web link
            where they can see any eCard/Photo eCard and choose how they wish to
            receive your JiveBird. If they choose a telephone call, they will
            receive it shortly afterwards from the JiveBird number 07451289603.
            [Please note this number is for outgoing calls only] When they
            receive the call they will ho asked to either pross 1 on their
            Devansh Richharia, Now be asked to either press I on their keypad or
            speak the word 'continue'. The JiveBird Greeting will then be
            played. A second text will then be received, confirming JiveBird
            delivery and providing again the web link to listen to the JiveBird
            Greeting. If they wish they can choose to listen to your JiveBird
            immediately via the web. Due to circumstances beyond our control the
            sound quality of the JiveBird call will vary depending on the
            delivery phone/mobile network. The web link can always be used to
            hear the JiveBird in the best sound quality. If you have provided an
            email address an email will also be sent, also providing the
            JiveBird web link. If they do not see the email they should check
            their SPAM box on their device and email service provider server.
          </p>
          <br />

          <h3>How many times can they listen to my JiveBird greeting?</h3>
          <p>
            Your friend can listen to your JiveBird greeting via the web as many
            times as they wish using the link in the text or email. Additionally
            they can download this link to their device download folder for safe
            keeping on all devices except iOS. At this time iOS devices do not
            allow downloads. A JiveBird phone call can only be listened to once.
          </p>
          <br />

          <h3> How will my friend receive my e Gift?</h3>
          <p>
            {' '}
            A separate text will be sent from eGift JB providing the eGift web
            link. If you have also supplied an email address for your friend the
            JiveBird email will also inform them that you have sent them an
            eGift and to look in their messages for a text from eGift JB
          </p>
          <br />

          <h3>
            How will I be informed about the progress of my JiveBird and eGift?
          </h3>
          <p>
            A 'Sender report' will be emailed to you as soon as the JiveBird is
            sent. This will detail the destination information and contents of
            the JiveBird and if an eGift was sent.As soon as your friend hears
            your JiveBird Greeting, either from a) the JiveBird Phone Call or b)
            using the web link in the text or if provided, email, a further
            report will be sent. This will again detail the destination
            information and contents of the JiveBird and additionally show how
            the JiveBird was first listened to.
          </p>
          <br />

          <h3>
            {' '}
            What happens if the recipient does not have a signal, the phone is
            busy or turned off
          </h3>

          <p>
            {' '}
            The recipient will receive any outstanding JiveBird or eCard text as
            soon as the recipient's phone is free and connected to their phone
            network.
          </p>
          <br />

          <h3> How do I remove my data from JiveBird</h3>
          <p>
            If you would like to have any data related to you or your account
            removed.Please use the Feedback link on the app and provide your
            email address and request deletion of all your data.Alternatively
            email us at info @jivebird.com
          </p>
          <br />

          <h3> How do I choose the music/song that I want?</h3>

          <p>
            To display a list of music/songs which can be scrolled, tap one of
            the listed categories, or type in the text box the title of the
            required music/song, Genre, Occasion, or artist's names originally
            associated with the particular song.Tap the triangle play button to
            play the track.The track you hear will usually be a 'cover' of the
            original song recorded by artists other than the original
            artist.When you have chosen your track tap NEXT.Our catalogue
            contains many thousands of hit and well-known songs.If for any
            reason you cannot find the track you want, you may contact us using
            the JiveBird app, contact form and we will endeavour to locate it
            for you.The track plays for 30 seconds.
          </p>
          <br />

          <h3> How do I register and create a JiveBird account?</h3>

          <p>
            Now At the Account screen tap the Register tab and then complete the
            required information not forgetting to confirm you have read the
            Terms & Conditions.The password must be at least 8 characters long
            and must not be entirely numerical.Also it should not contain your
            first and last name or email address.After successful submission of
            this information, you will be sent an email from JiveBird,
            containing a link for you to confirm your email address.If you do
            not see this email from JiveBird, please check in
            yourSpam/Junk/Promotions/Social folders.If you can not find the
            JiveBird email, please use the Feedback link on the app and provide
            your email address and request deletion of your account, so that you
            can register again. Alternatively email us at info @jivebird.com
          </p>
          <br />
        </div>
      </div>
    </>
  );
};

export default FaqTemp;
