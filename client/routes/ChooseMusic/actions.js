import { createActionTypes } from '@/lib/utils/helper';

export const chooseMusicCategoryActions = createActionTypes(
  'chooseMusicCategory',
  [
    'CHOOSE_MUSIC_CATEGORY',
    'RESET_STATE',

    'GET_MUSIC_CATEGORY',
    'GET_MUSIC_CATEGORY_SUCCESS',
    'GET_MUSIC_CATEGORY_FAIL',
  ],
);

export function chooseMusicCategory(obj) {
  return {
    type: chooseMusicCategoryActions.CHOOSE_MUSIC_CATEGORY,
    payload: {
      ...obj,
    },
  };
}

export function resetMusicState(obj) {
  return {
    type: chooseMusicCategoryActions.RESET_STATE,
    payload: {
      ...obj,
    },
  };
}

export const getMusicCategoryList = () => ({
  type: chooseMusicCategoryActions.GET_MUSIC_CATEGORY,
});

export const getMusicCategorySuccess = (obj) => ({
  type: chooseMusicCategoryActions.GET_MUSIC_CATEGORY_SUCCESS,
  payload: {
    ...obj,
  },
});

export const getMusicCategoryFailure = (obj) => ({
  type: chooseMusicCategoryActions.GET_MUSIC_CATEGORY_FAIL,
  payload: {
    ...obj,
  },
});
