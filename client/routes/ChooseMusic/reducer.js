import { chooseMusicCategoryActions } from './actions';
import { songsListActions } from '../SongsList/actions';

export const initialState = {
  categoryListLoading: true,
  categorySelected: false,
  category: '',
  songCategoryList: [],
  songSelected: false,
  song: '',
};

export const chooseMusicReducer = (state = initialState, action) => {
  const {
    CHOOSE_MUSIC_CATEGORY,
    RESET_STATE,
    GET_MUSIC_CATEGORY,
    GET_MUSIC_CATEGORY_SUCCESS,
    GET_MUSIC_CATEGORY_FAIL,
  } = chooseMusicCategoryActions;
  const { RESELECT_CATEGORY, CHOOSE_SONG } = songsListActions;

  switch (action.type) {
    // To fetch the music category
    case GET_MUSIC_CATEGORY:
      return {
        ...state,
      };
    case GET_MUSIC_CATEGORY_SUCCESS:
      let sortedByPopularity = action.payload.Data.sort((a, b) => {
        var p1 = a.popularity,
          p2 = b.popularity;

        if (p1 < p2) return 1;
        if (p1 > p2) return -1;
        return 0;
      });

      return {
        ...state,
        categoryListLoading: false,
        songCategoryList: sortedByPopularity,
        success: 1,
      };
    case GET_MUSIC_CATEGORY_FAIL:
      return {
        ...state,
        categoryListLoading: false,
        songCategoryList: action.payload.Data,
        success: 0,
      };

    case CHOOSE_MUSIC_CATEGORY:
      return {
        ...state,
        categorySelected: true,
        category: action.payload.title,
      };

    case RESELECT_CATEGORY:
      return {
        ...state,
        categorySelected: false,
        category: '',
      };
    case CHOOSE_SONG:
      return {
        ...state,
        songSelected: true,
      };
    case RESET_STATE:
      return {
        ...initialState,
      };

    default:
      return state;
  }
};
