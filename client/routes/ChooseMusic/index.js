import React, { useEffect } from 'react';
import { func } from 'prop-types';
import { connect } from 'react-redux';
import { useInjectReducer } from '../../reducers/injectReducer';
import { useInjectSaga } from '../../saga/injectSaga';
import { getMusicCategoryList } from './actions';
import { chooseMusicReducer } from './reducer';
import musicCategorySaga from './saga';
import ChooseMusicTemp from './component';
import SongList from '../SongsList';
import { firebaseAnalytics } from '../../firebaseConfig';
import { logEvent } from 'firebase/analytics';

const ChooseMusic = (props) => {
  const { music, dispatch } = props;

  useInjectReducer({
    key: 'chooseMusicReducer',
    reducer: chooseMusicReducer,
  });
  useInjectSaga({ key: 'musicCategorySaga', saga: musicCategorySaga });

  useEffect(() => {
    logEvent(firebaseAnalytics, 'music_visited');
    dispatch(getMusicCategoryList());
  }, []);

  return (
    <>
      {music.categorySelected === false ? (
        <ChooseMusicTemp dispatch={dispatch} music={music} />
      ) : (
        <SongList music={music} dispatch={dispatch} />
      )}
    </>
  );
};

ChooseMusic.propTypes = {
  dispatch: func.isRequired,
};

ChooseMusic.defaultProps = {
  music: {
    categorySelected: false,
  },
};

function mapStateToProps(props) {
  return {
    music: props.chooseMusicReducer,
  };
}

export default connect(mapStateToProps)(ChooseMusic);
