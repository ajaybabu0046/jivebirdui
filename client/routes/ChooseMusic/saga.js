/**
 * Gets the repositories of the user from Github
 */
import { call, put, takeLatest } from 'redux-saga/effects';

import { apiResource } from '@/lib/utils/api';

import {
  chooseMusicCategoryActions,
  getMusicCategorySuccess,
  getMusicCategoryFailure,
} from './actions';

function* getMusicCategoryList() {
  try {
    const url = '/song_categories';
    const res = yield call(apiResource.get, url);

    if (res.length > 0) {
      yield put(
        getMusicCategorySuccess({
          Data: res,
        }),
      );
    } else {
      yield put(
        getMusicCategoryFailure({
          Data: res,
        }),
      );
    }
  } catch (e) {
    yield put(
      getMusicCategoryFailure({
        Data: [],
      }),
    );
  }
}

function* musicCategorySaga() {
  yield takeLatest(
    chooseMusicCategoryActions.GET_MUSIC_CATEGORY,
    getMusicCategoryList,
  );
}

export default musicCategorySaga;
