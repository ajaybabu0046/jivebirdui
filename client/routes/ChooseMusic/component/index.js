import React, { useState, useEffect } from 'react';
import './style.css';
import SearchBox from '../../../components/SearchBox';
import { chooseMusicCategory } from '../actions';
import Loader from 'react-loader-spinner';
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';
import SongsList from '../../SongsList';
import { firebaseAnalytics } from '../../../firebaseConfig';
import { logEvent } from 'firebase/analytics';

const ChooseMusicTemp = (props) => {
  const { music, dispatch } = props;

  useEffect(() => {
    logEvent(firebaseAnalytics, 'Song_Categories_Loaded');
  }, []);

  const [isSearching, setIsSearching] = useState(false);
  const [searchText, setSearchText] = useState('');

  const getImage = (image) => {
    switch (image) {
      case 'Christmas':
        return process.env.REACT_APP_WEB_ASSETS + '/christ.png';
      case 'Congratulations':
        return process.env.REACT_APP_WEB_ASSETS + '/congra.png';
      case 'Birthday':
        return process.env.REACT_APP_WEB_ASSETS + '/birth.png';
      case 'Thank You':
        return process.env.REACT_APP_WEB_ASSETS + '/thank.png';
      case 'Love':
        return process.env.REACT_APP_WEB_ASSETS + '/love.png';
      case 'Valentine':
        return process.env.REACT_APP_WEB_ASSETS + '/valen.png';
      case 'Clubbing':
        return process.env.REACT_APP_WEB_ASSETS + '/club.png';
      case 'Celebration':
        return process.env.REACT_APP_WEB_ASSETS + '/celebr.png';
      case 'Sorry':
        return process.env.REACT_APP_WEB_ASSETS + '/sorry.png';
      case 'Sport':
        return process.env.REACT_APP_WEB_ASSETS + '/sport.png';
      case 'Anniversary':
        return process.env.REACT_APP_WEB_ASSETS + '/anivr.png';
      case 'Baby Shower':
        return process.env.REACT_APP_WEB_ASSETS + '/BabyShower.png';
      case 'Wedding':
        return process.env.REACT_APP_WEB_ASSETS + '/Wedding.png';
      case 'Get Well':
        return process.env.REACT_APP_WEB_ASSETS + '/GetWell.png';
      case 'Latest Hits':
        return process.env.REACT_APP_WEB_ASSETS + '/LatestHits.png';

      default:
        return process.env.REACT_APP_WEB_ASSETS + '/default-icon.png';
    }
  };

  const handleSearch = (e) => {
    setIsSearching(true);

    if (e.target.value === '') {
      setIsSearching(false);
      setSearchText('');
    } else setSearchText(e.target.value);
  };

  return (
    <>
      {music && music.categoryListLoading === false ? (
        <div className="container-fluid chooseCardContainer p-0">
          <div className="d-flex mt-3 musicHeader sticky">
            <h1 className="mainHeading">
              Choose <span style={{ color: '#734385' }}>Music</span>
            </h1>
            <SearchBox handleSearch={handleSearch} />
          </div>
          {isSearching ? (
            <>
              <SongsList
                music={music}
                dispatch={dispatch}
                isSearching={isSearching}
                searchText={searchText}
              />
            </>
          ) : (
            <div className="row mt-4 musicContainer commonScroller">
              {music &&
              music.songCategoryList &&
              music.songCategoryList.length > 0 ? (
                music.songCategoryList.map((data) => (
                  <div
                    key={data.id}
                    className="col-sm-2 col-md-6 col-lg-2 mb-4 chooseCard"
                  >
                    <div className="d-flex justify-content-center imgDiv">
                      <img
                        src={getImage(data.name)}
                        alt="birthday"
                        onClick={() => {
                          logEvent(firebaseAnalytics, 'Song_Category_Selected');
                          dispatch(
                            chooseMusicCategory({ title: `${data.name}` }),
                          );
                        }}
                      />
                    </div>
                    <h6 className="d-flex justify-content-center mt-3">
                      {data.name}
                    </h6>
                  </div>
                ))
              ) : (
                <div className="d-flex justify-content-center">
                  <h4>No music categories found!</h4>
                </div>
              )}
            </div>
          )}
        </div>
      ) : (
        <div className="loaderContainer">
          <Loader
            type="Bars"
            color="#734385"
            height={100}
            width={100}
            radius={30}
          />
        </div>
      )}
    </>
  );
};

export default ChooseMusicTemp;
