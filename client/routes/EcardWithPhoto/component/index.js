import React, { useState, useRef, Fragment, useEffect } from 'react';
import './style.css';
import GalleryImg from '@assets/galry1.png';
import CamraImg from '@assets/camra1.png';
import CancelIcon from '@assets/crossBtn.png';
import { Modal } from 'react-bootstrap';
import CameraOpen from '../../../components/OpenCamera';
import Loader from 'react-loader-spinner';
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import Button from 'react-bootstrap/Button';
import { uploadFile } from 'react-s3';
import { selectPhotoCard } from '../actions';
import { changeScreen } from '../../ChooseEcard/actions';
import { firebaseAnalytics } from '../../../firebaseConfig';
import { logEvent } from 'firebase/analytics';
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import Accordion from 'react-accordian';
const s3config = {
  bucketName: 'jivebirdbucketprod',
  region: 'eu-west-2',
  accessKeyId: 'AKIAWRPJOMVGRRNNDFWJ',
  secretAccessKey: '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',
  dirName: 'images/photocards',
};

const responsive = {
  superLargeDesktop: {
    // the naming can be any, depends on you.
    breakpoint: { max: 4000, min: 3000 },
    items: 5,
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 5,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 3,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
  },
};

const cardAccordian = {
  padding: '6px',
  background: 'transparent',
  borderBottom: '1px solid #E96D30',
  padding: '10px',
  color: '#734385',
  fontSize: '1.7rem',
  fontFamily: 'Paytone One',
};

const cardAccordianActive = {
  height: '100%',
  padding: '6px',
  background: 'transparent',
  padding: '10px',
  color: '#734385',
  borderBottom: '1px solid #E96D30',
  fontSize: '1.7rem',
  fontFamily: 'Paytone One',
};

const ActiveIconStyle = {
  color: '#734385',
  fontSize: '20px !important',
};
const notActiveIconStyle = {
  color: '#734385',
  fontSize: '20px !important',
};

const EcardWithPhotoTemp = (props) => {
  const { setSelected, photoCardData, dispatch } = props;
  const [open, setOpen] = useState(false);
  const hiddenFileInput1 = useRef(null);
  const [modal2Visible, setModal2Visible] = useState(false);
  const [responseData, setResponseData] = useState();
  const [loadingAllData, setLoadingAllData] = useState(true);
  const [photoCardId, setPhotoCardId] = useState('');
  const [cropmodalShow, setCropModalShow] = useState(false);
  const [img, setImg] = useState();
  const [crop, setCrop] = useState({
    unit: '%',
    width: 50,
    aspect: 4 / 5,
  });
  const [image, setImage] = useState(null);
  const [clicked, setClicked] = useState('0');

  const handleToggle = (index, data) => {
    if (clicked === index) return setClicked('0');

    setClicked(index);
    if (responseData[index].cardList.length === 0) {
      let tempArray = [...responseData];
      fetch(`${process.env.BASE_API_PATH}/photocard_categories/${data.name}/`, {
        method: 'GET',
      })
        .then((response) => response.json())
        .then((response) => {
          let sortedByPopularity = response.sort((a, b) => {
            var p1 = a.popularity,
              p2 = b.popularity;

            if (p1 < p2) return 1;
            if (p1 > p2) return -1;
            return 0;
          });

          tempArray[index] = {
            ...tempArray[index],
            cardList: sortedByPopularity,
          };
          setResponseData(tempArray);
        });
    }
  };
  const handleOpen = (id, cardName) => {
    setPhotoCardId(id);

    logEvent(firebaseAnalytics, 'Card_PhotoCard_Picked');
    setOpen(true);
    // setSelected(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleClick = () => {
    logEvent(firebaseAnalytics, 'GettingImageFromGallery');
    hiddenFileInput1.current.click();
  };

  const handleImgChange = (e) => {
    const reader = new FileReader();
    reader.readAsDataURL(e.target.files[0]);
    reader.onload = () => {
      if (reader.readyState === 2) {
        setImg(reader.result);
        setModal2Visible(false);
        setOpen(false);
        setCropModalShow(true);
        hiddenFileInput1.current.value = null;
      }
    };

    // var file = e.target.files[0];
    // if (file) {
    //   uploadFile(file, s3config).then((doc) => {
    //     dispatch(
    //       selectPhotoCard({
    //         id: photoCardId,
    //         photoUrl: doc.location,
    //         location: doc.key,
    //       }),
    //     );
    //     hiddenFileInput1.current.value = null;
    //     setSelected(true);
    //   });
    // }
  };

  function CropImg() {
    const canvas = document.createElement('canvas');
    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    canvas.width = crop.width;
    canvas.height = crop.height;
    const ctx = canvas.getContext('2d');

    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      crop.width,
      crop.height,
    );

    // const base64Image = canvas.toDataURL('image/jpeg');
    // setProfilePic(base64Image);

    return new Promise((resolve, reject) => {
      canvas.toBlob(
        (blob) => {
          resolve(blob);
          blob.name = Math.random() * 100 + 99;
          setCropModalShow(false);
          uploadtoaws(blob);
        },
        'image/jpeg',
        1,
      );
    });
  }

  const uploadtoaws = (blobImage) => {
    uploadFile(blobImage, s3config).then((doc) => {
      dispatch(
        selectPhotoCard({
          id: photoCardId,
          photoUrl: doc.location,
          location: doc.key,
        }),
      );
      setSelected(true);
    });
  };

  const handleModalOpen = () => {
    setModal2Visible(true);
  };

  useEffect(() => {
    let responsArray = [];
    let i = 0;
    if (
      photoCardData &&
      photoCardData.getPhotoCardCategory &&
      photoCardData.getPhotoCardCategory.length > 0
    ) {
      let responsArray = [];
      photoCardData.getPhotoCardCategory.map((data) => {
        let obj = {
          id: data.id,
          name: data.name,
          cardList: [],
        };
        responsArray.push(obj);
      });

      setResponseData(responsArray);
      if (responsArray.length === photoCardData.getPhotoCardCategory.length) {
        setLoadingAllData(false);
      }
    }
  }, [photoCardData]);
  const DownArrow = () => {
    return <i className="fas fa-arrow-down"></i>;
  };

  const RightArrow = () => {
    return <i className="fas fa-arrow-right"></i>;
  };

  return (
    <>
      {modal2Visible === false ? (
        <>
          {loadingAllData === false ? (
            <div className="container-fluid selectECardWithPhotoContainer px-0 py-3">
              <div className="d-flex eCardRow btns-div my-3 sticky">
                <h1 className="mainHeading mr-auto head1">
                  Select{' '}
                  <span style={{ color: '#734385' }}>eCard with Photo</span>
                </h1>
                <button
                  type="button"
                  name="reselect"
                  className="btn app-btn head2"
                  onClick={() => {
                    dispatch(
                      changeScreen({
                        Id: 0,
                      }),
                    );
                  }}
                >
                  Re-Select
                </button>
              </div>
              {responseData &&
                responseData.length > 0 &&
                responseData.map((data, index) => (
                  <Accordion
                    active={clicked === index}
                    onToggle={() => handleToggle(index, data)}
                    header={data.name}
                    gap="10px"
                    styling={cardAccordian}
                    activeStyling={cardAccordianActive}
                    activeIcon={<DownArrow />}
                    notActiveIcon={<RightArrow />}
                    activeIconStyling={ActiveIconStyle}
                    notActiveIconStyling={notActiveIconStyle}
                  >
                    {data.cardList.length === 0 ? (
                      <div className="loaderContainer">
                        <Loader
                          type="Bars"
                          color="#734385"
                          height={200}
                          width={100}
                          radius={30}
                        />
                      </div>
                    ) : (
                      <Carousel responsive={responsive}>
                        {data.cardList.map((cards) => (
                          <div
                            className="Cards"
                            key={cards.id}
                            onClick={() => handleOpen(cards.id, data.name)}
                          >
                            <img
                              className="phtotocardFrame pc"
                              style={{
                                cursor: 'pointer',
                                borderRadius: 10,
                              }}
                              src={`${process.env.BASE_API_PATH}/photocards/download_thumbnail/${cards.id}`}
                              alt="birth"
                            />
                          </div>
                        ))}
                      </Carousel>
                    )}
                  </Accordion>
                ))}

              {/* Modal Start */}
              <Modal
                show={open}
                onHide={() => handleClose()}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
              >
                <Modal.Body style={{ backgroundColor: '#fff' }}>
                  <div className="imgSource">
                    <h1>Pick image source</h1>
                    <div className="cancelIcon" onClick={() => handleClose()}>
                      <img src={CancelIcon} alt="cancel" />
                    </div>
                  </div>
                  <div className="modalBody mt-5 mb-5">
                    <button
                      type="button"
                      className="btn btnGallery"
                      onClick={(e) => handleClick(e)}
                    >
                      <div className="">
                        <img src={GalleryImg} alt="gal" />
                      </div>
                      Gallery
                    </button>
                    <input
                      type="file"
                      accept="image/*"
                      ref={hiddenFileInput1}
                      onChange={(e) => handleImgChange(e)}
                      style={{ display: 'none' }}
                    />
                    <button
                      type="button"
                      className="btn btnGallery m-4"
                      onClick={() => handleModalOpen()}
                    >
                      <div className="mb-2">
                        <img src={CamraImg} alt="cam" />
                      </div>
                      Camera
                    </button>
                  </div>
                </Modal.Body>
              </Modal>
              {/* End */}
            </div>
          ) : (
            <div className="loaderContainer">
              <Loader
                type="Bars"
                color="#734385"
                height={100}
                width={100}
                radius={30}
              />
            </div>
          )}
        </>
      ) : (
        <div className="">
          {modal2Visible && (
            <CameraOpen
              setModal2Visible={setModal2Visible}
              setOpen={setOpen}
              setSelected={setSelected}
              dispatch={dispatch}
              photoCardId={photoCardId}
              setImg={setImg}
              setCropModalShow={setCropModalShow}
            />
          )}
        </div>
      )}

      {/* Crop Modal */}
      <Modal
        show={cropmodalShow}
        backdrop="static"
        onHide={() => setCropModalShow(false)}
        size="md"
        dialogClassName="cropModal"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header
          style={{ backgroundColor: '#ececec', textAlign: 'center' }}
        >
          <Modal.Title id="contained-modal-title-vcenter">
            <h1 className="mainHeading">
              Crop <span style={{ color: '#734385' }}>Image</span>{' '}
            </h1>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body style={{ backgroundColor: '#ececec', textAlign: 'center' }}>
          <div className="crop-image-container">
            <ReactCrop
              src={img}
              crop={crop}
              onImageLoaded={setImage}
              onChange={(newCrop) => setCrop(newCrop)}
              keepSelection
              locked
            />
          </div>
        </Modal.Body>
        <Modal.Footer
          style={{ backgroundColor: '#ececec', justifyContent: 'center' }}
        >
          <Button
            variant="light"
            className="app-btn"
            onClick={() => setCropModalShow(false)}
          >
            Cancel
          </Button>
          <Button variant="light" className="app-btn" onClick={() => CropImg()}>
            Crop
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default EcardWithPhotoTemp;
