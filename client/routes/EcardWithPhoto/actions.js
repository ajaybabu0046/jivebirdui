import { createActionTypes } from '@/lib/utils/helper';

export const getPhotoWCardCategoriesActions = createActionTypes(
  'getPhotoCardCategory',
  [
    'GET_PHOTO_CARD_CATEGORY',
    'GET_PHOTO_CARD_CATEGORY_SUCCESS',
    'GET_PHOTO_CARD_CATEGORY_FAIL',
    'SELECT_PHOTO_CARD',
    'SAVE_MESSAGE',
    'SKIPPED',
  ],
);

export const skipCardWithPhoto = () => ({
  type: getPhotoWCardCategoriesActions.SKIPPED,
});

export const getPhotoWCardCategories = (obj) => ({
  type: getPhotoWCardCategoriesActions.GET_PHOTO_CARD_CATEGORY,
  payload: {
    ...obj,
  },
});

export const getPhotoWCardCategoriesSuccess = (obj) => ({
  type: getPhotoWCardCategoriesActions.GET_PHOTO_CARD_CATEGORY_SUCCESS,
  payload: {
    ...obj,
  },
});

export const getPhotoWCardCategoriesFailure = (obj) => ({
  type: getPhotoWCardCategoriesActions.GET_PHOTO_CARD_CATEGORY_FAIL,
  payload: {
    ...obj,
  },
});

export const selectPhotoCard = (obj) => ({
  type: getPhotoWCardCategoriesActions.SELECT_PHOTO_CARD,
  payload: {
    ...obj,
  },
});

export const saveMessage = (obj) => ({
  type: getPhotoWCardCategoriesActions.SAVE_MESSAGE,
  payload: {
    ...obj,
  },
});
