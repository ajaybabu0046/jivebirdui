import React, { useState, useEffect } from 'react';
import PhotoPreviewTemp from '../PhotoCardPreview/component';
import EcardWithPhotoTemp from './component';
import { func } from 'prop-types';
import { connect } from 'react-redux';
import { useInjectReducer } from '../../reducers/injectReducer';
import { useInjectSaga } from '../../saga/injectSaga';
import { getPhotoWCardCategories } from './actions';
import { getPhotoCardCategoriesReducer } from './reducer';
import getPhotoWCardCategoriesSaga from './saga';

const selectEcardWithPhoto = (props) => {
  const { dispatch, photoCardData } = props;
  const { cardSelected } = photoCardData;

  useInjectReducer({
    key: 'getPhotoCardCategoriesReducer',
    reducer: getPhotoCardCategoriesReducer,
  });
  useInjectSaga({
    key: 'getPhotoWCardCategoriesSaga',
    saga: getPhotoWCardCategoriesSaga,
  });

  const [selected, setSelected] = useState(false);

  useEffect(() => {
    dispatch(getPhotoWCardCategories({}));
  }, []);

  return (
    <>
      {cardSelected === true ? (
        <PhotoPreviewTemp {...props} />
      ) : (
        <EcardWithPhotoTemp {...props} setSelected={setSelected} />
      )}
    </>
  );
};

selectEcardWithPhoto.propTypes = {
  dispatch: func.isRequired,
};

selectEcardWithPhoto.defaultProps = {
  photoCardData: {
    cardSelected: false,
  },
};

function mapStateToProps(props) {
  return {
    photoCardData: props.getPhotoCardCategoriesReducer,
  };
}

export default connect(mapStateToProps)(selectEcardWithPhoto);
