/**
 * Gets the repositories of the user from Github
 */
import { call, put, takeLatest } from 'redux-saga/effects';

import { apiResource } from '@/lib/utils/api';

import {
  getPhotoWCardCategoriesActions,
  getPhotoWCardCategoriesSuccess,
  getPhotoWCardCategoriesFailure,
} from './actions';

function* getPhotoWCardCategories(dObj) {
  try {
    const url = '/photocard_categories';
    const res = yield call(apiResource.get, url);

    if (res.length > 0) {
      yield put(
        getPhotoWCardCategoriesSuccess({
          Data: res,
        }),
      );
    } else {
      yield put(
        getPhotoWCardCategoriesFailure({
          Data: res,
        }),
      );
    }
  } catch (e) {
    yield put(
      getPhotoWCardCategoriesFailure({
        message: 'Something went wrong',
      }),
    );
  }
}

function* getPhotoWCardCategoriesSaga() {
  yield takeLatest(
    getPhotoWCardCategoriesActions.GET_PHOTO_CARD_CATEGORY,
    getPhotoWCardCategories,
  );
}

export default getPhotoWCardCategoriesSaga;
