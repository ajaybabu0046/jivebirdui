import { getPhotoWCardCategoriesActions } from './actions';

export const initialState = {
  message: '',
  getPhotoCardCategory: [],
  success: 0,
  loading: false,
  cardSelected: false,
  cardId: '',
  cardUrl: '',
  photoUrl: '',
  photoLocation: '',
  isSkipped: false,
};

export const getPhotoCardCategoriesReducer = (state = initialState, action) => {
  const {
    GET_PHOTO_CARD_CATEGORY,
    GET_PHOTO_CARD_CATEGORY_SUCCESS,
    GET_PHOTO_CARD_CATEGORY_FAIL,
    SELECT_PHOTO_CARD,
    SAVE_MESSAGE,
    SKIPPED,
  } = getPhotoWCardCategoriesActions;

  switch (action.type) {
    case SKIPPED:
      return {
        ...state,
        isSkipped: true,
      };
    case GET_PHOTO_CARD_CATEGORY:
      return {
        ...state,
        loading: true,
        isSkipped: false,
      };
    case GET_PHOTO_CARD_CATEGORY_SUCCESS:
      let sortedByPopularity = action.payload.Data.sort((a, b) => {
        var p1 = a.popularity,
          p2 = b.popularity;

        if (p1 < p2) return 1;
        if (p1 > p2) return -1;
        return 0;
      });

      return {
        ...state,
        loading: false,
        getPhotoCardCategory: sortedByPopularity,
        success: 1,
        isSkipped: false,
      };
    case GET_PHOTO_CARD_CATEGORY_FAIL:
      return {
        ...state,
        loading: false,
        getPhotoCardCategory: action.payload.Data,
        success: 0,
        isSkipped: false,
      };
    case SELECT_PHOTO_CARD:
      if (action.payload.id === '') {
        return {
          ...state,
          cardId: '',
          cardUrl: '',
          photoUrl: '',
          cardSelected: false,
          photoLocation: '',
        };
      } else {
        return {
          ...state,
          cardId: action.payload.id,
          cardUrl: `${process.env.BASE_API_PATH}/photocards/download/${action.payload.id}`,
          photoUrl: action.payload.photoUrl,
          cardSelected: true,
          photoLocation: action.payload.location,
          isSkipped: false,
        };
      }

    case SAVE_MESSAGE:
      return {
        ...state,
        message: action.payload.msg,
        isSkipped: false,
      };

    default:
      return state;
  }
};
