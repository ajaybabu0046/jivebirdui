import { chooseEgiftActions } from './actions';

export const initialState = {
  EgiftLoading: false,
  EgiftSelected: false,
  EgiftLoading: '',
  EgiftList: [],
  selectedEgift: {},
  EgiftLoadingVendor: true,
  selectedEgiftValue: 0,
  eGiftScreen: 0,
  isSkipped: false,
};

export const EgiftReducer = (state = initialState, action) => {
  const {
    CHOOSE_E_GIFT,
    RESET_STATE,
    GET_E_GIFT,
    GET_E_GIFT_SUCCESS,
    GET_E_GIFT_FAIL,
    SELECT_E_GIFT,
    SELECT_E_GIFT_VALUE,
    CHANGE_SCREEN,
    RESET,
    RESELECT_GIFT_CARD,
    SKIPPED,
  } = chooseEgiftActions;

  switch (action.type) {
    case SKIPPED:
      return {
        ...initialState,
        isSkipped: true,
        selectedEgift: {},
        selectedEgiftValue: 0,
      };
    case RESET:
      return {
        ...initialState,
      };
    case RESELECT_GIFT_CARD:
      return {
        ...state,
        selectedEgift: {},
        selectedEgiftValue: 0,
      };
    case CHANGE_SCREEN:
      return {
        ...state,
        eGiftScreen: action.payload.Id,
      };
    case CHOOSE_E_GIFT:
      return {
        ...state,
        EgiftSelected: true,
        EgiftLoading: action.payload.title,
      };
    case SELECT_E_GIFT_VALUE:
      return {
        ...state,
        selectedEgiftValue: action.payload.egiftValue,
      };
    case RESET_STATE:
      return {
        ...initialState,
      };
    case GET_E_GIFT:
      return {
        ...state,
        EgiftLoading: true,
        EgiftLoadingVendor: true,
      };
    case GET_E_GIFT_SUCCESS:
      return {
        ...state,
        EgiftLoading: false,
        EgiftList: action.payload.Data,
        success: 1,
        EgiftLoadingVendor: false,
      };
    case GET_E_GIFT_FAIL:
      return {
        ...state,
        EgiftLoading: false,
        EgiftList: action.payload.Data,
        success: 0,
        EgiftLoadingVendor: false,
      };
    case SELECT_E_GIFT:
      return {
        ...state,
        selectedEgift: action.payload,
      };

    default:
      return state;
  }
};
