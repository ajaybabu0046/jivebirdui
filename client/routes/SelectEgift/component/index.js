import React from 'react';
import './style.css';
import Loader from 'react-loader-spinner';
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';
import { chooseEgift, changeScreen } from '../actions';

const SelectEgiftTemp = (props) => {
  const { vendorlist, dispatch } = props;

  const handleSelect = (data) => {
    dispatch(chooseEgift(data));
    dispatch(changeScreen({ Id: 2 }));
  };

  return (
    <div className="container-fluid egiftcontainer mt-3">
      <div className="d-flex  btns-div mt-3 sticky">
        <h1 className="mainHeading">
          Select <span>eGift</span>
        </h1>
        <div className="ml-auto head2">
          <button
            type="button"
            name="reselect"
            className="btn app-btn mt-2"
            onClick={() => dispatch(changeScreen({ Id: 0 }))}
          >
            Re-Select
          </button>
        </div>
      </div>
      <div className="scrollbar commonScroller">
        <div className="row ">
          {vendorlist &&
          vendorlist.EgiftList &&
          vendorlist.EgiftList.length > 0 ? (
            vendorlist.EgiftList.map((data) => (
              <div
                key={data.id}
                className="col-lg-3 col-md-6  imagesizing mt-5"
              >
                <img
                  src={data.vendorLogo}
                  style={{ cursor: 'pointer' }}
                  onClick={() => handleSelect(data)}
                />
              </div>
            ))
          ) : vendorlist &&
            vendorlist.EgiftList &&
            vendorlist.EgiftList.length == 0 ? (
            ''
          ) : (
            <div className="loaderContainer">
              <Loader
                type="Bars"
                color="#734385"
                height={100}
                width={100}
                radius={30}
              />
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default SelectEgiftTemp;
