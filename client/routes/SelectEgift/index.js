import React, { useEffect } from 'react';
import SelectEgiftTemp from './component';

import { func } from 'prop-types';
import { connect } from 'react-redux';
import { useInjectReducer } from '../../reducers/injectReducer';
import { useInjectSaga } from '../../saga/injectSaga';
import { getEgift } from './actions';
import { EgiftReducer } from './reducer';
import EgiftSaga from './saga';

const SelectEgift = (props) => {
  const { dispatch } = props;

  useInjectReducer({
    key: 'EgiftReducer',
    reducer: EgiftReducer,
  });
  useInjectSaga({ key: 'EgiftSaga', saga: EgiftSaga });

  useEffect(() => {
    dispatch(getEgift({}));
  }, []);

  return <SelectEgiftTemp {...props} />;
};
SelectEgift.propTypes = {
  dispatch: func.isRequired,
};

function mapStateToProps(props) {
  return {
    vendorlist: props.EgiftReducer,
  };
}

export default connect(mapStateToProps)(SelectEgift);
