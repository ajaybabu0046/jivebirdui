/**
 * Gets the repositories of the user from Github
 */
import { call, put, takeLatest } from 'redux-saga/effects';

import { apiResource } from '@/lib/utils/api';

import {
  chooseEgiftActions,
  getEgiftSuccess,
  getEgiftFailure,
} from './actions';

function* getEgift(dObj) {
  try {
    const url = '/gifts/vendors';
    const res = yield call(apiResource.get, url);

    if (res.length > 0) {
      yield put(
        getEgiftSuccess({
          Data: res,
        }),
      );
    } else {
      yield put(
        getEgiftFailure({
          Data: res,
        }),
      );
    }
  } catch (e) {
    yield put(
      getEgiftFailure({
        message: 'Something went wrong',
      }),
    );
  }
}

function* EgiftSaga() {
  yield takeLatest(chooseEgiftActions.GET_E_GIFT, getEgift);
}

export default EgiftSaga;
