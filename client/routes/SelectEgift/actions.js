import { createActionTypes } from '@/lib/utils/helper';

export const chooseEgiftActions = createActionTypes('chooseEgift', [
  'CHOOSE_E_GIFT',
  'RESET_STATE',

  'GET_E_GIFT',
  'GET_E_GIFT_SUCCESS',
  'GET_E_GIFT_FAIL',
  'SELECT_E_GIFT',
  'SELECT_E_GIFT_VALUE',

  'CHANGE_SCREEN',
  'RESET',
  'RESELECT_GIFT_CARD',
  'SKIPPED',
]);

export function skipEgift() {
  return {
    type: chooseEgiftActions.SKIPPED,
  };
}

export function reset() {
  return {
    type: chooseEgiftActions.RESET,
  };
}

export function reselectGiftCard() {
  return {
    type: chooseEgiftActions.RESELECT_GIFT_CARD,
  };
}

export function changeScreen(obj) {
  return {
    type: chooseEgiftActions.CHANGE_SCREEN,
    payload: {
      ...obj,
    },
  };
}

export function chooseEgift(obj) {
  return {
    type: chooseEgiftActions.SELECT_E_GIFT,
    payload: {
      ...obj,
    },
  };
}

export function chooseEgiftValue(obj) {
  return {
    type: chooseEgiftActions.SELECT_E_GIFT_VALUE,
    payload: {
      ...obj,
    },
  };
}

export function resetEgiftState(obj) {
  return {
    type: chooseEgiftActions.RESET_STATE,
    payload: {
      ...obj,
    },
  };
}

export const getEgift = (obj) => ({
  type: chooseEgiftActions.GET_E_GIFT,
  payload: {
    ...obj,
  },
});

export const getEgiftSuccess = (obj) => ({
  type: chooseEgiftActions.GET_E_GIFT_SUCCESS,
  payload: {
    ...obj,
  },
});

export const getEgiftFailure = (obj) => ({
  type: chooseEgiftActions.GET_E_GIFT_FAIL,
  payload: {
    ...obj,
  },
});
