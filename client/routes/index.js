import React, { useEffect } from 'react';
import './index.css';
import { Route, Switch } from 'react-router-dom';
import { shape } from 'prop-types';
import Loader from '@components/Loader';
import Loadable from 'react-loadable';
import { firebaseAnalytics } from '../firebaseConfig';
import { logEvent } from 'firebase/analytics';

const errorLoading = (err) =>
  console.log('Dynamic home page loading failed', err);

const CreateJiveRoute = Loadable({
  loader: () => import('@routes/CreateJiveBird').catch(errorLoading),
  loading: () => <Loader />, // full page loader here
  delay: 200, // delay in milliseconds, useful for suspense
});
const CreateJiveRoute1 = Loadable({
  loader: () => import('@routes/Test').catch(errorLoading),
  loading: () => <Loader />, // full page loader here
  delay: 200, // delay in milliseconds, useful for suspense
});

const Routes = () => {
  useEffect(() => {
    logEvent(firebaseAnalytics, 'EventAppOpen');
  }, []);

  return (
    <Switch>
      <Route key="createjive" exact path="/" component={CreateJiveRoute} />
      {/* <Route key="createjive1" exact path="/test" component={CreateJiveRoute1} /> */}
    </Switch>
    // <CreateJiveRoute />
  );
};

Routes.propTypes = {
  history: shape({}).isRequired,
  location: shape({}).isRequired,
  match: shape({}).isRequired,
};

const RootRoutes = () => <Route component={Routes} />;

export default RootRoutes;
