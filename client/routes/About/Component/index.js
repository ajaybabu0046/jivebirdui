import React from 'react';
import './index.css';

const AboutTemp = () => {
  return (
    <div className="container-fluid ">
      <div className="App">
        <h1 className="mt-5 heading">About</h1>

        <video
          id="vid"
          className="videoScreen1"
          autoPlay
          muted
          controls
          loop
          controlsList="nodownload nofullscreen noremoteplayback"
        >
          <source
            src="https://jivebirdbucketprod.s3.eu-west-2.amazonaws.com/webapp-assets/JiveBirdvideo.mp4"
            type="video/mp4"
          />
        </video>
      </div>
      <br />
      <br />
      <div className="paragraph">
        <h3>JiveBird</h3>
        <br />

        <p>
          The JiveBird idea was born several years ago, before broadband got
          going. Paul was asked to provide the music for a telephone service
          delivering a message and a song. There was lots of interest from the
          public, but the system didn't work. There simply wasn't the bandwidth
          at that time. Now, everyone has smartphones and more of our life is
          online, so Paul and Dominic got together to apply the digital
          transformation to greetings and gifts. We built a prototype, tried it
          out on a few people and the reaction was positive, very positive.
        </p>
        <br />
        <br />

        <h3>"Funny, different, very personal."</h3>
        <br />

        <p>
          After much research and many suggestions from potential users, in
          addition to a voice message we added an eCard and then the ability to
          add an image or a Selfie from one's smartphone. And then many people
          requested that we add an eGift voucher that can be ordered and then
          redeemed online and delivered to a chosen address.
        </p>
        <br />
        <br />

        <h3>Partners</h3>
        <br />

        <p>
          Others caught the JiveBird bug and came on board. Xiatech joined to
          build JiveBird v2.0. Cravens took a stake as our marketing agency,
          producing enhanced branding and easy-to use UX. Diggecard gave us the
          inventory of eGift vouchers from top brands. New investors joined us
          taking us to the next stage. Boodle Hatfield updated our articles of
          association so we could protect our early investors.
        </p>
        <br />
        <br />

        <h3>"Personal touch to a message. A song can mean a lot."</h3>
        <br />

        <p>
          And we are still just coming to the end of chapter one in the JiveBird
          story. Every day we find new ways for JiveBird to deliver on our
          promise a fully digital and personal greeting and eGift service. Our
          product team come up with ideas for new features. Our marketing gurus
          find new uses for JiveBird.
        </p>
        <br />
        <br />

        <h3>New Version</h3>
        <br />

        <p>
          And our friends ask when the new version will be available so they can
          order online and deliver to their gift online which can then be
          redeemed online and delivered to one's chosen address or to a selected
          delivery point nearby. And they don't have to find a stamp and a post
          box. You can find out more at our website www.JiveBird.com.
          <br />
          <br />
          Let's fly!
          <br />
          <br />
          Paul Lynton
          <br />
          <br />
          Dominic McGonigal
          <br />
          <br />
          Co-Founders of JiveBird
        </p>
        <p>
          <br />
        </p>
      </div>
    </div>
  );
};

export default AboutTemp;
