import React from 'react';
import ContactTemp from './component';

import { shape, func } from 'prop-types';
import { connect } from 'react-redux';

import { useInjectSaga } from '../../saga/injectSaga';
import contactSaga from './saga';
import { contactReducer } from './reducer';
import { useInjectReducer } from '../../reducers/injectReducer';

const Contact = (props) => {
  const { dispatch, ContactData } = props;
  useInjectReducer({
    key: 'ContactReducer',
    reducer: contactReducer,
  });
  useInjectSaga({ key: 'contactSaga', saga: contactSaga });
  return (
    <>
      <ContactTemp dispatch={dispatch} ContactData={ContactData} />
    </>
  );
};

Contact.propTypes = {
  ContactData: shape({}).isRequired,
  dispatch: func.isRequired,
};

Contact.defaultProps = {
  ContactData: {
    loading: false,
  },
};

function mapStateToProps(props) {
  return {
    ContactData: props.ContactReducer,
  };
}
export default connect(mapStateToProps)(Contact);
