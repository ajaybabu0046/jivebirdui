import React, { useState, useEffect } from 'react';
import './style.css';
import { ContactVerify, resetState } from '../actions';
import swal from '@sweetalert/with-react';
import axios from 'axios';
import { Message } from '@material-ui/icons';
const ContactTemp = (props) => {
  const { dispatch, ContactData } = props;
  const [YourName, setYourName] = useState('');
  const [Message, setMessage] = useState('');

  const [email, setEmail] = useState('');

  const [YourNameError, setYourNameError] = useState(false);
  const [MessageError, setMessageError] = useState(false);

  const [emailError, setEmailError] = useState(false);
  // const [errorMessage, setErrorMessage] = useState('');
  const [errorAllFields, setErrorAllFields] = useState('');
  // useEffect(() => {

  //   }, [ContactData]);
  const handleContact = () => {
    let errorFound = false;
    const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (YourName == '' || email == '' || Message == '') {
      errorFound = true;
      setErrorAllFields('All fields are mandatory');

      // setLoading(false);
    }
    if (YourName || YourName.length === 0) {
      let data = YourName.trim(' ');
      if (data.length === 0) {
        errorFound = true;
        setErrorAllFields('Fill your name');
        setYourNameError(true);
      }
    }
    if (email || email.length === 0) {
      let data = email.trim(' ');
      if (data.length === 0) {
        errorFound = true;
        setErrorAllFields('Fill the E-mail');
        setEmailError(true);
      }
    }
    if (email) {
      if (!email.match(mailformat)) {
        errorFound = true;
        setErrorAllFields('Enter valid E-mail');
        setEmailError(true);
      }
    }
    if (Message || Message.length === 0) {
      let data = Message.trim(' ');
      if (data.length === 0) {
        errorFound = true;
        setErrorAllFields('Fill message field');
        setYourNameError(true);
      }
    }
    if (errorFound) dispatch(resetState());

    if (!errorFound) {
      //   const emailId = email.toLowerCase();
      //   const randomId = Math.random().toString(36).substring(1, 7);
      //   dispatch(
      //     ContactVerify({
      //       email: emailId.trim(),
      //       user: YourName.trim(),
      //       message: Message.trim(),

      //     }),
      //   );
      //   dispatch(resetState());
      // }
      var formData = new FormData();
      formData.append('user', YourName);
      formData.append('email', email);
      formData.append('message', Message);

      var config = {
        method: 'post',
        url: `${process.env.BASE_API_PATH}/feedback/`,
        headers: {
          'Content-Type': 'multipart/form-data',
        },
        data: formData,
      };

      axios(config)
        .then((response) => {
          swal(<p>{response.data}</p>).then((willSuccess) => {
            if (willSuccess) {
              setYourName('');
              setMessage('');
              setEmail('');
            }
          });
        })
        .catch((response) => {
          console.log('Error Resp', response);
        });
    }
  };
  const addYourName = (e) => {
    setYourName(e.target.value);
    setErrorAllFields('');
    setYourNameError(false);
    dispatch(resetState());
  };
  const addMessage = (e) => {
    setMessage(e.target.value);
    setErrorAllFields('');
    setMessageError(false);
    dispatch(resetState());
  };

  const addEmail = (e) => {
    setEmail(e.target.value);
    setErrorAllFields('');
    setEmailError(false);
    dispatch(resetState());
  };
  return (
    <div className="container contactcontainer  d-flex justify-content-center align-items-center ">
      <div className="contactBody mt-5">
        <div className="row">
          <div className="col">
            <h1 className="mt-3">Contact</h1>
            <h2>
              Feedback? Help? Suggetion? Use the form below to get in touch.
            </h2>
          </div>
        </div>

        <form>
          <div className="container">
            <div className="row">
              <div className="col-lg-6 mt-3">
                <input
                  type="text"
                  className="form-control"
                  placeholder="Your Name"
                  value={YourName}
                  onChange={(e) => addYourName(e)}
                  onKeyPress={(event) => {
                    if (event.key === 'Enter') {
                      handleContact();
                    }
                  }}
                />
              </div>

              <div className="col-lg-6 mt-3">
                <input
                  type="email"
                  className="form-control emailicon"
                  placeholder="E-mail"
                  value={email}
                  onChange={(e) => addEmail(e)}
                  onKeyPress={(event) => {
                    if (event.key === 'Enter') {
                      handleContact();
                    }
                  }}
                />
              </div>
              <div className="col-lg-12 messagefield">
                <textarea
                  onChange={(e) => addMessage(e)}
                  onKeyPress={(event) => {
                    if (event.key === 'Enter') {
                      handleContact();
                    }
                  }}
                  className="form-control"
                  value={Message}
                  placeholder="Message"
                ></textarea>
              </div>
              <div className="btndiv container mb-3">
                <button
                  type="button"
                  className="btn btnSubmit"
                  onClick={() => handleContact()}
                >
                  Submit
                </button>
              </div>
              <div className="col-lg-12 linksColor mt-2 mb-4 d-flex justify-content-center">
                {errorAllFields}
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default ContactTemp;
