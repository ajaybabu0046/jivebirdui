import { ContactAction } from './actions';

export const initialState = {
  loading: false,

  message: '',
  response: {},
};
export const contactReducer = (state = initialState, action) => {
  const { VERIFY_CONTACT, CONTACT_SUCCESS, CONTACT_FAILED, RESET_STATE } =
    ContactAction;

  const { payload } = action;

  switch (action.type) {
    case VERIFY_CONTACT:
      return {
        ...state,
        loading: true,

        // message:"",
      };
    case CONTACT_SUCCESS:
      return {
        ...state,
        loading: false,

        response: action.payload.ContactData,
      };
    case CONTACT_FAILED:
      return {
        ...state,
        loading: false,
        response: action.payload.ContactData,
      };

    case RESET_STATE:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};
