import { createActionTypes } from '@/lib/utils/helper';

export const ContactAction = createActionTypes('Contact', [
  'VERIFY_CONTACT',
  'CONTACT_SUCCESS',
  'CONTACT_FAILED',
  'RESET_STATE',
]);
export function ContactVerify(obj) {
  return {
    type: ContactAction.VERIFY_CONTACT,
    payload: {
      ...obj,
    },
  };
}

export function ContactSuccess(obj) {
  return {
    type: ContactAction.CONTACT_SUCCESS,
    payload: {
      ...obj,
    },
  };
}

export function ContactFailed(obj) {
  return {
    type: ContactAction.CONTACT_FAILED,
    payload: {
      ...obj,
    },
  };
}
export function resetState() {
  return {
    type: ContactAction.RESET_STATE,
  };
}
