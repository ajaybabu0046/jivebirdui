import { call, put, takeLatest } from 'redux-saga/effects';
import { apiResource } from '@/lib/utils/api';

import { ContactAction, ContactSuccess, ContactFailed } from './actions';
function* ContactVerify(dObj) {
  try {
    const url = '/feedback/';
    const res = yield call(apiResource.post, url, dObj.payload);

    if (res.message == ' ') {
      yield put(
        ContactSuccess({
          ContactData: res,
        }),
      );
    } else {
      yield put(
        ContactFailed({
          ContactData: res,
        }),
      );
    }
  } catch (e) {
    yield put(
      ContactFailed({
        ContactData: 'something went wrong',
      }),
    );
  }
}
function* contactSaga() {
  yield takeLatest(ContactAction.VERIFY_CONTACT, ContactVerify);
}
export default contactSaga;
