import { createActionTypes } from '@/lib/utils/helper';

export const eCardActions = createActionTypes('eCard', [
  'CHANGE_CURRENT_SCREEN',
  'SKIPPED',
]);

export function skipCards() {
  return {
    type: eCardActions.SKIPPED,
  };
}
export function changeScreen(obj) {
  return {
    type: eCardActions.CHANGE_CURRENT_SCREEN,
    payload: {
      ...obj,
    },
  };
}
