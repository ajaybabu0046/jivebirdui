import React from 'react';
import { func } from 'prop-types';
import { connect } from 'react-redux';
import { useInjectReducer } from '../../reducers/injectReducer';
import { eCardReducer } from './reducer';
import ChooseEcard from './component/index';
import SelectEcard from '../SelectEcard';
import EcardWithPhoto from '../EcardWithPhoto';

const chooseEcard = (props) => {
  const { eData } = props;
  const { typeSelected } = eData;

  useInjectReducer({
    key: 'eCardReducer',
    reducer: eCardReducer,
  });

  return (
    <>
      {typeSelected === 0 && <ChooseEcard {...props} />}
      {typeSelected === 1 && <SelectEcard {...props} />}
      {typeSelected === 2 && <EcardWithPhoto {...props} />}
    </>
  );
};

chooseEcard.propTypes = {
  dispatch: func.isRequired,
};

chooseEcard.defaultProps = {
  eData: {
    typeSelected: 0,
  },
};

function mapStateToProps(props) {
  return {
    eData: props.eCardReducer,
  };
}

export default connect(mapStateToProps)(chooseEcard);
