import React from 'react';
import { changeTab } from '../../CreateJiveBird/actions';
import './style.css';
import chooseecard from '@assets/chooseecard.png';
import { changeScreen, skipCards } from '../actions';
import { skipCard } from '../../SelectEcard/actions';
import { skipCardWithPhoto } from '../../EcardWithPhoto/actions';
import { firebaseAnalytics } from '../../../firebaseConfig';
import { logEvent } from 'firebase/analytics';

export default function ChooseEcard(props) {
  const { dispatch } = props;

  return (
    <div className="container-fluid mb-5 mt-3">
      <div className="row eCardRow sticky1">
        <h1 className="mainHeading head1">
          Choose an <span>eCard</span>
        </h1>
        <div className="ml-auto head2">
          <button
            className="btn app-btn ml-3 mt-2"
            style={{ cursor: 'pointer' }}
            onClick={() => {
              logEvent(firebaseAnalytics, 'Card_NoCard_Picked');
              dispatch(changeTab({ tabId: 4 }));
              dispatch(skipCards());
              // dispatch(skipCard());
              // dispatch(skipCardWithPhoto());
            }}
          >
            Skip Card
          </button>
          <button
            className="btn app-btn ml-3 mt-2"
            style={{ cursor: 'pointer' }}
            onClick={() => {
              logEvent(firebaseAnalytics, 'Card_Decided');
              logEvent(firebaseAnalytics, 'Card_PhotoCard_Picked');
              dispatch(changeScreen({ Id: 2 }));
            }}
          >
            eCard with Photo
          </button>
          <button
            className="btn app-btn mt-2"
            style={{ cursor: 'pointer' }}
            onClick={() => {
              logEvent(firebaseAnalytics, 'Card_Decided');
              logEvent(firebaseAnalytics, 'Card_ECard_Picked');
              dispatch(changeScreen({ Id: 1 }));
            }}
          >
            eCard
          </button>
        </div>
      </div>
      <div className="row">
        <div className="col-lg-6 pl-0 Para">
          <h2 className="subHeading">
            Choose an eCard from our selection of eCards and enter your message.
            Or just select "eCard with Photo" where you can add your own gallery
            image or selfie where shown.
          </h2>
        </div>
        <div className="col-lg-6 textCenter">
          <img className="commonBird" src={chooseecard} />
        </div>
      </div>
    </div>
  );
}
