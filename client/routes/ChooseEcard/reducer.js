import { eCardActions } from './actions';

export const initialState = {
  typeSelected: 0, // 1: eCard, 2: photoEcard
  cardSkipped: false,
};

export const eCardReducer = (state = initialState, action) => {
  const { CHANGE_CURRENT_SCREEN, SKIPPED } = eCardActions;

  switch (action.type) {
    case SKIPPED:
      return {
        ...state,
        typeSelected: 0,
        cardSkipped: true,
      };
    case CHANGE_CURRENT_SCREEN:
      return {
        ...state,
        typeSelected: action.payload.Id,
        cardSkipped: false,
      };

    default:
      return state;
  }
};
