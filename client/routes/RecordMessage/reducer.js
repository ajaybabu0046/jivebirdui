import { recordingActions } from './actions';

export const initialState = {
  recordingState: 0,
  s3Url: '',
  blobURL: '',
  audioTimer: 0,
  audioProgress: 0,
  wavAudioKey: '',
  isBlocked: true,
  show: false,
  isRecording: false,
  recorder: null,
  permissionGranted: false,
  error: false,
  isSkipped: false,
};

export const recordingReducer = (state = initialState, action) => {
  const {
    START_RECORDING,
    STOP_RECORDING,
    RESET_STATE,
    SAVE_AUDIO_DATA,
    SAVE_BLOB,
    CHECK_PERMISSION,
    SKIPPED,
  } = recordingActions;

  switch (action.type) {
    case CHECK_PERMISSION:
      return {
        ...state,
        isBlocked: action.payload.blocked,
      };

    case SKIPPED:
      return {
        ...state,
        isSkipped: true,
      };

    case START_RECORDING:
      return {
        ...state,
        recordingState: 1,
        isSkipped: false,
      };

    case STOP_RECORDING:
      return {
        ...state,
        recordingState: 2,
        audioTimer: action.payload.audioTimer,
        audioProgress: action.payload.audioProgress,
        isSkipped: false,
      };

    case RESET_STATE:
      return {
        ...initialState,
      };

    case SAVE_BLOB:
      return {
        ...state,
        blobURL: action.payload.blob,
        isSkipped: false,
      };

    case SAVE_AUDIO_DATA:
      return {
        ...state,
        s3Url: action.payload.url,
        wavAudioKey: action.payload.wavAudioKey,
        show: true,
        isSkipped: false,
      };

    default:
      return state;
  }
};
