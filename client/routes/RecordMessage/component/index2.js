import React, { useState } from 'react';
import {
  resetState,
  recordingSkipped,
  saveAudioData,
  saveBlob,
  audioPermission,
} from '../actions';
import { changeTab } from '../../CreateJiveBird/actions';
import { uploadFile } from 'react-s3';
import MicRecorder from 'mic-recorder-to-mp3';
import audioImg from '@assets/audio.gif';
import swal from '@sweetalert/with-react';
import { firebaseAnalytics } from '../../../firebaseConfig';
import { logEvent } from 'firebase/analytics';

const Mp3Recorder = new MicRecorder({ bitRate: 128 });

const s3config = {
  bucketName: process.env.REACT_APP_BUCKET_NAME,
  region: process.env.REACT_APP_REGION,
  accessKeyId: process.env.REACT_APP_ACCESS_KEYID,
  secretAccessKey: process.env.REACT_APP_SECRETACCESS_KEY,
  dirName: process.env.REACT_APP_DIR_NAME,
};

export default function RecordMessageTemp(props) {
  const { dispatch, data, music } = props;
  const { blobURL } = data;

  const [isRecording, setisRecording] = useState(false);
  const [isPlayingAudio, setAudioPlay] = useState(false);

  async function requestRecorder() {
    try {
      const stream = await navigator.mediaDevices.getUserMedia({ audio: true });
      return new MediaRecorder(stream);
    } catch (error) {
      let errorObj = {
        errorStatus: 0,
        errorMsg: error,
      };
      return errorObj;
    }
  }

  const handleStart = () => {
    var isApple = /iPad|iPhone|Mac|iPod/i.test(navigator.userAgent);

    if (isApple) {
      requestRecorder().then((response) => {
        if (response.hasOwnProperty('errorStatus')) {
          alert(response.errorMsg.message);
        } else {
          setRecorder(response);
          swal(
            <div>
              <p>
                Permission Granted. Please click on OK button below to continue.
              </p>
            </div>,
            {
              icon: 'info',
              closeOnEsc: false,
              closeOnClickOutside: false,
            },
          ).then((willSuccess) => {
            if (willSuccess) {
              start();
            }
          });
        }
      });
    } else {
      navigator.getUserMedia(
        { audio: true },
        () => {
          console.log('Permission Granted');
          dispatch(
            audioPermission({
              blocked: false,
            }),
          );
          swal(
            <div>
              <p>
                Permission Granted. Please click on OK button below to continue.
              </p>
            </div>,
            {
              icon: 'info',
              closeOnEsc: false,
              closeOnClickOutside: false,
            },
          ).then((willSuccess) => {
            if (willSuccess) {
              start();
            }
          });
        },
        () => {
          console.log('Permission Denied');
          dispatch(
            audioPermission({
              blocked: true,
            }),
          );
          swal(
            <div>
              <p>
                You have blocked the permission to record audio. Please provide
                necessary permissions manually.
              </p>
            </div>,
            {
              icon: 'info',
              // closeOnEsc: false,
              // closeOnClickOutside: false,
            },
          );
        },
      );
    }
  };

  const start = () => {
    Mp3Recorder.start()
      .then(() => {
        setisRecording(true);
        logEvent(firebaseAnalytics, 'Record_Voice_Message');
      })
      .catch((e) => console.error(e));
  };

  const stop = () => {
    Mp3Recorder.stop()
      .getMp3()
      .then(([buffer, blob]) => {
        console.log('blob', blob);
        console.log('buffer', buffer);
        const blobURL = URL.createObjectURL(blob);
        dispatch(
          saveBlob({
            blob: blobURL,
          }),
        );
        setisRecording(false);

        // Code to upload audio to s3
        let fileName = Date.now().toString() + '_recording.mp3';
        let file = new File([blob], fileName, {
          type: blob.type,
        });

        uploadFile(file, s3config)
          .then((data) => {
            console.log('Data', data);
            dispatch(
              saveAudioData({
                url: data.location,
                wavAudioKey: data.key,
              }),
            );
          })
          .catch((err) => console.error(err));
      })
      .catch((e) => console.log(e));
  };

  const play = () => {
    var audioPlayer = document.getElementById('player');
    var audio2 = document.getElementById('beep2');
    audioPlayer.volume = 1.0;
    audio2.volume = 1.0;

    if (isPlayingAudio === true) {
      audioPlayer.pause();
      audioPlayer.currentTime = 0;
      if (music.songSelected === true) {
        audio2.pause();
        audio2.currentTime = 0;
      }
      setAudioPlay(false);
    } else {
      setAudioPlay(true);
      audioPlayer.volume = 1.0;
      audio2.volume = 1.0;

      audioPlayer.play();
      audioPlayer.onended = function () {
        audioPlayer.pause();
        audioPlayer.currentTime = 0;

        if (music.songSelected === true) {
          audio2.play();
          return (audio2.onended = function () {
            setAudioPlay(false);
          });
        } else {
          setAudioPlay(false);
        }
      };
    }
  };

  const handleNext = () => {
    setAudioPlay(false);
    dispatch(changeTab({ tabId: 3 }));
  };

  return (
    <div className="container">
      <div className="d-flex my-4 w-100">
        <button
          className="btn app-btn mr-3"
          onClick={() => handleStart()}
          disabled={isRecording}
        >
          Record
        </button>
        <button
          className="btn app-btn mr-3"
          onClick={() => stop()}
          disabled={!isRecording}
        >
          Stop
        </button>
        <button
          className="btn app-btn mr-3"
          onClick={() => play()}
          disabled={blobURL === ''}
        >
          Play/Stop
        </button>
        <button
          type="button"
          name="next"
          className="btn app-btn mr-3"
          style={{ float: 'none' }}
          onClick={() => handleNext()}
        >
          Next
        </button>
        <button
          type="button"
          name="record"
          className="btn app-btn"
          onClick={() => {
            dispatch(recordingSkipped());
            logEvent(firebaseAnalytics, 'Skip_Voice_Message');
            dispatch(changeTab({ tabId: 3 }));
            dispatch(resetState());
          }}
        >
          Skip
        </button>
      </div>
      <div className="d-flex w-100">
        {isRecording && <img src={audioImg} alt="audio" />}
      </div>
      <div className="d-flex w-100">
        {isPlayingAudio && <img src={audioImg} alt="audio" />}
      </div>

      {blobURL != '' && (
        <>
          <label>Recorded Message: </label>
          <audio id="player" controls className="mr-3">
            <source src={blobURL} />
            Your browser does not support the audio element.
          </audio>

          <label>Selected Music: </label>
          <audio id="beep2" controls>
            <source src={music.song} />
            Your browser does not support the audio element.
          </audio>
        </>
      )}
    </div>
  );
}
