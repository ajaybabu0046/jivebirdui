import React, { useState, useEffect } from 'react';
import './style.css';
import RecordBird from '@assets/RecordMessage.png';
import RecordingBird from '@assets/RecordingBird.png';
import {
  start,
  stop,
  resetState,
  recordingSkipped,
  saveAudioData,
  saveBlob,
  audioPermission,
} from '../actions';
import { changeTab } from '../../CreateJiveBird/actions';
import { uploadFile } from 'react-s3';
import AudioReactRecorder, { RecordState } from 'audio-react-recorder';
import useRecorder from '../../../hooks/useRecorder';
import swal from '@sweetalert/with-react';
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';
import { firebaseAnalytics } from '../../../firebaseConfig';
import { logEvent } from 'firebase/analytics';

const s3config = {
  bucketName: 'jivebirdbucketprod',
  region: 'eu-west-2',
  accessKeyId: 'AKIAWRPJOMVGRRNNDFWJ',
  secretAccessKey: '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',
  dirName: 'music/jbs',
};

const RecordMessageTemp = (props) => {
  let [
    startRecording,
    isRecording,
    error,
    permissionGranted,
    setIsRecording,
    stopRecording,
  ] = useRecorder();

  const { dispatch, data, music } = props;
  const {
    recordingState,
    blobURL,
    s3Url,
    audioTimer,
    audioProgress,
    isBlocked,
  } = data;

  const [timer, settimer] = useState(audioTimer);
  const [progress, setprogress] = useState(audioProgress);
  const [isPlayingAudio, setAudioPlay] = useState(false);
  const [recordState, setrecordState] = useState(null);
  const [totalTime, settotalTime] = useState(0);
  const [count, setCount] = useState(0);

  useEffect(() => {
    if (isPlayingAudio) {
      var timerID = setInterval(() => tick(), 1000);

      return function cleanup() {
        clearInterval(timerID);
      };
    }
  }, [isPlayingAudio, totalTime]);

  function tick() {
    settotalTime(totalTime + 1);
  }

  useEffect(() => {
    if (recordingState === 1) {
      if (timer === 30) {
        handleStop();
        return;
      }

      setTimeout(() => {
        settimer(timer + 1);
        setprogress(progress + 6.67);
      }, 1000);
    }
  }, [recordingState, timer]);

  useEffect(() => {
    if (isRecording && permissionGranted) {
      logEvent(firebaseAnalytics, 'Record_Voice_Message');
      setrecordState(RecordState.START);
      dispatch(start());
    }
  }, [isRecording, permissionGranted]);

  const handleStart = () => {
    var isApple = /iPad|iPhone|Mac|iPod/i.test(navigator.userAgent);

    if (isApple) {
      startRecording();
    } else {
      navigator.getUserMedia(
        { audio: true },
        () => {
          console.log('Permission Granted');
          dispatch(
            audioPermission({
              blocked: false,
            }),
          );

          if (count === 0) {
            setCount(count + 1);
            swal(
              <div>
                <p>
                  Permission Granted. Please tap Record button again to
                  continue.
                </p>
              </div>,
              {
                icon: 'info',
              },
            );
            return;
          } else {
            logEvent(firebaseAnalytics, 'Record_Voice_Message');
            setrecordState(RecordState.START);
            dispatch(start());
          }
        },
        () => {
          dispatch(
            audioPermission({
              blocked: true,
            }),
          );
          swal(
            <div>
              <p>
                You have blocked the permission to record audio. Please provide
                necessary permissions manually.
              </p>
            </div>,
            {
              icon: 'info',
              // closeOnEsc: false,
              // closeOnClickOutside: false,
            },
          ).then((willSuccess) => {
            if (willSuccess) {
              // window.location.reload();
            }
          });
        },
      );
    }
  };

  const handleStop = () => {
    // stopRecording();
    setIsRecording(false);
    setrecordState(RecordState.STOP);
    dispatch(
      stop({
        audioTimer: timer,
        audioProgress: progress,
      }),
    );
  };

  const onStop = (audioData) => {
    dispatch(
      saveBlob({
        blob: audioData.url,
      }),
    );

    // Code to upload audio to s3
    let fileName = Date.now().toString() + '_recording.wav';
    let file = new File([audioData.blob], fileName, {
      type: audioData.type,
    });

    uploadFile(file, s3config)
      .then((data) => {
        // setAudioPlay(false);
        dispatch(
          saveAudioData({
            url: data.location,
            wavAudioKey: data.key,
          }),
        );
      })
      .catch((err) => console.error(err));
  };

  const playRecording = () => {
    var audioPlayer = document.getElementById('player');
    var audio2 = document.getElementById('beep2');
    audioPlayer.volume = 1.0;
    audio2.volume = 1.0;

    if (isPlayingAudio === true) {
      audioPlayer.pause();
      audioPlayer.currentTime = 0;
      if (music.songSelected === true) {
        audio2.pause();
        audio2.currentTime = 0;
      }
      setAudioPlay(false);
      settotalTime(0);
    } else {
      setAudioPlay(true);
      audioPlayer.volume = 1.0;
      audio2.volume = 1.0;

      audioPlayer.play();
      audioPlayer.onended = function () {
        audioPlayer.pause();
        audioPlayer.currentTime = 0;

        if (music.songSelected === true) {
          audio2.play();
          return (audio2.onended = function () {
            setAudioPlay(false);
            settotalTime(0);
          });
        } else {
          setAudioPlay(false);
          settotalTime(0);
        }
      };
    }
  };

  const handleNext = () => {
    setrecordState(null);
    setAudioPlay(false);
    dispatch(changeTab({ tabId: 3 }));
  };

  const handleVol = (val) => {
    let recording = document.getElementById('player1');
    let music = document.getElementById('beep22');
    recording.volume = val;
    music.volume = val;
  };

  return (
    <div className="container-fluid mt-3">
      <div className="row eCardRow sticky1">
        <h1 className="mainHeading head1">
          Record Your <span style={{ color: '#734385' }}>Message</span>
        </h1>
        <div className="ml-auto head2">
          {recordingState === 0 ? (
            <>
              <button
                type="button"
                neme="record"
                className="btn app-btn mt-2"
                onClick={() => {
                  dispatch(recordingSkipped());
                  logEvent(firebaseAnalytics, 'Skip_Voice_Message');
                  dispatch(changeTab({ tabId: 3 }));
                  dispatch(resetState());
                }}
              >
                Skip
              </button>
              <button
                type="button"
                neme="record"
                className="btn app-btn mr-3 mt-2"
                onClick={() => handleStart()}
              >
                Record
              </button>
            </>
          ) : recordingState === 1 ? (
            <button
              type="button"
              neme="record"
              className="btn app-btn mr-3 mt-2"
              onClick={() => handleStop()}
            >
              Stop
            </button>
          ) : (
            <>
              <button
                id="play-stop"
                type="button"
                neme="record"
                className="btn app-btn mt-2 mr-3"
                style={{ float: 'none' }}
                onClick={() => playRecording()}
              >
                {isPlayingAudio === true ? 'Stop' : 'Play'}
              </button>
              <button
                type="button"
                name="next"
                className="btn app-btn mt-2 mr-3"
                style={{ float: 'none' }}
                onClick={() => {
                  settimer(0);
                  setprogress(0);
                  setAudioPlay(false);
                  setrecordState(null);
                  // setIsRecording(false);
                  dispatch(resetState());
                }}
              >
                Re-Record
              </button>
              <button
                type="button"
                name="next"
                className="btn app-btn mt-2"
                style={{ float: 'none' }}
                onClick={() => handleNext()}
              >
                Next
              </button>
            </>
          )}
        </div>
      </div>

      <div className="row">
        <div className="col-lg-6 pl-0 Para">
          {recordingState === 0 ? (
            <h2 className="subHeading">
              Record or re-record a message up to 30 seconds, which will be
              linked to your chosen music track...
            </h2>
          ) : recordingState === 1 ? (
            <>
              <label
                htmlFor="file"
                className="d-flex justify-content-center timer"
                style={{ marginTop: '3rem' }}
              >
                {timer}
              </label>
              <div className="progress recorder-bg">
                <div
                  id="audio-progress"
                  className="progress-bar recorder-progress"
                  role="progressbar"
                  aria-valuenow={timer}
                  aria-valuemin={0}
                  aria-valuemax={30}
                  style={{ width: `${progress}%` }}
                ></div>
              </div>
              <h3 className="mt-3" style={{ fontWeight: 'bold' }}>
                <span style={{ color: '#734385' }}>of 30 Seconds</span>
              </h3>
            </>
          ) : (
            <>
              <label
                htmlFor="file"
                className="d-flex justify-content-center timer"
                style={{ marginTop: '3rem' }}
              >
                {isPlayingAudio ? totalTime : timer}
              </label>
              <div className="progress recorder-bg">
                <div
                  className="progress-bar recorder-progress"
                  role="progressbar"
                  aria-valuenow={timer}
                  aria-valuemin={0}
                  aria-valuemax={30}
                  style={{ width: `${progress}%` }}
                ></div>
              </div>

              <audio id="player" style={{ display: 'none' }}>
                <source src={blobURL} />
                Your browser does not support the audio element.
              </audio>
              <audio id="beep2" style={{ display: 'none' }}>
                <source src={music.song} />
                Your browser does not support the audio element.
              </audio>
              {isPlayingAudio === false && (
                <h3 className="mt-3" style={{ fontWeight: 'bold' }}>
                  <span style={{ color: '#734385' }}>
                    {timer} Seconds Recorded
                  </span>
                </h3>
              )}
            </>
          )}
        </div>
        <div className="col-lg-6 textCenter">
          {recordingState === 0 ? (
            <img src={RecordBird} alt="chirp-chirp" className="commonBird" />
          ) : (
            <img
              className="commonBird"
              src={RecordingBird}
              alt="recordingBird"
            />
          )}
        </div>
      </div>
      <div className="d-none">
        <AudioReactRecorder state={recordState} onStop={onStop} />
      </div>

      {/* {blobURL != '' &&
        <>
          <audio id="player1" controls>
            <source src={blobURL} />
            Your browser does not support the audio element.
          </audio>
          <audio id="beep22" controls>
            <source src={music.song} />
            Your browser does not support the audio element.
          </audio>
          <button type='button' onClick={(e) => handleVol(0.2)}>Set Vol to 0.2</button>
          <button type='button' onClick={(e) => handleVol(1.0)}>Set Vol to 1.0</button>
        </>} */}
    </div>
  );
};

export default RecordMessageTemp;
