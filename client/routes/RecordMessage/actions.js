import { createActionTypes } from '@/lib/utils/helper';

export const recordingActions = createActionTypes('chooseMusicCategory', [
  'START_RECORDING',
  'STOP_RECORDING',
  'RESET_STATE',
  'SAVE_AUDIO_DATA',
  'SAVE_BLOB',
  'CHECK_PERMISSION',
  'SKIPPED',
]);

export function recordingSkipped() {
  return {
    type: recordingActions.SKIPPED,
  };
}

export function audioPermission(obj) {
  return {
    type: recordingActions.CHECK_PERMISSION,
    payload: {
      ...obj,
    },
  };
}

export function start() {
  return {
    type: recordingActions.START_RECORDING,
  };
}

export function stop(obj) {
  return {
    type: recordingActions.STOP_RECORDING,
    payload: {
      ...obj,
    },
  };
}

export function resetState() {
  return {
    type: recordingActions.RESET_STATE,
  };
}

export function saveAudioData(obj) {
  return {
    type: recordingActions.SAVE_AUDIO_DATA,
    payload: {
      ...obj,
    },
  };
}

export function saveBlob(obj) {
  return {
    type: recordingActions.SAVE_BLOB,
    payload: {
      ...obj,
    },
  };
}
