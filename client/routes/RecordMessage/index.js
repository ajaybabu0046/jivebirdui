import React, { useEffect } from 'react';
import { func } from 'prop-types';
import { connect } from 'react-redux';
import { useInjectReducer } from '../../reducers/injectReducer';
import { recordingReducer } from './reducer';
import { chooseSongReducer } from '../SongsList/reducer';
import RecordMessageTemp from './component';
import { audioPermission } from './actions';

const RecordMessage = (props) => {
  const { dispatch } = props;
  useInjectReducer({
    key: 'recordingReducer',
    reducer: recordingReducer,
  });

  useInjectReducer({
    key: 'chooseSongReducer',
    reducer: chooseSongReducer,
  });

  // useEffect(() => {
  //   navigator.getUserMedia({ audio: true },
  //     () => {
  //       console.log('Permission Granted');
  //       dispatch(audioPermission({
  //         blocked: false
  //       }))
  //     },
  //     () => {
  //       console.log('Permission Denied');
  //       dispatch(audioPermission({
  //         blocked: true
  //       }))
  //     },
  //   );
  // }, [])

  return <RecordMessageTemp {...props} />;
};

RecordMessage.propTypes = {
  dispatch: func.isRequired,
};

RecordMessage.defaultProps = {
  data: {
    recordingState: 0,
    audioTimer: 0,
    audioProgress: 0,
    isBlocked: true,
  },
  music: {
    song: '',
  },
};

function mapStateToProps(props) {
  return {
    data: props.recordingReducer,
    music: props.chooseSongReducer,
  };
}

export default connect(mapStateToProps)(RecordMessage);
