import { createJBActions } from './actions';

export const initialState = {
  currentTab: 0,
};

export const createJiveBirdReducer = (state = initialState, action) => {
  const { CHANGE_CURRENT_TAB } = createJBActions;

  switch (action.type) {
    case CHANGE_CURRENT_TAB:
      return {
        ...state,
        currentTab: action.payload.tabId,
        MyAccount: action.payload.MyAccount,
        loginPage: action.payload.logIn,
        TokenId: action.payload.token,
      };

    default:
      return state;
  }
};
