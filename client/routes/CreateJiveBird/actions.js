import { createActionTypes } from '@/lib/utils/helper';

export const createJBActions = createActionTypes('createJiveBird', [
  'CHANGE_CURRENT_TAB',
]);

export function changeTab(obj) {
  return {
    type: createJBActions.CHANGE_CURRENT_TAB,
    payload: {
      ...obj,
    },
  };
}
