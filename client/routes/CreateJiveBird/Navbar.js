import React, { useState } from 'react';
import './index.css';
import birdLogo from '@assets/logo.png';
import { changeTab } from './actions';
import swal from '@sweetalert/with-react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import ExampleIcon from '@assets/examples.png';
import logojb from '@assets/TextLogo.png';

const headerTabs = [
  {
    id: 0,
    title: 'Home',
    icon: 'fas fa-home',
  },
  {
    id: 1,
    title: 'Music',
    icon: 'fas fa-music',
  },
  {
    id: 2,
    title: 'Record',
    icon: 'fas fa-microphone',
  },
  {
    id: 3,
    title: 'Card',
    icon: 'fa fa-heart',
  },
  {
    id: 4,
    title: 'Gift',
    icon: 'fas fa-gift',
  },
  {
    id: 5,
    title: 'When',
    icon: 'fas fa-paper-plane',
  },
];

export default function Navbar(props) {
  const [show, setShow] = useState(false);

  const closesidebar = () => {
    setShow(false);
  };

  const { dispatch, currentTab } = props;
  const bird = () => {
    swal(
      <div>
        <p>Do you wish to go back? All your saved progress will be lost.</p>
      </div>,
      {
        icon: 'info',
        buttons: ['No', 'Yes'],
        dangerMode: true,
        closeOnEsc: false,
        closeOnClickOutside: false,
      },
    ).then((willSuccess) => {
      if (willSuccess) {
        window.location.reload(true);
      }
    });
  };
  const handleCurrentScreen = (id, MyAccount) => {
    dispatch(changeTab({ tabId: id, MyAccount: 'MyAccount' }));
    setShow(false);
  };

  const handleChangeTab = (id) => {
    dispatch(changeTab({ tabId: id }));
  };

  const handleLogout = () => {
    localStorage.clear();
    window.location.reload(true);
  };

  return (
    <>
      <nav
        className="navbar navbar-expand-lg navbar-light bg-light sticky"
        role="navigation"
      >
        <a
          className="navbar-brand d-block d-lg-none"
          role="button"
          onClick={() => bird()}
          style={{ cursor: 'pointer' }}
        >
          <img style={{ width: 'auto', height: '70px' }} src={birdLogo} />
        </a>
        <div
          className="collapse navbar-collapse myNavbar px-5"
          id="navbarTogglerDemo01"
        >
          <a
            className="navbar-brand"
            role="button"
            onClick={() => bird()}
            style={{ cursor: 'pointer' }}
          >
            <img style={{ width: 'auto', height: '70px' }} src={birdLogo} />
          </a>
          <div className="form-inline ml-auto">
            <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
              {headerTabs.map((eachTab) => (
                <div className="d-flex justify-content-center" key={eachTab.id}>
                  <li
                    className="nav-item"
                    onClick={() => handleChangeTab(eachTab.id)}
                  >
                    <span className="nav-link">
                      <i
                        className={`${eachTab.icon} fa-lg ${
                          eachTab.id === 3 && 'cardWithHeart'
                        }`}
                      ></i>
                    </span>
                    <span
                      className={
                        eachTab.id <= currentTab
                          ? 'nav-link active'
                          : 'nav-link'
                      }
                    >
                      {eachTab.title}
                    </span>
                  </li>

                  {eachTab.id < 5 && (
                    <i className="fas fa-chevron-right fa-lg bradcrumb"></i>
                  )}
                </div>
              ))}
              <li className="navbar nav-item d-flex justify-content-center align-items-center">
                <Button
                  variant="link"
                  onClick={() => setShow(true)}
                  className="p-0 d-lg-block d-md-none"
                >
                  <i
                    className="fas fa-ellipsis-v fa-2x menu-bars"
                    style={{ color: 'white' }}
                  ></i>
                </Button>
              </li>
            </ul>
            <Modal
              show={show}
              onHide={() => setShow(false)}
              contentClassName="custom-modal-style"
              dialogClassName="modal-90w"
              aria-labelledby="example-custom-modal-styling-title"
            >
              <Modal.Header className="ModalHead">
                <Modal.Title id="example-custom-modal-styling-title">
                  <h2>
                    <i
                      className="fas fa-chevron-left sidebarhead"
                      onClick={closesidebar}
                    ></i>
                    <img style={{ width: '164px' }} src={logojb} />
                  </h2>
                </Modal.Title>
              </Modal.Header>
              <Modal.Body className="CustomModalBody">
                <a className="d-block d-lg-none" role="button">
                  <p onClick={() => handleCurrentScreen(0)}>
                    <i
                      className="fas fa-home sidebaricon"
                      style={{ color: '#d6b7d8', fontSize: '25px' }}
                    ></i>
                    Home
                  </p>
                </a>
                <a className="d-block d-lg-none" role="button">
                  <p onClick={() => handleCurrentScreen(1)}>
                    <i
                      className="fas fa-music sidebaricon"
                      style={{ color: '#d6b7d8', fontSize: '25px' }}
                    ></i>
                    Music
                  </p>
                </a>
                <a className="d-block d-lg-none" role="button">
                  <p onClick={() => handleCurrentScreen(2)}>
                    <i
                      className="fas fa-microphone sidebaricon"
                      style={{ color: '#d6b7d8', fontSize: '25px' }}
                    ></i>
                    Record
                  </p>
                </a>
                <a className="d-block d-lg-none" role="button">
                  <p onClick={() => handleCurrentScreen(3)}>
                    <i
                      className="fas fa-heart sidebaricon"
                      style={{ color: '#d6b7d8', fontSize: '25px' }}
                    ></i>
                    Card
                  </p>
                </a>
                <a className="d-block d-lg-none" role="button">
                  <p onClick={() => handleCurrentScreen(4)}>
                    <i
                      className="fas fa-gift sidebaricon"
                      style={{ color: '#d6b7d8', fontSize: '25px' }}
                    ></i>
                    Gift
                  </p>
                </a>
                <a className="d-block d-lg-none" role="button">
                  <p onClick={() => handleCurrentScreen(5)}>
                    <i
                      className="fas fa-paper-plane sidebaricon"
                      style={{ color: '#d6b7d8', fontSize: '25px' }}
                    ></i>
                    When
                  </p>
                </a>
                <hr className="d-block d-lg-none" />
                <a role="button">
                  <p
                    onClick={
                      localStorage.getItem('authToken') != undefined
                        ? () => handleCurrentScreen(11)
                        : () => handleCurrentScreen(6, 'MyAccount')
                    }
                  >
                    <i
                      className="fas fa-user-circle sidebaricon"
                      style={{ color: '#d6b7d8', fontSize: '25px' }}
                    ></i>
                    My Account
                  </p>
                </a>
                <a role="button">
                  <p onClick={() => handleCurrentScreen(8)}>
                    <i>
                      {' '}
                      <img className="mr-4" src={ExampleIcon} />
                    </i>
                    Examples
                  </p>
                </a>
                <a role="button">
                  <p onClick={() => handleCurrentScreen(10)}>
                    <i
                      className="fas fa-question-circle sidebaricon"
                      style={{ color: '#d6b7d8', fontSize: '25px' }}
                    ></i>
                    FAQ's
                  </p>
                </a>
                <a role="button">
                  <p onClick={() => handleCurrentScreen(9)}>
                    <i
                      className="fas fa-info-circle sidebaricon"
                      style={{ color: '#d6b7d8', fontSize: '25px' }}
                    ></i>
                    About
                  </p>
                </a>
                <a role="button">
                  <p onClick={() => handleCurrentScreen(0)}>
                    <i
                      className="fas fa-info-circle sidebaricon"
                      style={{ color: '#d6b7d8', fontSize: '25px' }}
                    ></i>
                    What is a JB?
                  </p>
                </a>
                <br />

                <a role="button">
                  <p onClick={() => handleCurrentScreen(7)}>
                    <i
                      className="fas fa-comment-alt sidebaricon"
                      style={{ color: '#d6b7d8', fontSize: '25px' }}
                    ></i>
                    Contact
                  </p>
                </a>
                <a
                  href="https://www.jivebird.com/terms-conditions/"
                  target="_blank"
                >
                  <p>
                    <i
                      className="fas fa-info-circle sidebaricon"
                      style={{ color: '#d6b7d8', fontSize: '25px' }}
                    ></i>
                    Terms & Conditions
                  </p>
                </a>
                {localStorage.getItem('authToken') != undefined && (
                  <a role="button" onClick={() => handleLogout()}>
                    <p>
                      <i
                        className="fas fa-sign-out-alt fa-rotate-180 sidebaricon"
                        style={{ color: '#d6b7d8', fontSize: '25px' }}
                      ></i>
                      Log-out
                    </p>
                  </a>
                )}
              </Modal.Body>
            </Modal>
          </div>
        </div>
        <Button
          variant="link"
          onClick={() => setShow(true)}
          className="p-0 d-lg-none"
        >
          <i
            className="fas fa-ellipsis-v fa-2x menu-bars"
            style={{ color: 'white' }}
          ></i>
        </Button>
      </nav>
    </>
  );
}
