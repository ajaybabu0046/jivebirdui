import React, { useEffect } from 'react';
import { func } from 'prop-types';
import { connect } from 'react-redux';
import { useInjectReducer } from '../../reducers/injectReducer';
import { createJiveBirdReducer } from './reducer';
import Navbar from './Navbar';
import CreateJiveTemp from './component';
import ChooseMusic from '../ChooseMusic';
import RecordMessage from '../RecordMessage';
import ChooseEcard from '../ChooseEcard';
import AddEgift from '../AddEgift';
import SendECard from '../SendEcard';
import NewUser1 from '../NewUser';
import Contact from '../Contact';
import Example from '../Example';
import About from '../About';
import Faq from '../Faq';
import PersonalInfo from '../PersonalInfo';
import AccountHis from '../AccountHistory';
import { firebaseAnalytics } from '../../firebaseConfig';
import { logEvent } from 'firebase/analytics';

const CreateJive = (props) => {
  const { jBdata, dispatch } = props;
  const { currentTab, music } = jBdata;

  useInjectReducer({
    key: 'createJiveBirdReducer',
    reducer: createJiveBirdReducer,
  });

  useEffect(() => {
    logEvent(firebaseAnalytics, 'homepage_visited');
  }, []);

  return (
    <>
      {/* {currentTab != 6 && ( */}
      <Navbar dispatch={dispatch} currentTab={currentTab} />
      {/* )} */}
      {/* Changing Between Tabs here */}
      {currentTab === 0 && <CreateJiveTemp dispatch={dispatch} />}
      {currentTab === 1 && <ChooseMusic music={music} dispatch={dispatch} />}
      {currentTab === 2 && <RecordMessage dispatch={dispatch} />}
      {currentTab === 3 && <ChooseEcard dispatch={dispatch} />}
      {currentTab === 4 && <AddEgift dispatch={dispatch} />}
      {currentTab === 5 && <SendECard dispatch={dispatch} />}
      {currentTab === 6 && <NewUser1 dispatch={dispatch} />}
      {currentTab === 7 && <Contact dispatch={dispatch} />}
      {currentTab === 8 && <Example dispatch={dispatch} />}
      {currentTab === 9 && <About dispatch={dispatch} />}
      {currentTab === 10 && <Faq dispatch={dispatch} />}
      {currentTab === 11 && <AccountHis dispatch={dispatch} />}
      {currentTab === 12 && <PersonalInfo dispatch={dispatch} />}
    </>
  );
};

CreateJive.propTypes = {
  dispatch: func.isRequired,
};

CreateJive.defaultProps = {
  jBdata: {
    currentTab: 0,
  },
};

function mapStateToProps(props) {
  return {
    jBdata: props.createJiveBirdReducer,
  };
}

export default connect(mapStateToProps)(CreateJive);
