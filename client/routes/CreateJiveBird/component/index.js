import React, { useState } from 'react';
import './style.css';
import BirdCard from './BirdCard';
import { changeTab } from '../actions';
import Modal from 'react-bootstrap/Modal';
import { firebaseAnalytics } from '../../../firebaseConfig';
import { logEvent } from 'firebase/analytics';

export default function index(props) {
  const { dispatch } = props;
  const [open, setOpen] = useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <>
      <div className="BackgroundOfFirstPage">
        <div className="container-fluid p-0">
          <div className="d-flex justify-content-center align-items-center pt-3 section1 row1 sticky">
            <h1 className="mb-0 head1">
              Create a <span>JiveBird!</span>
            </h1>
            <div className="ml-auto headRight head2">
              <h2
                className="TxtBtn mr-3"
                style={{ cursor: 'pointer' }}
                onClick={() => handleOpen()}
              >
                What is a JB?
              </h2>

              <button
                className="btn homebutn head2"
                onClick={() => dispatch(changeTab({ tabId: 1 }))}
              >
                Next
              </button>
            </div>
          </div>
          <div className="d-flex mobile mt-3 justify-content-center">
            <div className="circle">
              <p className="freetext">
                First 2 JiveBirds
                <span> FREE!</span>
              </p>
            </div>
          </div>
          <div className="row banner">
            <div className="col">
              <BirdCard
                dispatch={dispatch}
                number={1}
                image={`${process.env.REACT_APP_WEB_ASSETS}/pick.png`}
                text1="Pick"
                text2="a hit or popular song"
                currentTab={1}
              />
            </div>
            <div className="col">
              <BirdCard
                dispatch={dispatch}
                number={2}
                image={`${process.env.REACT_APP_WEB_ASSETS}/record.png`}
                text1="Record"
                text2="your voice message"
                currentTab={1}
              />
            </div>
          </div>
          <div className="row desktop mt-3 justify-content-center">
            <div className="circle">
              <p className="freetext">
                First 2<br />
                JiveBirds
                <br />
                <span>FREE!</span>
              </p>
            </div>
          </div>
          <div className="row mt-3">
            <div className="col">
              <BirdCard
                dispatch={dispatch}
                number={3}
                image={`${process.env.REACT_APP_WEB_ASSETS}/personalise.png`}
                text1="Personalise"
                text2="your eCard or take a selfie for a photocard"
                currentTab={1}
              />
            </div>
            <div className="col">
              <BirdCard
                dispatch={dispatch}
                number={4}
                image={`${process.env.REACT_APP_WEB_ASSETS}/plus.png`}
                text1="Plus"
                text2="option to choose a great eGift Voucher"
                currentTab={1}
              />
            </div>
          </div>
        </div>

        {/* Modal Start */}
        <Modal
          show={open}
          onHide={() => handleClose()}
          size="lg"
          dialogClassName=""
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title className="TxtBtn"> What is a JB?</Modal.Title>
          </Modal.Header>

          <Modal.Body
            className="bodyModal"
            style={{ backgroundColor: '#44296b' }}
          >
            <video
              id="vid"
              className="video-fluid z-depth-1"
              controls
              controlsList="nodownload nofullscreen noremoteplayback"
              // autoPlay
              // muted
              onEnded={() => {
                logEvent(firebaseAnalytics, 'IntroVideo_WatchedFull');
                // history.push('/create-jive-bird')
              }}
            >
              <source
                src="https://jivebirdbucketprod.s3.eu-west-2.amazonaws.com/webapp-assets/JiveBirdvideo.mp4"
                type="video/mp4"
              />
            </video>
          </Modal.Body>
        </Modal>

        {/* End */}
      </div>
    </>
  );
}
