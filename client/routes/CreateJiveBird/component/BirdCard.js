import React from 'react';
import './style.css';
import { changeTab } from '../actions';
export default function BirdCard({
  dispatch,
  number,
  image,
  text1,
  text2,
  text3,
  currentTab,
}) {
  return (
    <div className="brd-card">
      <div className="brd-icon ">
        <div className="numbr " style={{ cursor: 'pointer' }}>
          {number}
        </div>
        <div
          className="d-flex"
          style={{ cursor: 'pointer' }}
          onClick={() => dispatch(changeTab({ tabId: currentTab }))}
          // style={{ cursor: 'default' }}
        >
          <img style={{ width: '130px', height: '130px' }} src={image}></img>
          <p>
            <span>{text1}</span> {text2} <br /> {text3}
          </p>
        </div>
      </div>
    </div>
  );
}
