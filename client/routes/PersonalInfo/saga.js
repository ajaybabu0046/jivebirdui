import { call, put, takeLatest } from 'redux-saga/effects';
import { apiResource } from '@/lib/utils/api';

import {
  PersonalInfoAction,
  PersonalInfoSuccess,
  PersonalInfoFailed,
  updateUserProfileSuccess,
  updateUserProfileFailed,
} from './action';

function* personalInfoVerify(dObj) {
  try {
    const url = '/credits/';
    const res = yield call(apiResource.get, url);
    if (res) {
      yield put(
        PersonalInfoSuccess({
          PersonalInfoData: res,
        }),
      );
    } else {
      yield put(
        PersonalInfoFailed({
          PersonalInfoData: res,
        }),
      );
    }
  } catch (e) {
    yield put(
      PersonalInfoFailed({
        PersonalInfoData: 'something went wrong',
      }),
    );
  }
}

function* updateUserProfile(dObj) {
  try {
    const url = '/accounts/profile/';
    const res = yield call(apiResource.post, url, dObj.payload);
    if (res.id) {
      yield put(
        updateUserProfileSuccess({
          data: res,
        }),
      );
    } else {
      yield put(
        updateUserProfileFailed({
          data: res,
        }),
      );
    }
  } catch (e) {
    yield put(
      updateUserProfileFailed({
        data: 'something went wrong',
      }),
    );
  }
}

function* PersonalInfoSaga() {
  yield takeLatest(PersonalInfoAction.VERIFY_PERSONALINFO, personalInfoVerify);
  yield takeLatest(PersonalInfoAction.UPDATE_PROFILE, updateUserProfile);
}
export default PersonalInfoSaga;
