import { PersonalInfoAction } from './action';

export const initialState = {
  loading: true,
  message: '',
  response: {},
  updateRes: {},
};

export const PersonalInfoReducer = (state = initialState, action) => {
  const {
    VERIFY_PERSONALINFO,
    PERSONALINFO_SUCCESS,
    PERSONALINFO_FAILED,
    RESET_STATE,
    UPDATE_PROFILE,
    UPDATE_PROFILE_SUCCESS,
    UPDATE_PROFILE_FAILED,
  } = PersonalInfoAction;

  const { payload } = action;

  switch (action.type) {
    case VERIFY_PERSONALINFO:
      return {
        ...state,
        loading: true,
      };
    case PERSONALINFO_SUCCESS:
      return {
        ...state,
        loading: false,
        response: action.payload.PersonalInfoData,
      };
    case PERSONALINFO_FAILED:
      return {
        ...state,
        loading: false,
        response: action.payload.PersonalInfoData,
      };
    case UPDATE_PROFILE:
      return {
        ...state,
        loading: true,
      };
    case UPDATE_PROFILE_SUCCESS:
      return {
        ...state,
        loading: false,
        updateRes: action.payload.data,
      };
    case UPDATE_PROFILE_FAILED:
      return {
        ...state,
        loading: false,
        updateRes: action.payload.data,
      };
    case RESET_STATE:
      return {
        ...initialState,
        // message:""
      };

    default:
      return state;
  }
};
