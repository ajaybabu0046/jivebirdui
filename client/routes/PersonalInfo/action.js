import { createActionTypes } from '@/lib/utils/helper';

export const PersonalInfoAction = createActionTypes('PersonalInfo', [
  'RESET_STATE',
  'VERIFY_PERSONALINFO',
  'PERSONALINFO_SUCCESS',
  'PERSONALINFO_FAILED',

  'UPDATE_PROFILE',
  'UPDATE_PROFILE_SUCCESS',
  'UPDATE_PROFILE_FAILED',
]);

export function personalInfoVerify(obj) {
  return {
    type: PersonalInfoAction.VERIFY_PERSONALINFO,
    payload: {
      ...obj,
    },
  };
}

export function PersonalInfoSuccess(obj) {
  return {
    type: PersonalInfoAction.PERSONALINFO_SUCCESS,
    payload: {
      ...obj,
    },
  };
}

export function PersonalInfoFailed(obj) {
  return {
    type: PersonalInfoAction.PERSONALINFO_FAILED,
    payload: {
      ...obj,
    },
  };
}

export function updateUserProfile(obj) {
  return {
    type: PersonalInfoAction.UPDATE_PROFILE,
    payload: {
      ...obj,
    },
  };
}

export function updateUserProfileSuccess(obj) {
  return {
    type: PersonalInfoAction.UPDATE_PROFILE_SUCCESS,
    payload: {
      ...obj,
    },
  };
}

export function updateUserProfileFailed(obj) {
  return {
    type: PersonalInfoAction.UPDATE_PROFILE_FAILED,
    payload: {
      ...obj,
    },
  };
}

export function resetState() {
  return {
    type: PersonalInfoAction.RESET_STATE,
  };
}
