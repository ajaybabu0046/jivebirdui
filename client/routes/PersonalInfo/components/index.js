import React, { useState, useEffect } from 'react';
import './style.css';
import MyAccountToggling from '../../../components/MyAccountToggling';
import { resetState, updateUserProfile } from '../action';
import Loader from 'react-loader-spinner';
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';

const PersonalInfoTemp = (props) => {
  const { PersonalInfoData, dispatch } = props;

  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [credits, setCredits] = useState(0);
  const [firstNameError, setFirstNameError] = useState(false);
  const [lastNameError, setLastNameError] = useState(false);
  const [errorAllFields, setErrorAllFields] = useState('');

  useEffect(() => {
    if (
      PersonalInfoData &&
      PersonalInfoData.response &&
      PersonalInfoData.response.username
    ) {
      setFirstName(PersonalInfoData.response.first_name);
      setLastName(PersonalInfoData.response.last_name);
      setEmail(PersonalInfoData.response.username);
      setCredits(PersonalInfoData.response.credits);
    }
  }, [PersonalInfoData]);

  const handlePersonalInfo = () => {
    let errorFound = false;
    if (firstName == '' || lastName == '') {
      errorFound = true;
      setErrorAllFields('All fields are mandatory');
    }
    if (firstName || firstName.length === 0) {
      let data = firstName.trim(' ');
      if (data.length === 0) {
        errorFound = true;
        setErrorAllFields('Fill the first name');
        // setFirstNameError(true);
      }
    }
    if (lastName || lastName.length === 0) {
      let data = lastName.trim(' ');
      if (data.length === 0) {
        errorFound = true;
        setErrorAllFields('Fill the last name');
        // setLastNameError(true);
      }
    }
    // if (errorFound) dispatch(resetState());

    if (!errorFound) {
      dispatch(
        updateUserProfile({
          username: email,
          first_name: firstName.trim(),
          last_name: lastName.trim(),
          email: email,
          credits: credits,
        }),
      );
      dispatch(resetState());
    }
  };

  const addFirstName = (e) => {
    setFirstName(e.target.value);
    setErrorAllFields('');
    setFirstNameError(false);
    // dispatch(resetState());
  };
  const addLastName = (e) => {
    setLastName(e.target.value);
    setErrorAllFields('');
    setLastNameError(false);
    // dispatch(resetState());
  };
  return (
    <>
      <div className="container-fluid">
        <MyAccountToggling {...props} />
      </div>
      <div className="container PersonalInfocontainer d-flex justify-content-center align-items-center">
        {PersonalInfoData && PersonalInfoData.loading == false ? (
          <>
            <div className="PersonalInfoBody">
              <div className="row"></div>
              <div className="row">
                <div className="col-lg-6 mt-3">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="First name"
                    onChange={(e) => addFirstName(e)}
                    value={firstName}
                    onKeyPress={(event) => {
                      if (event.key === 'Enter') {
                        handlePersonalInfo();
                      }
                    }}
                  />
                </div>

                <div className="col-lg-6 mt-3">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Last name"
                    onChange={(e) => addLastName(e)}
                    value={lastName}
                    onKeyPress={(event) => {
                      if (event.key === 'Enter') {
                        handlePersonalInfo();
                      }
                    }}
                  />
                </div>

                <div className="col-lg-6 mt-3">
                  <input
                    type="email"
                    className="form-control emailicon"
                    placeholder="E-mail"
                    disabled="disabled"
                    value={email}
                    // onChange={(e) => addEmail(e)}
                    // onKeyPress={(event) => {
                    //   if (event.key === 'Enter') {
                    //     handlePersonalInfo();
                    //   }
                    // }}
                  />
                </div>
                <div className="col-lg-6 mt-3 ">
                  <div className="jivebirdleft mt-3">
                    <span>{credits}</span> JiveBird left
                  </div>
                </div>
              </div>
              <button
                type="button"
                className="btn btnSaveChanges mb-4"
                onClick={() => handlePersonalInfo()}
              >
                Save Changes
              </button>

              <div className="">{errorAllFields}</div>
            </div>
          </>
        ) : (
          <div className="loaderContainer">
            <Loader
              type="Bars"
              color="#734385"
              height={100}
              width={100}
              radius={30}
            />
          </div>
        )}
      </div>
    </>
  );
};

export default PersonalInfoTemp;
