import React, { useEffect, useState } from 'react';
import PersonalInfoTemp from './components';
import { shape, func } from 'prop-types';
import { connect } from 'react-redux';
import PersonalInfoSaga from './saga';
import { PersonalInfoReducer } from './reducer';
import { useInjectSaga } from '../../saga/injectSaga';
import { useInjectReducer } from '../../reducers/injectReducer';
import { personalInfoVerify } from './action';
const PersonalInfo = (props) => {
  const { PersonalInfoData, dispatch } = props;

  useInjectSaga({ key: 'PersonalInfoSaga', saga: PersonalInfoSaga });

  useInjectReducer({
    key: 'PersonalInfoReducer',
    reducer: PersonalInfoReducer,
  });
  useEffect(() => {
    dispatch(personalInfoVerify({}));
  }, []);

  return (
    <div>
      <PersonalInfoTemp
        {...props}
        PersonalInfoData={PersonalInfoData}
        dispatch={dispatch}
      />
    </div>
  );
};
PersonalInfo.propTypes = {
  PersonalInfoData: shape({}).isRequired,
  dispatch: func.isRequired,
};

PersonalInfo.defaultProps = {
  PersonalInfoData: {
    loading: false,
  },
};
function mapStateToProps(props) {
  return {
    PersonalInfoData: props.PersonalInfoReducer,
  };
}

export default connect(mapStateToProps)(PersonalInfo);
