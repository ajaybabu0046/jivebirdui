import React from 'react';
import RegisterTemp from './component';
import { shape, func } from 'prop-types';
import { connect } from 'react-redux';
import { useInjectSaga } from '../../saga/injectSaga';
import registerSaga from './saga';
import { registerReducer } from './reducer';
import { useInjectReducer } from '../../reducers/injectReducer';

const Register = (props) => {
  const { registerData, dispatch, setCurrentScreen, setLoginBtnToggle } = props;
  useInjectReducer({ key: 'registerReducer', reducer: registerReducer });
  useInjectSaga({ key: 'registerSaga', saga: registerSaga });

  return (
    <div>
      <RegisterTemp
        registerdata={registerData}
        setCurrentScreen={setCurrentScreen}
        setLoginBtnToggle={setLoginBtnToggle}
        dispatch={dispatch}
      />
    </div>
  );
};
Register.propTypes = {
  registerData: shape({}).isRequired,
  dispatch: func.isRequired,
};

Register.defaultProps = {
  registerData: {
    loading: false,
  },
};

function mapStateToProps(props) {
  return {
    registerData: props.registerReducer,
  };
}

export default connect(mapStateToProps)(Register);
