import { RegisterAction } from './action';

export const initialState = {
  loading: false,

  message: '',
  response: {},
};

export const registerReducer = (state = initialState, action) => {
  const {
    VERIFY_REGISTER_USER,
    REGISTER_SUCCESS,
    REGISTER_FAILED,
    RESET_STATE,
  } = RegisterAction;

  const { payload } = action;

  switch (action.type) {
    case VERIFY_REGISTER_USER:
      return {
        ...state,
        loading: true,

        // message: "",
      };
    case REGISTER_SUCCESS:
      return {
        ...state,
        loading: false,
        response: action.payload.registerData,
      };
    case REGISTER_FAILED:
      return {
        ...state,
        loading: false,

        response: action.payload.registerData,
      };
    case RESET_STATE:
      return {
        ...initialState,
      };

    default:
      return state;
  }
};
