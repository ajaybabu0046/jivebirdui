import { createActionTypes } from '@/lib/utils/helper';

export const RegisterAction = createActionTypes('Register', [
  'VERIFY_REGISTER_USER',
  'REGISTER_SUCCESS',
  'REGISTER_FAILED',
  'RESET_STATE',
]);

export function registerUserVerify(obj) {
  return {
    type: RegisterAction.VERIFY_REGISTER_USER,
    payload: {
      ...obj,
    },
  };
}

export function registerSuccess(obj) {
  return {
    type: RegisterAction.REGISTER_SUCCESS,
    payload: {
      ...obj,
    },
  };
}
export function registerFailed(obj) {
  return {
    type: RegisterAction.REGISTER_FAILED,
    payload: {
      ...obj,
    },
  };
}

export function resetState() {
  return {
    type: RegisterAction.RESET_STATE,
  };
}
