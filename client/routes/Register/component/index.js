import React from 'react';
import { useEffect, useState } from 'react';
import './style.css';
import { registerUserVerify, resetState } from '../action';
import swal from '@sweetalert/with-react';
import Checkbox from '@material-ui/core/Checkbox';

import { useForm } from 'react-hook-form';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEye } from '@fortawesome/free-solid-svg-icons';
const eye = <FontAwesomeIcon icon={faEye} />;
const RegisterTemp = (props) => {
  const { registerdata, dispatch, setCurrentScreen, setLoginBtnToggle } = props;
  const [checked, setChecked] = useState(false);
  const [Toggle, setToggle] = useState(false);
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const [firstNameError, setFirstNameError] = useState(false);
  const [lastNameError, setLastNameError] = useState(false);
  const [emailError, setEmailError] = useState(false);
  const [passwordError, setPasswordError] = useState(false);
  const [errorAllFields, setErrorAllFields] = useState('');
  const [loading, setLoading] = useState(false);

  const [passwordShown, setPasswordShown] = useState(false);
  const handleToggle = () => {
    setToggle(!Toggle);
  };
  const handleChange = (event) => {
    setChecked(event.target.checked);
  };

  useEffect(() => {
    if (
      registerdata &&
      registerdata.response &&
      registerdata.response.id != null
    ) {
      swal(
        <div>
          <p>Successfully Registered</p>
        </div>,
        {
          button: 'Yes',
          closeOnEsc: false,
          closeOnClickOutside: false,
        },
      ).then((willSuccess) => {
        if (willSuccess) {
          setLoginBtnToggle(true);
          dispatch(resetState());
          setCurrentScreen(1);
        }
      });
    }

    if (
      registerdata &&
      registerdata.response &&
      registerdata.response.password != undefined
    ) {
      setErrorAllFields(registerdata.response.password[0]);

      setLoading(false);
    }
    if (
      registerdata &&
      registerdata.response &&
      registerdata.response.username != undefined
    ) {
      setErrorAllFields(registerdata.response.username[0]);

      setLoading(false);
    }
  }, [registerdata]);

  const handleRegistration = () => {
    setLoading(true);
    dispatch(resetState());
    let errorFound = false;
    // const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    // const passwordRegx = /^(?=.*[a-z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,15}$/;
    if (firstName == '' || lastName == '' || email == '' || password == '') {
      errorFound = true;
      setErrorAllFields('All fields are mandatory');
      setLoading(false);
    }
    if (firstName || firstName.length === 0) {
      let data = firstName.trim(' ');
      if (data.length === 0) {
        errorFound = true;
        setErrorAllFields('All fields are mandatory');
        setFirstNameError(true);
      }
    }
    if (lastName || lastName.length === 0) {
      let data = lastName.trim(' ');
      if (data.length === 0) {
        errorFound = true;
        setErrorAllFields('All fields are mandatory');
        setLastNameError(true);
      }
    }
    if (email || email.length === 0) {
      let data = email.trim(' ');
      if (data.length === 0) {
        errorFound = true;
        setErrorAllFields('All fields are mandatory');
        setEmailError(true);
      }
    }
    // if (email) {
    //   if (!email.match(mailformat)) {
    //     errorFound = true;
    //     setErrorAllFields('Email is not valid');
    //     setEmailError(true);
    //   }
    // }
    if (password || password.length === 0) {
      let data = password.trim(' ');
      if (data.length === 0) {
        errorFound = true;
        setErrorAllFields('All fields are mandatory');
        setPasswordError(true);
      }
    }
    // if (password) {
    //   if (!password.match(passwordRegx)) {
    //     errorFound = true;
    //     setErrorAllFields('Password is not valid');
    //     setPasswordError(true);
    //   }
    // }

    if (errorFound) setLoading(false);

    if (!errorFound) {
      const emailId = email.toLowerCase();
      const PasswordFromUser = password;
      const finalPasswordFromUser = PasswordFromUser.toString();
      dispatch(
        registerUserVerify({
          email: emailId.trim(),
          first_name: firstName.trim(),
          last_name: lastName.trim(),
          password: password,
          // device_id: "54b3e7c700",
          username: emailId.trim(),
          password_confirm: password,
          // login:emailId.trim()
        }),
      );
      dispatch(resetState());
    }
  };
  const addFirstName = (e) => {
    setFirstName(e.target.value);
    setErrorAllFields('');
    setFirstNameError(false);
    dispatch(resetState());
  };
  const addLastName = (e) => {
    setLastName(e.target.value);
    setErrorAllFields('');
    setLastNameError(false);
    dispatch(resetState());
  };

  const addEmail = (e) => {
    setEmail(e.target.value);
    setErrorAllFields('');
    setEmailError(false);
    dispatch(resetState());
  };
  const addPassword = (e) => {
    setPassword(e.target.value);
    setErrorAllFields('');
    setPasswordError(false);
    dispatch(resetState());
  };
  const togglePasswordVisiblity = () => {
    setPasswordShown(passwordShown ? false : true);
  };
  return (
    <div className="container registercontainer  d-flex justify-content-center align-items-center ">
      <div className="registerBody mt-5">
        <div className="row">
          <div className="col">
            <h1 className="mt-3">Register</h1>
          </div>
        </div>

        <form>
          <div className="container">
            <div className="row">
              <div className="col-lg-6 mt-3">
                <input
                  type="text"
                  className={
                    firstNameError
                      ? 'form-control input-grouperror'
                      : 'form-control'
                  }
                  placeholder="First name"
                  onChange={(e) => addFirstName(e)}
                  onKeyPress={(event) => {
                    if (event.key === 'Enter') {
                      handleRegistration();
                    }
                  }}
                />
              </div>
              <div className="col-lg-6 mt-3">
                <input
                  type="text"
                  className={
                    lastNameError
                      ? 'form-control input-grouperror'
                      : 'form-control'
                  }
                  placeholder="Last name"
                  onChange={(e) => addLastName(e)}
                  onKeyPress={(event) => {
                    if (event.key === 'Enter') {
                      handleRegistration();
                    }
                  }}
                />
              </div>
              <div className="col-lg-6 mt-3">
                <input
                  type="email"
                  // emailicon
                  className={
                    emailError
                      ? 'form-control emailicon input-grouperror'
                      : 'form-control emailicon'
                  }
                  placeholder="E-mail"
                  onChange={(e) => addEmail(e)}
                  onKeyPress={(event) => {
                    if (event.key === 'Enter') {
                      handleRegistration();
                    }
                  }}
                />
              </div>

              <div className="col-lg-6 mt-3">
                <input
                  // type={Toggle ? 'text' : 'password'}
                  type={passwordShown ? 'text' : 'password'}
                  name="password"
                  className="form-control passwordicon"
                  // className={ // passwordicon
                  //   passwordError
                  //     ? 'form-control passwordicon input-grouperror'
                  //     : 'form-control passwordicon'
                  // }
                  placeholder="Password"
                  onChange={(e) => addPassword(e)}
                  onKeyPress={(event) => {
                    if (event.key === 'Enter') {
                      handleRegistration();
                    }
                  }}
                />
                <div>
                  <i className="eyeIcon" onClick={togglePasswordVisiblity}>
                    {passwordShown ? eye : <i className="fas fa-eye-slash"></i>}
                  </i>
                </div>
                {/* <div className="input-group-append">
                  <span className="input-group-text">
                    <i
                      onClick={handleToggle}
                      style={{ cursor: 'pointer' }}
                      className={
                        Toggle ? 'fas fa-eye fa-lg' : 'far fa-eye-slash fa-lg'
                      }></i>
                  </span>
                </div> */}
              </div>
              <div className="d-flex justify-content-center col-lg-12">
                <div className="allErrors ml-3 mt-2 mb-2">{errorAllFields}</div>
              </div>

              <div className="d-flex justify-content-center col-lg-12">
                <div className="btndiv container mb-3">
                  <div className="termConditions mr-2">
                    <p className="">I have read and agree to the</p>
                    <Checkbox
                      style={{ color: '#734385' }}
                      checked={checked}
                      onChange={handleChange}
                    />
                    <a
                      href="https://www.jivebird.com/terms-conditions/"
                      target="_blank"
                    >
                      Terms & Conditions
                    </a>
                  </div>
                </div>

                <button
                  type="button"
                  className="btn btnRegister"
                  disabled={!checked}
                  onClick={() => handleRegistration()}
                >
                  Register
                </button>
              </div>
              <div className="lastLine d-flex justify-content-center col-lg-12">
                <p>Only 99p to send your JiveBird, excluding eGift voucher</p>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default RegisterTemp;
