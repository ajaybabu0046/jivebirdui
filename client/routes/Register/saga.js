import { call, put, takeLatest } from 'redux-saga/effects';
import { apiResource } from '@/lib/utils/api';

import { RegisterAction, registerSuccess, registerFailed } from './action';

function* registerUserVerify(dObj) {
  // console.log(dObj, "hd")
  try {
    const url = '/accounts/register/';
    const res = yield call(apiResource.post, url, dObj.payload);
    // console.log(res)
    if (res.detail == 'Registeration successful') {
      yield put(
        registerSuccess({
          registerData: res,
        }),
      );
    } else {
      yield put(
        registerFailed({
          registerData: res,
        }),
      );
    }
  } catch (e) {
    // console.log(e,"response")
    yield put(
      registerFailed({
        registerData: JSON.parse(e.data),
      }),
    );
  }
}
function* registerSaga() {
  yield takeLatest(RegisterAction.VERIFY_REGISTER_USER, registerUserVerify);
}
export default registerSaga;
