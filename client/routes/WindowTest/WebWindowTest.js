import React from 'react';

export default function WebWindowTest() {
  var windowObjectReference;
  var windowFeatures = 'popup';

  const handleClick = () => {
    let url = 'https://www.google.com/';
    // console.log("windowReference", windowReference)
    // windowReference.location = url;
    windowObjectReference = window.open(url, 'CNN_WindowName', windowFeatures);
  };

  const handleClose = () => {
    windowObjectReference.close();
  };

  return (
    <div className="container mt-5">
      <button className="btn btn-primary my-2" onClick={() => handleClick()}>
        Initiate Payment
      </button>
      <br />
      <button className="btn btn-primary my-2" onClick={() => handleClose()}>
        Cancel Payment
      </button>
    </div>
  );
}
