import { call, put, takeLatest } from 'redux-saga/effects';
import { apiResource } from '@/lib/utils/api';

import { NewuserAction, newuserSuccess, newuserFailed } from './action';

function* newuserUserVerify(dObj) {
  try {
    const url = '/user-accounts/create-guest-user';
    const res = yield call(apiResource.post, url, dObj.payload);

    if (res.message == 'User created!') {
      yield put(
        newuserSuccess({
          newuserData: res,
        }),
      );
    } else {
      yield put(
        newuserFailed({
          newuserData: res,
        }),
      );
    }
  } catch (e) {
    let a = JSON.parse(e.data);

    yield put(
      newuserFailed({
        newuserData: a ? a : 'something went wrong',
      }),
    );
  }
}
function* newuserUser1Verify(dObj) {
  try {
    const url = '/accounts/register/';
    const res = yield call(apiResource.post, url, dObj.payload);

    if (res.message == 'User created!') {
      yield put(
        newuserSuccess({
          newuserData: res,
        }),
      );
    } else {
      yield put(
        newuserFailed({
          newuserData: res,
        }),
      );
    }
  } catch (e) {
    let a = JSON.parse(e.data);
    // console.log(a,"emailchevk")
    yield put(
      newuserFailed({
        newuserData: a ? a : 'something went wrong',
      }),
    );
  }
}
function* newuserSaga() {
  yield takeLatest(NewuserAction.VERIFY_NEWUSER_USER, newuserUserVerify);
  yield takeLatest(NewuserAction.VERIFY_NEWUSER1_USER, newuserUser1Verify);
}
export default newuserSaga;
