import React, { useState } from 'react';
import NewUserTemp from './component';
import Login from '../Login';
import { shape, func } from 'prop-types';
import { connect } from 'react-redux';
import { useInjectSaga } from '../../saga/injectSaga';
import newuserSaga from './saga';
import { newuserReducer } from './reducer';
import { useInjectReducer } from '../../reducers/injectReducer';
import { chooseSongReducer } from '../SongsList/reducer';
import { createJiveBirdReducer } from '../CreateJiveBird/reducer';
import { recipientReducer } from '../Recipient/reducer';
import { getCardCategoriesReducer } from '../SelectEcard/reducer';
import { recordingReducer } from '../RecordMessage/reducer';
import { EgiftReducer } from '../SelectEgift/reducer';
import { sendEcardReducer } from '../SendEcard/reducer';
import { getPhotoCardCategoriesReducer } from '../EcardWithPhoto/reducer';
import Register from '../Register';
import NewUser1 from './NewUser1';

const NewUser = (props) => {
  const { JiveBirdReducer, newuserData, dispatch } = props;
  const [currentScreen, setCurrentScreen] = useState(
    JiveBirdReducer && JiveBirdReducer.MyAccount ? 3 : 0,
  );
  const [loginBtnToggle, setLoginBtnToggle] = useState(false);

  useInjectReducer({ key: 'newuserReducer', reducer: newuserReducer });
  useInjectReducer({
    key: 'chooseSongReducer',
    reducer: chooseSongReducer,
  });
  useInjectReducer({
    key: 'createJiveBirdReducer',
    reducer: createJiveBirdReducer,
  });
  useInjectReducer({
    key: 'recipientReducer',
    reducer: recipientReducer,
  });
  useInjectReducer({
    key: 'getCardCategoriesReducer',
    reducer: getCardCategoriesReducer,
  });
  useInjectReducer({
    key: 'EgiftReducer',
    reducer: EgiftReducer,
  });
  useInjectReducer({
    key: 'sendEcardReducer',
    reducer: sendEcardReducer,
  });
  useInjectReducer({
    key: 'getPhotoCardCategoriesReducer',
    reducer: getPhotoCardCategoriesReducer,
  });
  useInjectReducer({
    key: 'recordingReducer',
    reducer: recordingReducer,
  });
  useInjectSaga({ key: 'newuserSaga', saga: newuserSaga });

  return (
    <div>
      {currentScreen === 0 && (
        <NewUserTemp
          {...props}
          setCurrentScreen={setCurrentScreen}
          newuserData={newuserData}
          dispatch={dispatch}
          setLoginBtnToggle={setLoginBtnToggle}
          loginBtnToggle={loginBtnToggle}
        />
      )}
      {currentScreen === 1 && (
        <Login
          setCurrentScreen={setCurrentScreen}
          setLoginBtnToggle={setLoginBtnToggle}
          loginBtnToggle={loginBtnToggle}
        />
      )}

      {currentScreen === 2 && (
        <Register
          setCurrentScreen={setCurrentScreen}
          setLoginBtnToggle={setLoginBtnToggle}
        />
      )}
      {currentScreen === 3 && (
        <NewUser1
          {...props}
          setCurrentScreen={setCurrentScreen}
          newuserData={newuserData}
          dispatch={dispatch}
          setLoginBtnToggle={setLoginBtnToggle}
          loginBtnToggle={loginBtnToggle}
        />
      )}
    </div>
  );
};

NewUser.propTypes = {
  newuserData: shape({}).isRequired,
  dispatch: func.isRequired,
};

NewUser.defaultProps = {
  newuserData: {
    loading: false,
  },
};

function mapStateToProps(props) {
  return {
    newuserData: props.newuserReducer,
    songUrl: props.chooseSongReducer,
    recipient: props.recipientReducer,
    card: props.getCardCategoriesReducer,
    eGift: props.EgiftReducer,
    scheduledTime: props.sendEcardReducer,
    voiceMsg: props.recordingReducer,
    JiveBirdReducer: props.createJiveBirdReducer,
    photoCard: props.getPhotoCardCategoriesReducer,
  };
}

export default connect(mapStateToProps)(NewUser);
