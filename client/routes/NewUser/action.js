import { createActionTypes } from '@/lib/utils/helper';

export const NewuserAction = createActionTypes('Newuser', [
  'VERIFY_NEWUSER_USER',
  'NEWUSER_SUCCESS',
  'NEWUSER_FAILED',
  'RESET_STATE',
  'VERIFY_NEWUSER1_USER',
  'NEWUSER1_SUCCESS',
  'NEWUSER1_FAILED',
]);

export function newuserUserVerify(obj) {
  return {
    type: NewuserAction.VERIFY_NEWUSER_USER,
    payload: {
      ...obj,
    },
  };
}

export function newuserSuccess(obj) {
  return {
    type: NewuserAction.NEWUSER_SUCCESS,
    payload: {
      ...obj,
    },
  };
}

export function newuserFailed(obj) {
  return {
    type: NewuserAction.NEWUSER_FAILED,
    payload: {
      ...obj,
    },
  };
}
export function newuserUser1Verify(obj) {
  return {
    type: NewuserAction.VERIFY_NEWUSER1_USER,
    payload: {
      ...obj,
    },
  };
}

export function newuser1Success(obj) {
  return {
    type: NewuserAction.NEWUSER1_SUCCESS,
    payload: {
      ...obj,
    },
  };
}

export function newuser1Failed(obj) {
  return {
    type: NewuserAction.NEWUSER1_FAILED,
    payload: {
      ...obj,
    },
  };
}
export function resetState() {
  return {
    type: NewuserAction.RESET_STATE,
  };
}
