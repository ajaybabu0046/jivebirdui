import React, { useEffect, useState } from 'react';
import Checkbox from '@material-ui/core/Checkbox';
import { newuserUser1Verify, resetState } from './action';
import swal from '@sweetalert/with-react';
import { firebaseAnalytics } from '../../firebaseConfig';
import { logEvent } from 'firebase/analytics';

const NewUser1 = (props) => {
  const {
    dispatch,
    setCurrentScreen,
    setLoginBtnToggle,
    loginBtnToggle,
    newuserData,
  } = props;
  const [checked, setChecked] = useState(false);
  const [Toggle, setToggle] = useState(false);
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [password, setPassword] = useState('');
  const [email, setEmail] = useState('');
  const [firstNameError, setFirstNameError] = useState(false);
  const [lastNameError, setLastNameError] = useState(false);
  const [emailError, setEmailError] = useState(false);
  const [passwordError, setPasswordError] = useState(false);
  const [errorAllFields, setErrorAllFields] = useState('');
  const [loading, setLoading] = useState(false);
  const handleChange = (event) => {
    setChecked(event.target.checked);
  };

  useEffect(() => {
    if (
      newuserData &&
      newuserData.response &&
      newuserData.response.hasOwnProperty('id')
    ) {
      logEvent(firebaseAnalytics, 'EventSignUp');
      swal(
        <div>
          <h1>You Are Almost There.</h1>
          <p>
            Please next click on the link in the <br />
            email from JiveBird that has been sent
            <br />
            to your email account. If you cannot
            <br />
            see the email, please check your
            <br />
            Spam/Junk/Promotions/Social folders
          </p>
        </div>,
        {
          button: 'OK',
          closeOnEsc: false,
          closeOnClickOutside: false,
        },
      ).then((willSuccess) => {
        if (willSuccess) {
          dispatch(resetState());
          setLoginBtnToggle(true);
          setCurrentScreen(1);
        }
      });
    }
    if (
      newuserData &&
      newuserData.response &&
      newuserData.response.password != undefined
    ) {
      setErrorAllFields(newuserData.response.password[0]);

      setLoading(false);
    }

    if (
      newuserData &&
      newuserData.response &&
      newuserData.response.username != undefined
    ) {
      setErrorAllFields(newuserData.response.username[0]);

      setLoading(false);
    }
    if (
      newuserData &&
      newuserData.response &&
      newuserData.response.email != undefined
    ) {
      setErrorAllFields(
        newuserData && newuserData.response && newuserData.response.email[0],
      );

      setLoading(false);
    }
    // if (
    //   newuserData &&
    //   newuserData.response &&
    //   newuserData.response.status_code == 1002
    // ) {
    //   swal(
    //     <div>
    //       <p>{newuserData.response.message}</p>
    //     </div>,
    //     {
    //       button: 'Yes',
    //     },
    //   ).then((willSuccess) => {
    //     if (willSuccess) {
    //       dispatch(resetState());
    //       setCurrentScreen(2);
    //     }
    //   });
    // }
    // if (
    //   newuserData &&
    //   newuserData.response &&
    //   newuserData.response == 'something went wrong'
    // ) {
    //   setErrorAllFields(<p>{newuserData.response[0]}</p>);

    // }
  }, [newuserData]);

  const handleNewuser1 = () => {
    let errorFound = false;
    // const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (firstName == '' || lastName == '' || email == '') {
      errorFound = true;
      setErrorAllFields('All fields are mandatory');

      setLoading(false);
    }
    if (firstName || firstName.length === 0) {
      let data = firstName.trim(' ');
      if (data.length === 0) {
        errorFound = true;
        setErrorAllFields('Fill the first name');
        setFirstNameError(true);
      }
    }
    if (lastName || lastName.length === 0) {
      let data = lastName.trim(' ');
      if (data.length === 0) {
        errorFound = true;
        setErrorAllFields('Fill the last name');
        setLastNameError(true);
      }
    }
    if (email || email.length === 0) {
      let data = email.trim(' ');
      if (data.length === 0) {
        errorFound = true;
        setErrorAllFields('Fill the email');
        setEmailError(true);
      }
    }
    // if (email) {
    //   if (!email.match(mailformat)) {
    //     errorFound = true;
    //     setErrorAllFields('Enter valid e-mail');
    //     setEmailError(true);
    //   }

    // }
    if (!password) {
      errorFound = true;
      setErrorAllFields('Empty Password Field');
      return;
    }
    if (errorFound) dispatch(resetState());

    if (!errorFound) {
      const emailId = email.toLowerCase();
      const randomId = Math.random().toString(36).substring(1, 7);
      dispatch(
        newuserUser1Verify({
          email: emailId.trim(),
          username: emailId.trim(),
          first_name: firstName.trim(),
          last_name: lastName.trim(),
          password: password,
          password_confirm: password,
          device_id: randomId,
        }),
      );
      dispatch(resetState());
    }
  };
  const addFirstName = (e) => {
    setFirstName(e.target.value);
    setErrorAllFields('');
    setFirstNameError(false);
    dispatch(resetState());
  };
  const addLastName = (e) => {
    setLastName(e.target.value);
    setErrorAllFields('');
    setLastNameError(false);
    dispatch(resetState());
  };

  const addEmail = (e) => {
    setEmail(e.target.value);
    setErrorAllFields('');
    setEmailError(false);
    dispatch(resetState());
  };

  const addPassword = (e) => {
    setPassword(e.target.value);
    setErrorAllFields('');
    setPasswordError(false);
    dispatch(resetState());
  };

  const handleLoginToggle = (data) => {
    if (data == 'login') {
      setLoginBtnToggle(true);
      setCurrentScreen(1);
    } else {
      setLoginBtnToggle(false);
      setCurrentScreen(3);
    }
  };

  return (
    <div className="container NewUsercontainer d-flex justify-content-center align-items-center">
      <div className="NewUserBody mt-5">
        <div className="row">
          <div className="col myToggalDiv">
            <span
              role="button"
              onClick={() => handleLoginToggle('login')}
              className={loginBtnToggle ? 'login' : 'default'}
            >
              Login
            </span>
            <span
              role="button"
              onClick={() => handleLoginToggle('regi')}
              className={loginBtnToggle ? 'default' : 'regi'}
            >
              New User
            </span>
          </div>
        </div>

        <form className="px-5">
          <div className="row">
            <div className="col">
              <h3 className="textHeader">Enter User Contact Details</h3>
              <h6 style={{ textAlign: 'center', color: '#926AA1' }}>
                (Enter your contact details below as a New User so we can inform
                you of its progress and you can keep a record.)
              </h6>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-6 mt-3">
              <input
                type="text"
                className="form-control"
                placeholder="First name"
                onChange={(e) => addFirstName(e)}
                onKeyPress={(event) => {
                  if (event.key === 'Enter') {
                    handleNewuser1();
                  }
                }}
              />
            </div>
            <div className="col-lg-6 mt-3">
              <input
                type="text"
                className="form-control"
                placeholder="Last name"
                onChange={(e) => addLastName(e)}
                onKeyPress={(event) => {
                  if (event.key === 'Enter') {
                    handleNewuser1();
                  }
                }}
              />
            </div>
          </div>
          <div className="row">
            <div className="col-lg-6 mt-3">
              <input
                type="password"
                name="password"
                className="form-control passwordicon"
                placeholder="Password"
                value={password}
                onChange={(e) => {
                  addPassword(e);
                  setPasswordError(false);
                }}
                onKeyPress={(event) => {
                  if (event.key === 'Enter') {
                    handleNewuser1();
                  }
                }}
              />
            </div>
            <div className="col-lg-6 mt-3">
              <input
                type="email"
                className="form-control emailicon"
                placeholder="E-mail"
                onChange={(e) => addEmail(e)}
                onKeyPress={(event) => {
                  if (event.key === 'Enter') {
                    handleNewuser1();
                  }
                }}
              />
            </div>
            <div className="d-flex justify-content-center col-lg-12">
              <div className="allErrors ml-3 mt-2 mb-2">{errorAllFields}</div>
            </div>
            <div className="btndiv container mb-3">
              <div className="termConditions mr-2">
                <p className="">I have read and agree to the</p>
                <Checkbox
                  style={{ color: '#734385' }}
                  checked={checked}
                  onChange={handleChange}
                />
                <a
                  href="https://www.jivebird.com/terms-conditions/"
                  target="_blank"
                >
                  Terms & Conditions
                </a>
              </div>
              <button
                type="button"
                className="btn btnSend"
                disabled={!checked}
                onClick={() => handleNewuser1()}
              >
                Send
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default NewUser1;
