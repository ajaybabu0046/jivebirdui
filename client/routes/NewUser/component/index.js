import React, { useState, useEffect, useRef } from 'react';
import './style.css';
import axios from 'axios';
import Checkbox from '@material-ui/core/Checkbox';
import { newuserUserVerify, resetState } from '../action';
import swal from '@sweetalert/with-react';
import CongratsImg from '@assets/Group.png';
import Moment from 'moment';
import Modal from 'react-bootstrap/Modal';
import { changeTab } from '../../CreateJiveBird/actions';
import {
  FacebookShareButton,
  TwitterShareButton,
  EmailShareButton,
  LinkedinShareButton,
  WhatsappShareButton,
  FacebookIcon,
  TwitterIcon,
  EmailIcon,
  LinkedinIcon,
  WhatsappIcon,
} from 'react-share';
import { firebaseAnalytics } from '../../../firebaseConfig';
import { logEvent } from 'firebase/analytics';

const NewUserTemp = (props) => {
  const {
    newuserData,
    songUrl,
    recipient,
    card,
    eGift,
    dispatch,
    setLoginBtnToggle,
    loginBtnToggle,
    photoCard,
    scheduledTime,
    setCurrentScreen,
    voiceMsg,
  } = props;

  // Initializing the window before to handle IOS devices.
  var windowObjectReference;
  var windowFeatures = 'popup';
  const timer = useRef(null);

  const [checked, setChecked] = useState(false);
  const [paymentId, setPaymentId] = useState('');
  const [initiatePayment, setInitiatePayment] = useState(false);
  const [statusRes, setStatusRes] = useState('STARTED');
  const [windowObj, setWindowObj] = useState({});
  const [show, setShow] = useState(false);
  const [Toggle, setToggle] = useState(false);
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [phone, setPhone] = useState('');
  const [email, setEmail] = useState('');
  const [firstNameError, setFirstNameError] = useState(false);
  const [lastNameError, setLastNameError] = useState(false);
  const [emailError, setEmailError] = useState(false);
  const [phoneError, setPhoneError] = useState(false);
  const [errorAllFields, setErrorAllFields] = useState('');
  const [token, setToken] = useState('');
  const [sending, isSending] = useState(false);

  const handleChange = (event) => {
    setChecked(event.target.checked);
  };

  var recordPath =
    voiceMsg.wavAudioKey != ''
      ? voiceMsg.wavAudioKey
      : process.env.REACT_APP_SILENT_AUDIO_PATH;

  useEffect(() => {
    if (
      newuserData &&
      newuserData.response &&
      newuserData.response.status_code == 1000
    ) {
      logEvent(firebaseAnalytics, 'Guest_User_Verified');
      setToken(newuserData.response.token);
      sendJive(newuserData.response.token);
    } else if (
      newuserData &&
      newuserData.response &&
      newuserData.response.status_code == 1001
    ) {
      swal(
        <div>
          <p>{newuserData.response.message}</p>
        </div>,
        {
          button: 'Yes',
          closeOnEsc: false,
          closeOnClickOutside: false,
        },
      ).then((willSuccess) => {
        if (willSuccess) {
          dispatch(resetState());
          setLoginBtnToggle(true);
          setCurrentScreen(1);
        }
      });
    } else if (
      newuserData &&
      newuserData.response &&
      newuserData.response.status_code == 1002
    ) {
      swal(
        <div>
          <p>{newuserData.response.message}</p>
        </div>,
        {
          button: 'Yes',
          closeOnEsc: false,
          closeOnClickOutside: false,
        },
      ).then((willSuccess) => {
        if (willSuccess) {
          dispatch(resetState());
          setCurrentScreen(2);
        }
      });
    } else if (
      newuserData &&
      newuserData.response &&
      newuserData.response.email != undefined
    ) {
      setErrorAllFields(
        <p>
          {newuserData && newuserData.response && newuserData.response.email}
        </p>,
      );
    }
  }, [newuserData]);
  // console.log(newuserData && newuserData.response &&newuserData.response.email,"inner")
  useEffect(() => {
    if (paymentId != '') {
      if (statusRes === 'STARTED') {
        timer.current = setInterval(() => getPaymentStatus(), 1000);
      } else if (statusRes == 'CANCELLED') {
        isSending(false);
        logEvent(firebaseAnalytics, 'Purchase_Cancelled');
        let b1 = document.getElementById('cancelByCodeButton');
        b1.click();

        clearInterval(timer.current);
        setPaymentId('');
        swal(
          <div>
            <p>JiveBird not sent, payment cancelled.</p>
          </div>,
          {
            icon: 'info',
            closeOnEsc: false,
            closeOnClickOutside: false,
          },
        ).then((willSuccess) => {
          if (willSuccess) {
            window.location.reload();
          }
        });
      } else if (statusRes == 'REFUNDED') {
        clearInterval(timer.current);
        let b1 = document.getElementById('cancelByCodeButton');
        b1.click();

        setPaymentId('');
        isSending(false);
        swal(
          <div>
            <p>JiveBird not sent, your payment is refunded.</p>
          </div>,
          {
            icon: 'info',
            closeOnEsc: false,
            closeOnClickOutside: false,
          },
        ).then((willSuccess) => {
          if (willSuccess) {
            window.location.reload();
          }
        });
      } else {
        isSending(false);
        let b1 = document.getElementById('cancelByCodeButton');
        b1.click();
        clearInterval(timer.current);
        setPaymentId('');
        swal(
          <div>
            <img src={CongratsImg} className="congrats mb-3" />
            <div>
              <h5>JiveBird Sent</h5>
            </div>
          </div>,
          {
            closeOnEsc: false,
            closeOnClickOutside: false,
          },
        ).then((willSuccess) => {
          if (willSuccess) {
            window.location.reload();
          }
        });
      }
    }
  }, [paymentId, statusRes]);

  const getPaymentStatus = () => {
    var checkData = new FormData();
    checkData.append('id', paymentId);
    var config = {
      method: 'post',
      url: `${process.env.BASE_API_PATH}/payment/stripe/check/`,
      headers: {
        Authorization: `Token ${token}`,
        'Content-Type': 'multipart/form-data',
      },
      data: checkData,
    };
    axios(config)
      .then((response) => {
        setStatusRes(response.data.remittance_status);
      })
      .catch((response) => {
        console.log('Error Resp Payment status', response);
      });
  };

  const handleCancelPayment = () => {
    var checkData = new FormData();
    checkData.append('id', paymentId);
    checkData.append('client_secret', paymentId);
    var config = {
      method: 'post',
      url: `${process.env.BASE_API_PATH}/payment/stripe/cancel/`,
      headers: {
        'Content-Type': 'multipart/form-data',
      },
      data: checkData,
    };
    axios(config)
      .then((response) => {
        setStatusRes('CANCELLED');
        isSending(false);
        setPaymentId('');
      })
      .catch((response) => {
        console.log('Error Resp Payment status', response);
        isSending(false);
      });
  };

  const sendJive = (token) => {
    isSending(true);
    logEvent(firebaseAnalytics, 'JB_Sent_GuestUser');
    var SDate = scheduledTime.types == 'schedule' && scheduledTime.date.ts;
    var STime = scheduledTime.types == 'schedule' && scheduledTime.time;
    var formateDate = Moment(SDate).format('yyyy-MM-DD');
    var ScheduleDT = formateDate + ' ' + STime;

    var formData = new FormData();
    formData.append('song_id', songUrl.selectedId);
    formData.append('receipt_email', recipient.email);
    formData.append('receipt_phone', recipient.phoneNo);
    formData.append('receipt_name', recipient.name);

    if (scheduledTime.types === 'schedule') {
      formData.append('delivery_date', ScheduleDT);
    }

    formData.append('recording', recordPath);

    if (card && card.cardSelected === true) {
      formData.append('card_id', card.cardId);
      formData.append('message', card.message);
    }

    if (photoCard && photoCard.cardSelected === true) {
      formData.append('photo', photoCard.photoLocation);
      formData.append('photocard_id', photoCard.cardId);
      formData.append('message', photoCard.message);
    }

    formData.append('is_web', 1);

    if (eGift && eGift.selectedEgift && eGift.selectedEgift.id) {
      formData.append('vendor_code', eGift.selectedEgift.vendorCode);
      formData.append('gift_price', eGift.selectedEgiftValue);

      var config = {
        method: 'post',
        url: `${process.env.BASE_API_PATH}/payment/stripe/save/`,
        headers: {
          Authorization: `Token ${token}`,
          'Content-Type': 'multipart/form-data',
        },
        data: formData,
      };
      axios(config)
        .then((response) => {
          setInitiatePayment(true);
          setPaymentId(response.data.client_secret);
          setStatusRes('STARTED');
          setShow(true);
        })
        .catch((response) => {
          console.log('Error Resp Payment', response);
        });
    } else {
      var config = {
        method: 'post',
        url: `${process.env.BASE_API_PATH}/jbcards/upload/jb/`,
        headers: {
          Authorization: `Token ${token}`,
          'Content-Type': 'multipart/form-data',
        },
        data: formData,
      };

      axios(config)
        .then((response) => {
          swal(
            <div>
              <img src={CongratsImg} className="congrats mb-3" />
              <div>
                <h5>JiveBird Sent</h5>
              </div>
              <div>
                <h5>Share with friends.</h5>
              </div>
              <div>
                <EmailShareButton
                  subject="JiveBird"
                  body="Click to view your JB"
                  separator=": "
                  url={`${process.env.REACT_APP_SOCIAL_LINK}${
                    response && response.data && response.data.access_code
                  }`}
                >
                  <EmailIcon />
                </EmailShareButton>
                <FacebookShareButton
                  url={`${process.env.REACT_APP_SOCIAL_LINK}${
                    response && response.data && response.data.access_code
                  }`}
                  hashtag={'#jivebird'}
                  className="Demo__some-network__share-button"
                >
                  <FacebookIcon />
                </FacebookShareButton>
                <LinkedinShareButton
                  url={`${process.env.REACT_APP_SOCIAL_LINK}${
                    response && response.data && response.data.access_code
                  }`}
                  title="JB SHARE Title"
                  summary="View here"
                  source="jivebird.com"
                >
                  <LinkedinIcon />
                </LinkedinShareButton>
                <TwitterShareButton
                  url={`${process.env.REACT_APP_SOCIAL_LINK}${
                    response && response.data && response.data.access_code
                  }`}
                  title="JiveBird"
                  via="jbshare"
                  hashtags={['jivebird', 'jbshare']}
                >
                  <TwitterIcon />
                </TwitterShareButton>
                <WhatsappShareButton
                  url={`${process.env.REACT_APP_SOCIAL_LINK}${
                    response && response.data && response.data.access_code
                  }`}
                  title="JiveBird"
                  separator=": "
                >
                  <WhatsappIcon />
                </WhatsappShareButton>
              </div>
            </div>,
            {
              closeOnEsc: false,
              closeOnClickOutside: false,
            },
          ).then((willSuccess) => {
            if (willSuccess) {
              window.location.reload();
            }
          });
        })
        .catch((response) => {
          console.log('Error Resp', response);
        });
    }
  };

  const handleNewuser = () => {
    logEvent(firebaseAnalytics, 'New_User_Details_entered');
    isSending(true);
    let errorFound = false;
    // const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (firstName == '' || lastName == '' || email == '') {
      errorFound = true;
      setErrorAllFields('All fields are mandatory');
    }
    if (firstName || firstName.length === 0) {
      let data = firstName.trim(' ');
      if (data.length === 0) {
        errorFound = true;
        setErrorAllFields('Fill the first name');
        setFirstNameError(true);
      }
    }
    if (lastName || lastName.length === 0) {
      let data = lastName.trim(' ');
      if (data.length === 0) {
        errorFound = true;
        setErrorAllFields('Fill the last name');
        setLastNameError(true);
      }
    }
    if (email || email.length === 0) {
      let data = email.trim(' ');
      if (data.length === 0) {
        errorFound = true;
        setErrorAllFields('Fill the email');
        setEmailError(true);
      }
    }
    // if (email) {
    //   if (!email.match(mailformat)) {
    //     errorFound = true;
    //     setErrorAllFields('Enter valid e-mail');
    //     setEmailError(true);
    //   }
    // }

    var phone1 = '00000';
    if (phone != '') {
      if (phone[0] != '+') {
        errorFound = true;
        setErrorAllFields("Ensure correct country code is used with a '+'");
        setPhoneError(true);
        return;
      } else {
        phone1 = phone;
      }
    }

    if (errorFound) {
      isSending(false);
      dispatch(resetState());
      return;
    }

    if (!errorFound) {
      const emailId = email.toLowerCase();
      const randomId = Math.random().toString(36).substring(1, 7);
      dispatch(
        newuserUserVerify({
          email: emailId.trim(),
          first_name: firstName.trim(),
          last_name: lastName.trim(),
          mobile: phone1,
          device_id: randomId,
        }),
      );
      dispatch(resetState());
    }
  };

  const addFirstName = (e) => {
    setFirstName(e.target.value);
    setErrorAllFields('');
    setFirstNameError(false);
    dispatch(resetState());
  };
  const addLastName = (e) => {
    setLastName(e.target.value);
    setErrorAllFields('');
    setLastNameError(false);
    dispatch(resetState());
  };

  const addEmail = (e) => {
    setEmail(e.target.value);
    setErrorAllFields('');
    setEmailError(false);
    dispatch(resetState());
  };

  const addPhone = (e) => {
    setPhone(e.target.value);
    setErrorAllFields('');
    setPhoneError(false);
    dispatch(resetState());
  };

  const handleLoginToggle = (data) => {
    if (data == 'login') {
      setLoginBtnToggle(true);
      setCurrentScreen(1);
    } else {
      setLoginBtnToggle(false);
      setCurrentScreen(0);
    }
  };

  const continuePayment = () => {
    var urlLink = 'https://www.jivebird.com/your-payment/?id=' + paymentId;
    windowObjectReference = window.open(
      urlLink,
      'Jivebird_Payment',
      windowFeatures,
    );
    setWindowObj(windowObjectReference);
  };

  const cancelPaymentByCode = () => {
    setInitiatePayment(false);
    windowObj.close();
  };

  return (
    <div className="container NewUsercontainer d-flex justify-content-center align-items-center">
      {initiatePayment ? (
        <Modal
          id="fullScreenModalId"
          show={show}
          dialogClassName="fullscreen-modal"
        >
          <Modal.Body className="modalText">
            <h6>
              Your payment request has been initiated. Please click on
              "Continue" button below to be redirected to the payment page.
              Please do not press back button or close this page.
            </h6>
            {/* <i
              className="fa fa-spinner fa-spin fa-2x faClass"
              style={{ marginRight: '5px' }}
            /> */}
            <div className="cancelBtnDiv">
              <button
                type="button"
                id="cancelByCodeButton"
                style={{ display: 'none' }}
                className="btn btnPayment mr-4"
                onClick={() => cancelPaymentByCode()}
              >
                Cancel By Code Button
              </button>
              <button
                type="button"
                id="continueButton"
                className="btn btnPayment mr-4"
                onClick={() => continuePayment()}
              >
                Continue
              </button>
              <button
                type="button"
                className="btn btnPayment"
                onClick={() => handleCancelPayment()}
              >
                Cancel Payment
              </button>
            </div>
          </Modal.Body>
        </Modal>
      ) : (
        <div className="NewUserBody mt-5">
          <div className="row">
            <div className="col myToggalDiv">
              <span
                role="button"
                onClick={() => {
                  handleLoginToggle('login');
                  logEvent(firebaseAnalytics, 'Login_Clicked_From_NewUser');
                }}
                className={loginBtnToggle ? 'login' : 'default'}
              >
                Login
              </span>
              <span
                role="button"
                onClick={() => handleLoginToggle('regi')}
                className={loginBtnToggle ? 'default' : 'regi'}
              >
                New User
              </span>
            </div>
          </div>

          <form className="px-5">
            <div className="row">
              <div className="col">
                <h3 className="textHeader">Enter User Contact Details</h3>
                <h6 style={{ textAlign: 'center', color: '#926AA1' }}>
                  (Enter your contact details below as a New User so we can
                  inform you of its progress and you can keep a record.)
                </h6>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-6 mt-3">
                <input
                  type="text"
                  className="form-control"
                  placeholder="First name"
                  onChange={(e) => addFirstName(e)}
                  onKeyPress={(event) => {
                    if (event.key === 'Enter') {
                      handleNewuser();
                    }
                  }}
                />
              </div>
              <div className="col-lg-6 mt-3">
                <input
                  type="text"
                  className="form-control"
                  placeholder="Last name"
                  onChange={(e) => addLastName(e)}
                  onKeyPress={(event) => {
                    if (event.key === 'Enter') {
                      handleNewuser();
                    }
                  }}
                />
              </div>
            </div>
            <div className="row">
              <div className="col-lg-6 mt-3">
                <input
                  type="email"
                  className="form-control emailicon"
                  placeholder="E-mail"
                  onChange={(e) => addEmail(e)}
                  onKeyPress={(event) => {
                    if (event.key === 'Enter') {
                      handleNewuser();
                    }
                  }}
                />
              </div>

              <div className="d-flex justify-content-center col-lg-12">
                <div className="allErrors ml-3 mt-2 mb-2">{errorAllFields}</div>
              </div>
              <div className="btndiv container mb-3">
                <div className="termConditions mr-2">
                  <p className="">I have read and agree to the</p>
                  <Checkbox
                    style={{ color: '#734385' }}
                    checked={checked}
                    onChange={handleChange}
                  />
                  <a
                    href="https://www.jivebird.com/terms-conditions/"
                    target="_blank"
                  >
                    Terms & Conditions
                  </a>
                </div>
                <button
                  type="button"
                  className="btn btnSend"
                  disabled={sending}
                  onClick={() => handleNewuser()}
                >
                  {/* <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> */}
                  Send
                </button>
              </div>
            </div>
          </form>
        </div>
      )}
    </div>
  );
};

export default NewUserTemp;
