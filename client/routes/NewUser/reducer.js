import { NewuserAction } from './action';

export const initialState = {
  loading: false,

  message: '',
  response: {},
};

export const newuserReducer = (state = initialState, action) => {
  const {
    VERIFY_NEWUSER_USER,
    NEWUSER_SUCCESS,
    NEWUSER_FAILED,
    RESET_STATE,
    VERIFY_NEWUSER1_USER,
    NEWUSER1_SUCCESS,
    NEWUSER1_FAILED,
  } = NewuserAction;

  const { payload } = action;

  switch (action.type) {
    case VERIFY_NEWUSER_USER:
      return {
        ...state,
        loading: true,

        // message:"",
      };
    case NEWUSER_SUCCESS:
      return {
        ...state,
        loading: false,

        response: action.payload.newuserData,
      };
    case NEWUSER_FAILED:
      return {
        ...state,
        loading: false,
        response: action.payload.newuserData,
      };

    case VERIFY_NEWUSER1_USER:
      return {
        ...state,
        loading: true,

        // message:"",
      };
    case NEWUSER1_SUCCESS:
      return {
        ...state,
        loading: false,

        response: action.payload.newuserData,
      };
    case NEWUSER1_FAILED:
      return {
        ...state,
        loading: false,
        response: action.payload.newuserData,
      };

    case RESET_STATE:
      return {
        ...initialState,
      };

    default:
      return state;
  }
};
