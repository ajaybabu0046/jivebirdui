import { createActionTypes } from '@/lib/utils/helper';

export const songsListActions = createActionTypes('songsList', [
  'CHOOSE_SONG',
  'RESELECT_CATEGORY',
  'RESET_STATES',

  'GET_SONG_CATEGORY',
  'GET_SONG_CATEGORY_SUCCESS',
  'GET_SONG_CATEGORY_FAIL',

  'SEARCH_SONGS',
  'SEARCH_SONGS_SUCCESS',
  'SEARCH_SONGS_FAIL',
]);

export function reselectMusicCategory() {
  return {
    type: songsListActions.RESELECT_CATEGORY,
  };
}
export function resetStates() {
  return {
    type: songsListActions.RESET_STATES,
  };
}

export function chooseSong(obj) {
  return {
    type: songsListActions.CHOOSE_SONG,
    payload: {
      ...obj,
    },
  };
}

export const getSongFromCategoryList = (obj) => ({
  type: songsListActions.GET_SONG_CATEGORY,
  payload: {
    ...obj,
  },
});

export const getSongFromCategorySuccess = (obj) => ({
  type: songsListActions.GET_SONG_CATEGORY_SUCCESS,
  payload: {
    ...obj,
  },
});

export const getSongFromCategoryFailure = (obj) => ({
  type: songsListActions.GET_SONG_CATEGORY_FAIL,
  payload: {
    ...obj,
  },
});

export const searchSongs = (obj) => ({
  type: songsListActions.SEARCH_SONGS,
  payload: {
    ...obj,
  },
});

export const searchSongsSuccess = (obj) => ({
  type: songsListActions.SEARCH_SONGS_SUCCESS,
  payload: {
    ...obj,
  },
});

export const searchSongsFailure = (obj) => ({
  type: songsListActions.SEARCH_SONGS_FAIL,
  payload: {
    ...obj,
  },
});
