import React, { useState } from 'react';
import './style.css';
import { reselectMusicCategory, chooseSong, resetStates } from '../actions';
import { changeTab } from '../../CreateJiveBird/actions';
import Loader from 'react-loader-spinner';
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';
import { firebaseAnalytics } from '../../../firebaseConfig';
import { logEvent } from 'firebase/analytics';

const MusicListTemp = (props) => {
  const { music, song, dispatch, isSearching } = props;

  const [playOn, setPlayOn] = useState(false);
  const [selected, setSelected] = useState(song.selectedId);
  const [toggel, setToggel] = useState(false);

  const handlePlay = (data) => {
    logEvent(firebaseAnalytics, 'Track_Choosen');
    const { file_id, id } = data;
    setSelected(file_id);

    var myAudio = null;
    if (toggel === false) {
      myAudio = document.getElementById(file_id);
      if (myAudio !== null) {
        myAudio.play();
        setPlayOn(true);
        setToggel(true);
        dispatch(
          chooseSong({
            url: `${`https://jivebirdbucketprod.s3.eu-west-2.amazonaws.com/music/songs/${file_id}.mp3`}`,
            id: id,
          }),
        );
      }
    } else {
      myAudio = document.getElementById(selected);
      if (myAudio !== null) {
        myAudio.pause();
        myAudio.currentTime = 0;
        setToggel(false);
        setPlayOn(false);
      }
      if (selected != file_id) {
        myAudio = document.getElementById(file_id);
        if (myAudio !== null) {
          myAudio.play();
          setPlayOn(true);
          setToggel(true);
          dispatch(
            chooseSong({
              url: `${`https://jivebirdbucketprod.s3.eu-west-2.amazonaws.com/music/songs/${file_id}.mp3`}`,
              id: id,
            }),
          );
        }
      }
    }
  };

  return (
    <div
      className={
        isSearching ? 'p-0' : 'container-fluid chooseCardContainer p-0'
      }
    >
      <div className="d-flex mt-3 songHeader sticky">
        {isSearching === false && (
          <h1 className="mainHeading head1">
            {music.category} <span style={{ color: '#734385' }}>Song</span>
          </h1>
        )}
        <div className="d-flex btns-div ">
          {isSearching === false && (
            <button
              type="button"
              name="reselect"
              className="btn app-btn mr-3 mt-2"
              onClick={() => {
                dispatch(reselectMusicCategory());
                dispatch(resetStates());
              }}
            >
              Re-Select
            </button>
          )}
          <button
            type="button"
            name="next"
            className="btn app-btn  mt-2 head2"
            disabled={!song.songSelected}
            onClick={() => {
              dispatch(changeTab({ tabId: 2 }));
            }}
          >
            Next
          </button>
        </div>
      </div>

      <div
        className={
          isSearching
            ? 'row mt-3 searchContainer commonScroller'
            : 'row mt-3 songsContainer commonScroller'
        }
      >
        {song && song.songListLoading === true ? (
          <div className="loaderContainer">
            <Loader
              type="Bars"
              color="#734385"
              height={100}
              width={100}
              radius={30}
            />
          </div>
        ) : song && song.songCategoryLists.length > 0 ? (
          song.songCategoryLists.map((data) => (
            <div
              key={data.file_id}
              onClick={() => handlePlay(data)}
              style={{ cursor: 'pointer' }}
              className="col-lg-4 col-md-6 mb-4"
            >
              <div className="chooseMusic">
                <div className="d-flex">
                  {selected !== data.file_id && (
                    <>
                      <i
                        role="button"
                        className="mr-4 ml-3 playIcon mouseCursor fas fa-play fa-lg"
                      ></i>
                    </>
                  )}
                  {selected == data.file_id && (
                    <>
                      {playOn ? (
                        <i
                          role="button"
                          className="fas fa-pause fa-lg mr-4 ml-3 playIcon mouseCursor"
                        ></i>
                      ) : (
                        <i
                          role="button"
                          className="fas fa-play fa-lg mr-4 ml-3 playIcon mouseCursor"
                        ></i>
                      )}
                    </>
                  )}

                  <h6 role="button" style={{ cursor: 'pointer' }}>
                    {data.title}
                  </h6>
                </div>
                <div className="playIcon">
                  {selected == data.file_id && (
                    <i className="fas fa-check fa-lg checkIcon"></i>
                  )}
                </div>
              </div>
              <audio
                id={data.file_id}
                src={`https://jivebirdbucketprod.s3.eu-west-2.amazonaws.com/music/songs/${data.file_id}.mp3`}
                type="audio/mpeg"
                style={{ display: 'none' }}
              />
            </div>
          ))
        ) : (
          <h5 className="ml-3" style={{ color: 'rgb(115, 67, 133)' }}>
            No songs available for selection.
          </h5>
        )}
      </div>
    </div>
  );
};

export default MusicListTemp;
