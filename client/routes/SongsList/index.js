import React, { useEffect } from 'react';
import MusicListTemp from './component';
import { func } from 'prop-types';
import { connect } from 'react-redux';
import { useInjectReducer } from '../../reducers/injectReducer';
import { useInjectSaga } from '../../saga/injectSaga';
import { getSongFromCategoryList, searchSongs } from './actions';
import { chooseSongReducer } from './reducer';
import SongCategorySaga from './saga';
import Loader from 'react-loader-spinner';
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';

const MusicList = (props) => {
  const { music, song, dispatch, isSearching, searchText } = props;

  useInjectReducer({
    key: 'chooseSongReducer',
    reducer: chooseSongReducer,
  });
  useInjectSaga({ key: 'SongCategorySaga', saga: SongCategorySaga });

  useEffect(() => {
    if (isSearching) {
      dispatch(
        searchSongs({
          search: searchText,
        }),
      );
    } else {
      dispatch(
        getSongFromCategoryList({
          category: music.category,
        }),
      );
    }
  }, [isSearching, searchText]);

  return song.songListLoading === false ? (
    <MusicListTemp {...props} />
  ) : (
    <div className="loaderContainer">
      <Loader
        type="Bars"
        color="#734385"
        height={100}
        width={100}
        radius={30}
      />
    </div>
  );
};

MusicList.propTypes = {
  dispatch: func.isRequired,
};

MusicList.defaultProps = {
  song: {
    songListLoading: true,
    song: '',
    songCategoryLists: [],
    songSelected: false,
    selectedId: '',
  },
  isSearching: false,
  searchText: '',
};

function mapStateToProps(props) {
  return {
    song: props.chooseSongReducer,
  };
}

export default connect(mapStateToProps)(MusicList);
