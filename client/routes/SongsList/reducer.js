import { songsListActions } from './actions';
// import { songsListActions } from '../SongsList/actions';

export const initialState = {
  songListLoading: true,
  songCategoryLists: [],
  songSelected: false,
  selectedId: '',
  song: '',
};

export const chooseSongReducer = (state = initialState, action) => {
  const {
    CHOOSE_SONG,
    RESET_STATES,
    GET_SONG_CATEGORY,
    GET_SONG_CATEGORY_SUCCESS,
    GET_SONG_CATEGORY_FAIL,
    SEARCH_SONGS,
    SEARCH_SONGS_SUCCESS,
    SEARCH_SONGS_FAIL,
  } = songsListActions;

  switch (action.type) {
    case RESET_STATES:
      return {
        ...initialState,
      };

    case GET_SONG_CATEGORY:
      return {
        ...state,
        songListLoading: true,
        songCategoryLists: [],
        songSelected: false,
        selectedId: '',
        song: '',
      };
    case GET_SONG_CATEGORY_SUCCESS:
      // let sortedByPopularity = action.payload.Data.sort((a, b) => {
      //   var p1 = a.popularity, p2 = b.popularity;

      //   if (p1 < p2) return 1;
      //   if (p1 > p2) return -1;
      //   return 0;
      // });

      return {
        ...state,
        songListLoading: false,
        songCategoryLists: action.payload.Data,
        success: 1,
      };
    case GET_SONG_CATEGORY_FAIL:
      return {
        ...state,
        songListLoading: false,
        songCategoryLists: action.payload.Data,
        success: 0,
      };
    case SEARCH_SONGS:
      return {
        ...state,
        songListLoading: true,
        songCategoryLists: [],
        songSelected: false,
        selectedId: '',
        song: '',
      };
    case SEARCH_SONGS_SUCCESS:
      return {
        ...state,
        songListLoading: false,
        songCategoryLists: action.payload.Data,
        success: 1,
      };
    case SEARCH_SONGS_FAIL:
      return {
        ...state,
        songListLoading: false,
        songCategoryLists: action.payload.Data,
        success: 0,
      };

    case CHOOSE_SONG:
      return {
        ...state,
        songSelected: true,
        selectedId: action.payload.id,
        song: action.payload.url,
      };

    default:
      return state;
  }
};
