/**
 * Gets the repositories of the user from Github
 */
import { call, put, takeLatest } from 'redux-saga/effects';

import { apiResource } from '@/lib/utils/api';
import {
  songsListActions,
  getSongFromCategorySuccess,
  getSongFromCategoryFailure,
  searchSongsSuccess,
  searchSongsFailure,
} from './actions';

function* getSongFromCategoryList(dObj) {
  try {
    const url = `/song_categories/${dObj.payload.category}`;
    const res = yield call(apiResource.get, url);

    if (res.length > 0) {
      yield put(
        getSongFromCategorySuccess({
          Data: res,
        }),
      );
    } else {
      yield put(
        getSongFromCategoryFailure({
          Data: res,
        }),
      );
    }
  } catch (e) {
    yield put(
      getSongFromCategoryFailure({
        Data: [],
      }),
    );
  }
}

function* searchSongs(dObj) {
  try {
    const url = `songs/any/${dObj.payload.search}`;
    const res = yield call(apiResource.get, url);

    if (res.length > 0) {
      yield put(
        searchSongsSuccess({
          Data: res,
        }),
      );
    } else {
      yield put(
        searchSongsFailure({
          Data: res,
        }),
      );
    }
  } catch (e) {
    yield put(
      searchSongsFailure({
        Data: [],
      }),
    );
  }
}

function* SongCategorySaga() {
  yield takeLatest(songsListActions.GET_SONG_CATEGORY, getSongFromCategoryList);
  yield takeLatest(songsListActions.SEARCH_SONGS, searchSongs);
}

export default SongCategorySaga;
