import React, { useEffect } from 'react';
import { func } from 'prop-types';
import { connect } from 'react-redux';
import { useInjectReducer } from '../../reducers/injectReducer';
import { useInjectSaga } from '../../saga/injectSaga';
import EgiftCardTemp from './Component';
import EgiftSaga from '../SelectEgift/saga';
import { EgiftReducer } from '../SelectEgift/reducer';
import Loader from 'react-loader-spinner';
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';

const EgiftCard = (props) => {
  const { vendorlist } = props;

  useInjectReducer({
    key: 'EgiftReducer',
    reducer: EgiftReducer,
  });

  useInjectSaga({ key: 'EgiftSaga', saga: EgiftSaga });

  return (
    <>
      {vendorlist && vendorlist.EgiftLoading == false ? (
        <EgiftCardTemp
          {...props}
          vendor={vendorlist.selectedEgift}
          selectedValue={vendorlist.selectedEgiftValue}
        />
      ) : (
        <div className="loaderContainer">
          <Loader
            type="Bars"
            color="#734385"
            height={100}
            width={100}
            radius={30}
          />
        </div>
      )}
    </>
  );
};
EgiftCard.propTypes = {
  dispatch: func.isRequired,
};

EgiftCard.defaultProps = {
  vendorlist: {
    categorySelected: false,
    selectedEgift: {},
    selectedEgiftValue: '',
  },
};

function mapStateToProps(props) {
  return {
    vendorlist: props.EgiftReducer,
  };
}

export default connect(mapStateToProps)(EgiftCard);
