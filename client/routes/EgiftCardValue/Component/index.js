import React, { useState } from 'react';
import './style.css';
import { changeTab } from '../../CreateJiveBird/actions';
import {
  chooseEgiftValue,
  changeScreen,
  reselectGiftCard,
} from '../../SelectEgift/actions';
import { Modal } from 'react-bootstrap';

const EgiftCardTemp = (props) => {
  const { dispatch, vendor, setCurrentScreen, selectedValue } = props;

  const [selected, setSelected] = useState(selectedValue);
  const [open, setOpen] = useState(false);

  const handleCardValue = (vendtl) => {
    dispatch(chooseEgiftValue({ egiftValue: vendtl }));
    setSelected(vendtl);
  };

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    setCurrentScreen(1);
  };

  return (
    <>
      <div className="container-fluid mb-3 customContainer">
        <div className="d-flex eCardRow btns-div mt-3 sticky1">
          <h1 className="mainHeading head1">
            eGift <span style={{ color: '#734385' }}>Card</span>
          </h1>
          <div className="ml-auto head2">
            <button
              type="button"
              name="next"
              disabled={selected == ''}
              className="btn app-btn ml-4 mt-2"
              onClick={() => dispatch(changeTab({ tabId: 5 }))}
            >
              Next
            </button>
            <button
              type="button"
              name="reselect"
              className="btn app-btn mt-2"
              onClick={() => {
                dispatch(reselectGiftCard());
                dispatch(changeScreen({ Id: 1 }));
              }}
            >
              Re-Select
            </button>
          </div>
        </div>
        <div className="row rowone">
          <div className="col-lg-6 col-md-12 container">
            <h1 className="subHeading">
              <span style={{ color: '#734385' }}>eGift Card Value</span>
            </h1>
            {vendor &&
              vendor.values &&
              vendor.values.length > 0 &&
              vendor.values.map((value, index) => (
                <div
                  key={index}
                  className="item firstcard"
                  style={{ cursor: 'pointer' }}
                  onClick={() => handleCardValue(value)}
                >
                  <h1>
                    <span style={{ color: '#734385' }}>
                      <i className="euroImg fas fa-pound-sign"></i>
                      {value}
                    </span>
                  </h1>

                  <div className="ml-4 mb-4">
                    {selected == value && (
                      <>
                        <i className="fas fa-check fa-lg checkIcon"></i>
                      </>
                    )}
                  </div>
                </div>
              ))}
          </div>

          <div className="col-lg-6 col-md-12 columntwo ">
            <div className="giftcardimage">
              <img className="editimage" src={vendor.vendorLogo} />
              <div className="imgtext ">
                <p>
                  {vendor.name}{' '}
                  <span
                    style={{ textDecorationLine: 'underline' }}
                    onClick={() => handleOpen()}
                  >
                    Terms & Conditions
                  </span>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* Modal Start */}
      <Modal
        show={open}
        onHide={() => handleClose()}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Body style={{ backgroundColor: '#fff' }}>
          <div className="modalHeading">
            <h1>Terms & Conditions</h1>
          </div>
          <div className="modalBody mt-5 mb-4">
            <div className="row">
              <p>{vendor.terms}</p>
            </div>

            <div className="mt-5">
              <button
                type="button"
                className="btn btnCancel"
                onClick={() => handleClose()}
              >
                OK
              </button>
            </div>
          </div>
        </Modal.Body>
      </Modal>

      {/* End */}
    </>
  );
};

export default EgiftCardTemp;
