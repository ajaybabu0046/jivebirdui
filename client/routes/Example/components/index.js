import React, { useState } from 'react';
import play from '@assets/play.png';
import pause from '@assets/pause.png';
import './style.css';
import welldone from '@assets/welldone.png';

const ExampleTemp = () => {
  const [pausetoggle, setPauseToggle] = useState(false);
  const [selectedSongId, setselectedSongId] = useState('');
  const [selectedElm, setselectedElm] = useState('');

  const handlePlayPause = (id, element) => {
    let toggle = pausetoggle;
    let myAudio = document.getElementById(element);

    if (selectedSongId == id) {
      myAudio.pause();
      myAudio.currentTime = 0;
      setPauseToggle(false);
      setselectedElm('');
    } else {
      let prevAudio = document.getElementById(selectedElm);

      if (prevAudio && prevAudio.currentTime > 0) {
        toggle = false;
        prevAudio.pause();
        prevAudio.currentTime = 0;
      }

      if (toggle === false) {
        setselectedSongId(id);
        setselectedElm(element);
        myAudio.play();
        setPauseToggle(true);
      } else {
        myAudio.pause();
        myAudio.currentTime = 0;
        setPauseToggle(false);
      }
    }
  };

  return (
    <div>
      <h1 className="mt-5 ml-5 heading">Example</h1>

      <div className="card-deck ml-4 mr-4 mt-5">
        <div
          className="card ml-3 cardBorder"
          onClick={() => handlePlayPause('1', 'audioPlay')}
        >
          <div className="card-body bodyOfCard d-flex justify-content-center">
            <div className="card-title txt">
              <span>BIRTHDAY</span>
              <span>
                {' '}
                <img
                  className="card-img-top ml-3 mr-2 playPause"
                  src={selectedSongId == 1 && pausetoggle ? pause : play}
                  alt="Card image cap"
                />
                <audio
                  id="audioPlay"
                  src={process.env.REACT_APP_WEB_ASSETS + '/BIRTHDAY.mp3'}
                  type="audio"
                  style={{ display: 'none' }}
                />
              </span>
              <span>Play JiveBird</span>
            </div>
          </div>
          <div className="d-flex justify-content-center mb-3">
            <p className="card-text">
              <img
                className="card-img-center Imgcard"
                src="https://kiui8tq53c.execute-api.eu-west-2.amazonaws.com/prod/cards/download_thumbnail/42"
                alt="Card image cap"
              />
            </p>
          </div>
        </div>
        <div
          className="card ml-3 cardBorder"
          onClick={() => handlePlayPause('2', 'audioPlay1')}
        >
          <div className="card-body bodyOfCard d-flex justify-content-center">
            <div className="card-title txt">
              <span>LOVE</span>
              <span>
                {' '}
                <img
                  className="card-img-top ml-3 mr-2 playPause"
                  src={selectedSongId == 2 && pausetoggle ? pause : play}
                  alt="Card image cap"
                />
                <audio
                  id="audioPlay1"
                  src={process.env.REACT_APP_WEB_ASSETS + '/LOVE.mp3'}
                  type="audio"
                  style={{ display: 'none' }}
                />
              </span>
              <span>Play JiveBird</span>
            </div>
          </div>
          <div className="d-flex justify-content-center mb-3">
            <p className="card-text">
              <img
                className="card-img-center Imgcard"
                src="https://kiui8tq53c.execute-api.eu-west-2.amazonaws.com/prod/cards/download_thumbnail/48"
                alt="Card image cap"
              />
            </p>
          </div>
        </div>
        <div
          className="card cardBorder ml-3"
          onClick={() => handlePlayPause('3', 'audioPlay2')}
        >
          <div className="card-body bodyOfCard d-flex justify-content-center">
            <div className="card-title txt">
              <span>CONGRATS</span>
              <span>
                {' '}
                <img
                  className="card-img-top ml-3 mr-2 playPause"
                  src={selectedSongId == 3 && pausetoggle ? pause : play}
                  alt="Card image cap"
                />
                <audio
                  id="audioPlay2"
                  src={process.env.REACT_APP_WEB_ASSETS + '/CONGRATS.mp3'}
                  type="audio"
                  style={{ display: 'none' }}
                />
              </span>
              <span>Play JiveBird</span>
            </div>
          </div>
          <div className="d-flex justify-content-center mb-3">
            <p className="card-text">
              <img
                className="card-img-center Imgcard"
                src={welldone}
                alt="Card image cap"
              />
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ExampleTemp;
