import React from 'react';
import ExampleTemp from './components';

const Example = () => {
  return (
    <div>
      <ExampleTemp />
    </div>
  );
};

export default Example;
