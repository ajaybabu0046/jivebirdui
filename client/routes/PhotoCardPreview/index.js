import React from 'react';
import PhotoPreviewTemp from './component';

const PhotoPreview = (props) => {
  return <PhotoPreviewTemp {...props} />;
};

export default PhotoPreview;
