import React, { useState } from 'react';
import './style.css';
import { EditTextarea } from 'react-edit-text';
import 'react-edit-text/dist/index.css';
import { changeTab } from '../../CreateJiveBird/actions';
import { saveMessage, selectPhotoCard } from '../../EcardWithPhoto/actions';
import { firebaseAnalytics } from '../../../firebaseConfig';
import { logEvent } from 'firebase/analytics';

const PhotoPreviewTemp = (props) => {
  const { dispatch, photoCardData } = props;
  const { cardUrl, photoUrl, message } = photoCardData;
  const [text, setText] = useState(message);

  const handleText = (e) => {
    if (text.length > 100) {
      let trimmed = e.substring(0, 100);
      setText(trimmed);
    } else setText(e);
  };

  const handleSave = ({ value }) => {
    if (value.length > 100) {
      let trimmed = value.substring(0, 100);
      setText(trimmed);
      dispatch(saveMessage({ msg: trimmed }));
    } else {
      setText(value);
      dispatch(saveMessage({ msg: value }));
    }
  };

  return (
    <>
      <div className="container-fluid customContainer">
        <div className="d-flex eCardRow btns-div mt-3 sticky">
          <h1 className="mainHeading head1">
            Photo Card <span style={{ color: '#734385' }}>Preview</span>
          </h1>
          <div className="ml-auto head2">
            <button
              type="button"
              name="next"
              className="btn app-btn ml-4 mt-2"
              onClick={() => {
                logEvent(firebaseAnalytics, 'PhotoCard_Accepted');
                dispatch(changeTab({ tabId: 4 }));
              }}
            >
              Next
            </button>
            <button
              type="button"
              name="reselect"
              className="btn app-btn mt-2"
              onClick={() =>
                dispatch(selectPhotoCard({ id: '', photoUrl: '' }))
              }
            >
              Re-Select
            </button>
          </div>
        </div>
        <div
          className="d-flex align-content-center justify-content-center my-3"
          style={{ position: 'relative' }}
        >
          <img src={cardUrl} className="photoFrameWithMessage" />
          <img src={photoUrl} className="photograph" />
          <EditTextarea
            name="textbox1"
            className="photoCardMessage"
            placeholder="Click here to enter your&#10;personal greeting"
            value={text}
            onChange={(e) => handleText(e)}
            onSave={handleSave}
          />
        </div>
      </div>
    </>
  );
};

export default PhotoPreviewTemp;
