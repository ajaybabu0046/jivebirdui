import React, { useState, useEffect } from 'react';
import SelectEcardTemp from './component';
import CustomiseEcard from '../CustomiseEcard';
import { func } from 'prop-types';
import { connect } from 'react-redux';
import { useInjectReducer } from '../../reducers/injectReducer';
import { useInjectSaga } from '../../saga/injectSaga';
import { getCardCategories } from './actions';
import { getCardCategoriesReducer } from './reducer';
import getCardCategoriesSaga from './saga';

const SelectEcard = (props) => {
  const { dispatch, cardCategoryData } = props;
  const { cardSelected } = cardCategoryData;

  useInjectReducer({
    key: 'getCardCategoriesReducer',
    reducer: getCardCategoriesReducer,
  });
  useInjectSaga({ key: 'getCardCategoriesSaga', saga: getCardCategoriesSaga });

  useEffect(() => {
    dispatch(getCardCategories({}));
  }, []);

  return (
    <>
      {cardSelected === true ? (
        <CustomiseEcard {...props} />
      ) : (
        <SelectEcardTemp {...props} />
      )}
    </>
  );
};

SelectEcard.propTypes = {
  dispatch: func.isRequired,
};

SelectEcard.defaultProps = {
  cardCategoryData: {
    cardSelected: false,
  },
};

function mapStateToProps(props) {
  return {
    cardCategoryData: props.getCardCategoriesReducer,
  };
}

export default connect(mapStateToProps)(SelectEcard);
