import { createActionTypes } from '@/lib/utils/helper';

export const getCardCategoriesActions = createActionTypes('getCardCategory', [
  'GET_CARD_CATEGORY',
  'GET_CARD_CATEGORY_SUCCESS',
  'GET_CARD_CATEGORY_FAIL',
  'SELECT_CARD',
  'SAVE_MESSAGE',
  'SKIPPED',
]);

export const skipCard = () => ({
  type: getCardCategoriesActions.SKIPPED,
});

export const getCardCategories = (obj) => ({
  type: getCardCategoriesActions.GET_CARD_CATEGORY,
  payload: {
    ...obj,
  },
});

export const getCardCategoriesSuccess = (obj) => ({
  type: getCardCategoriesActions.GET_CARD_CATEGORY_SUCCESS,
  payload: {
    ...obj,
  },
});

export const getCardCategoriesFailure = (obj) => ({
  type: getCardCategoriesActions.GET_CARD_CATEGORY_FAIL,
  payload: {
    ...obj,
  },
});

export const selectCard = (obj) => ({
  type: getCardCategoriesActions.SELECT_CARD,
  payload: {
    ...obj,
  },
});

export const saveMessage = (obj) => ({
  type: getCardCategoriesActions.SAVE_MESSAGE,
  payload: {
    ...obj,
  },
});
