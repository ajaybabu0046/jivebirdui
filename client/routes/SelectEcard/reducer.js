import { getCardCategoriesActions } from './actions';

export const initialState = {
  message: '',
  getCardCategory: [],
  success: 0,
  loading: false,
  cardSelected: false,
  cardId: '',
  cardUrl: '',
  isSkipped: false,
};

export const getCardCategoriesReducer = (state = initialState, action) => {
  const {
    GET_CARD_CATEGORY,
    GET_CARD_CATEGORY_SUCCESS,
    GET_CARD_CATEGORY_FAIL,
    SELECT_CARD,
    SAVE_MESSAGE,
    SKIPPED,
  } = getCardCategoriesActions;

  switch (action.type) {
    case SKIPPED:
      return {
        ...state,
        isSkipped: true,
      };
    case GET_CARD_CATEGORY:
      return {
        ...state,
        loading: true,
        isSkipped: false,
      };
    case GET_CARD_CATEGORY_SUCCESS:
      let sortedByPopularity = action.payload.Data.sort((a, b) => {
        var p1 = a.popularity,
          p2 = b.popularity;

        if (p1 < p2) return 1;
        if (p1 > p2) return -1;
        return 0;
      });

      return {
        ...state,
        loading: false,
        getCardCategory: sortedByPopularity,
        success: 1,
        isSkipped: false,
      };
    case GET_CARD_CATEGORY_FAIL:
      return {
        ...state,
        loading: false,
        getCardCategory: action.payload.Data,
        success: 0,
        isSkipped: false,
      };
    case SELECT_CARD:
      if (action.payload.id === '') {
        return {
          ...state,
          cardId: '',
          cardUrl: '',
          message: '',
          cardSelected: false,
          isSkipped: false,
        };
      } else {
        return {
          ...state,
          cardId: action.payload.id,
          cardUrl: `${process.env.BASE_API_PATH}/cards/download/${action.payload.id}`,
          cardSelected: true,
          isSkipped: false,
        };
      }

    case SAVE_MESSAGE:
      return {
        ...state,
        message: action.payload.msg,
        isSkipped: false,
      };

    default:
      return state;
  }
};
