/**
 * Gets the repositories of the user from Github
 */
import { call, put, takeLatest } from 'redux-saga/effects';

import { apiResource } from '@/lib/utils/api';

import {
  getCardCategoriesActions,
  getCardCategoriesSuccess,
  getCardCategoriesFailure,
} from './actions';

function* getCardCategories(dObj) {
  try {
    const url = '/card_categories';
    const res = yield call(apiResource.get, url);

    if (res.length > 0) {
      yield put(
        getCardCategoriesSuccess({
          Data: res,
        }),
      );
    } else {
      yield put(
        getCardCategoriesFailure({
          Data: res,
        }),
      );
    }
  } catch (e) {
    yield put(
      getCardCategoriesFailure({
        message: 'Something went wrong',
      }),
    );
  }
}

function* getCardCategoriesSaga() {
  yield takeLatest(
    getCardCategoriesActions.GET_CARD_CATEGORY,
    getCardCategories,
  );
}

export default getCardCategoriesSaga;
