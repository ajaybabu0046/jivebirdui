import React, { useEffect, useState } from 'react';
import './style.css';
import Loader from 'react-loader-spinner';
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';
import { selectCard } from '../actions';
import { changeScreen } from '../../ChooseEcard/actions';
import { firebaseAnalytics } from '../../../firebaseConfig';
import { logEvent } from 'firebase/analytics';
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import Accordion from 'react-accordian';

const responsive = {
  superLargeDesktop: {
    // the naming can be any, depends on you.
    breakpoint: { max: 4000, min: 3000 },
    items: 5,
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 5,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 3,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
  },
};

const cardAccordian = {
  padding: '6px',
  background: 'transparent',
  borderBottom: '1px solid #E96D30',
  padding: '10px',
  color: '#734385',
  fontSize: '1.7rem',
  fontFamily: 'Paytone One',
};

const cardAccordianActive = {
  height: '100%',
  padding: '6px',
  background: 'transparent',
  padding: '10px',
  color: '#734385',
  borderBottom: '1px solid #E96D30',
  fontSize: '1.7rem',
  fontFamily: 'Paytone One',
};

const ActiveIconStyle = {
  color: '#734385',
  fontSize: '20px !important',
};
const notActiveIconStyle = {
  color: '#734385',
  fontSize: '20px !important',
};

const SelectEcardTemp = (props) => {
  const { cardCategoryData, dispatch } = props;
  const [responseData, setResponseData] = useState();
  const [loadingAllData, setLoadingAllData] = useState(true);
  const [clicked, setClicked] = useState('0');

  const handleToggle = (index, data) => {
    if (clicked === index) return setClicked('0');

    setClicked(index);
    if (responseData[index].cardList.length === 0) {
      let tempArray = [...responseData];
      fetch(`${process.env.BASE_API_PATH}/card_categories/${data.name}/`, {
        method: 'GET',
      })
        .then((response) => response.json())
        .then((response) => {
          let sortedByPopularity = response.sort((a, b) => {
            var p1 = a.popularity,
              p2 = b.popularity;

            if (p1 < p2) return 1;
            if (p1 > p2) return -1;
            return 0;
          });

          tempArray[index] = {
            ...tempArray[index],
            cardList: sortedByPopularity,
          };
          setResponseData(tempArray);
        });
    }
  };

  useEffect(() => {
    if (
      cardCategoryData &&
      cardCategoryData.getCardCategory &&
      cardCategoryData.getCardCategory.length > 0
    ) {
      let responsArray = [];
      cardCategoryData.getCardCategory.map((data) => {
        let obj = {
          id: data.id,
          name: data.name,
          cardList: [],
        };
        responsArray.push(obj);
      });
      setResponseData(responsArray);

      if (responsArray.length === cardCategoryData.getCardCategory.length) {
        setLoadingAllData(false);
      }
    }
  }, [cardCategoryData]);

  const handleSelect = async (id) => {
    logEvent(firebaseAnalytics, 'Card_ECard_Picked');
    dispatch(selectCard({ id: id }));
  };

  const DownArrow = () => {
    return <i className="fas fa-arrow-down"></i>;
  };

  const RightArrow = () => {
    return <i className="fas fa-arrow-right"></i>;
  };

  return (
    <>
      {loadingAllData === false ? (
        <div className="container-fluid selectECardContainer px-0 py-3">
          <div className="d-flex eCardRow btns-div my-3 sticky">
            <h1 className="mainHeading mr-auto head1">
              Select <span style={{ color: '#734385' }}>eCard</span>
            </h1>

            <button
              type="button"
              name="reselect"
              className="btn app-btn head2"
              onClick={() => {
                dispatch(
                  changeScreen({
                    Id: 0,
                  }),
                );
              }}
            >
              Re-Select
            </button>
          </div>

          {responseData &&
            responseData.length > 0 &&
            responseData.map((data, index) => (
              <Accordion
                active={clicked === index}
                key={index}
                onToggle={() => handleToggle(index, data)}
                header={data.name}
                gap="10px"
                styling={cardAccordian}
                activeStyling={cardAccordianActive}
                activeIcon={<DownArrow />}
                notActiveIcon={<RightArrow />}
                activeIconStyling={ActiveIconStyle}
                notActiveIconStyling={notActiveIconStyle}
              >
                {data.cardList.length === 0 ? (
                  <div className="loaderContainer">
                    <Loader
                      type="Bars"
                      color="#734385"
                      height={200}
                      width={100}
                      radius={30}
                    />
                  </div>
                ) : (
                  <Carousel responsive={responsive}>
                    {data.cardList.map((cards) => (
                      <div
                        className="Cards"
                        key={cards.id}
                        onClick={() => handleSelect(cards.id)}
                      >
                        <img
                          className="ecardFrame"
                          style={{
                            cursor: 'pointer',
                            borderRadius: 10,
                          }}
                          src={`${process.env.BASE_API_PATH}/cards/download_thumbnail/${cards.id}`}
                          alt="eCards"
                        />
                      </div>
                    ))}
                  </Carousel>
                )}
              </Accordion>
            ))}
        </div>
      ) : (
        <div className="loaderContainer">
          <Loader
            type="Bars"
            color="#734385"
            height={100}
            width={100}
            radius={30}
          />
        </div>
      )}
    </>
  );
};

export default SelectEcardTemp;
