import React from 'react';
import { func } from 'prop-types';
import { connect } from 'react-redux';
import { useInjectReducer } from '../../reducers/injectReducer';
import { chooseSongReducer } from '../SongsList/reducer';
import { recipientReducer } from '../Recipient/reducer';
import { getCardCategoriesReducer } from '../SelectEcard/reducer';
import { EgiftReducer } from '../SelectEgift/reducer';
import { getPhotoCardCategoriesReducer } from '../EcardWithPhoto/reducer';
import { sendEcardReducer } from '../SendEcard/reducer';
import { recordingReducer } from '../RecordMessage/reducer';
import { eCardReducer } from '../ChooseEcard/reducer';
import JiveBirdSummaryTemp from './component';

const JiveBirdSummary = (props) => {
  useInjectReducer({
    key: 'chooseSongReducer',
    reducer: chooseSongReducer,
  });
  useInjectReducer({
    key: 'recipientReducer',
    reducer: recipientReducer,
  });
  useInjectReducer({
    key: 'getCardCategoriesReducer',
    reducer: getCardCategoriesReducer,
  });
  useInjectReducer({
    key: 'EgiftReducer',
    reducer: EgiftReducer,
  });

  useInjectReducer({
    key: 'getPhotoCardCategoriesReducer',
    reducer: getPhotoCardCategoriesReducer,
  });
  useInjectReducer({
    key: 'sendEcardReducer',
    reducer: sendEcardReducer,
  });
  useInjectReducer({
    key: 'recordingReducer',
    reducer: recordingReducer,
  });
  useInjectReducer({
    key: 'eCardReducer',
    reducer: eCardReducer,
  });

  return (
    <>
      <JiveBirdSummaryTemp {...props} />
    </>
  );
};

JiveBirdSummary.propTypes = {
  dispatch: func.isRequired,
};

JiveBirdSummary.defaultProps = {
  songUrl: '',
  recipient: {
    name: '',
    email: '',
    phoneNo: '',
  },
  chooseEcard: {
    cardSkipped: false,
    typeSelected: 0,
  },
  eGift: {},
  photoCard: {},
  recAudio: {},
};

function mapStateToProps(props) {
  return {
    songUrl: props.chooseSongReducer,
    recipient: props.recipientReducer,
    chooseEcard: props.eCardReducer,
    card: props.getCardCategoriesReducer,
    eGift: props.EgiftReducer,
    photoCard: props.getPhotoCardCategoriesReducer,
    scheduledTime: props.sendEcardReducer,
    recAudio: props.recordingReducer,
  };
}

export default connect(mapStateToProps)(JiveBirdSummary);
