import React, { useState, useEffect, useRef } from 'react';
import './style.css';
import profile from '@assets/profile.png';
import play from '@assets/play.png';
import pause from '@assets/pause.png';
import edit from '@assets/rename.png';
import { changeTab } from '../../CreateJiveBird/actions';
import CongratsImg from '@assets/Group.png';
import swal from '@sweetalert/with-react';
import Moment from 'moment';
import axios from 'axios';
import Modal from 'react-bootstrap/Modal';
import {
  FacebookShareButton,
  TwitterShareButton,
  EmailShareButton,
  LinkedinShareButton,
  WhatsappShareButton,
  FacebookIcon,
  TwitterIcon,
  EmailIcon,
  LinkedinIcon,
  WhatsappIcon,
} from 'react-share';
import { firebaseAnalytics } from '../../../firebaseConfig';
import { logEvent } from 'firebase/analytics';

const JiveBirdSummaryTemp = (props) => {
  const {
    songUrl,
    recipient,
    eGift,
    dispatch,
    setCurrentScreen,
    chooseEcard,
    card,
    photoCard,
    scheduledTime,
    JiveBirdReducer,
    recAudio,
  } = props;

  // Initializing the window before to handle IOS devices.
  var windowObjectReference;
  var windowFeatures = 'popup';

  const [pausetoggle, setPauseToggle] = useState(false);
  const [paymentId, setPaymentId] = useState('');
  const [initiatePayment, setInitiatePayment] = useState(false);
  const [statusRes, setStatusRes] = useState('STARTED');
  const [windowObj, setWindowObj] = useState({});
  const [show, setShow] = useState(false);
  const [sending, isSending] = useState(false);
  const [credits, setCredits] = useState(0);
  const [creditLoadeding, setCreditLoadeding] = useState(true);

  useEffect(() => {
    if (localStorage.getItem('authToken')) {
      logEvent(firebaseAnalytics, 'EventBeginCheckout');
      var config = {
        method: 'get',
        url: `${process.env.BASE_API_PATH}/credits/`,
        headers: {
          Authorization: `Token ${localStorage.getItem('authToken')}`,
        },
      };

      axios(config)
        .then((response) => {
          if (response.status === 200) {
            setCredits(response.data.credits);
            setCreditLoadeding(false);
          }
        })
        .catch((error) => {
          console.log('Error Resp', error);
          setCredits(response.data.credits);
          setCreditLoadeding(false);
        });
    }
  }, []);

  const myAudio = useRef(null);
  const timer = useRef(null);

  var recordPath =
    recAudio.wavAudioKey != ''
      ? recAudio.wavAudioKey
      : process.env.REACT_APP_SILENT_AUDIO_PATH;
  var recordingLink =
    recAudio.s3Url != '' ? recAudio.s3Url : process.env.REACT_APP_SILENT_AUDIO;

  useEffect(() => {
    if (paymentId != '') {
      if (statusRes === 'STARTED') {
        timer.current = setInterval(() => getPaymentStatus(), 1000);
      } else if (statusRes == 'CANCELLED') {
        logEvent(firebaseAnalytics, 'Purchase_Cancelled');
        // windowObj.close();
        let b1 = document.getElementById('cancelByCodeButton');
        b1.click();
        clearInterval(timer.current);
        setPaymentId('');
        isSending(false);
        swal(
          <div>
            <p>JiveBird not sent, payment cancelled.</p>
          </div>,
          {
            icon: 'info',
            closeOnEsc: false,
            closeOnClickOutside: false,
          },
        );
      } else if (statusRes == 'REFUNDED') {
        clearInterval(timer.current);
        // windowObj.close();
        let b1 = document.getElementById('cancelByCodeButton');
        b1.click();
        setPaymentId('');
        isSending(false);
        swal(
          <div>
            <p>JiveBird not sent, your payment is refunded.</p>
          </div>,
          {
            icon: 'info',
            closeOnEsc: false,
            closeOnClickOutside: false,
          },
        );
      } else {
        let b1 = document.getElementById('cancelByCodeButton');
        b1.click();
        clearInterval(timer.current);
        isSending(false);
        setPaymentId('');
        swal(
          <div>
            <img src={CongratsImg} className="congrats mb-3" />
            <div>
              <h5>JiveBird Sent</h5>
            </div>
          </div>,
          {
            closeOnEsc: false,
            closeOnClickOutside: false,
          },
        ).then((willSuccess) => {
          if (willSuccess) {
            window.location.reload();
          }
        });
      }
    }
  }, [paymentId, statusRes]);

  const handlePlay = () => {
    if (songUrl.songSelected === false) return;
    myAudio.volume = 1.0;
    var audio2 = document.getElementById('beep2');
    audio2.volume = 1.0;

    setPauseToggle(!pausetoggle);
    if (pausetoggle === false) {
      if (myAudio.current !== null) {
        myAudio.current.play();
        setPauseToggle(true);
      }
    } else {
      if (myAudio.current !== null) {
        myAudio.current.pause();
        myAudio.current.currentTime = 0;
        setPauseToggle(false);
        audio2.pause();
      }
    }
    myAudio.current.onended = function () {
      audio2.play();
      return (audio2.onended = function () {
        setPauseToggle(false);
      });
    };
  };

  const sendJive = () => {
    isSending(true);
    logEvent(firebaseAnalytics, 'SendJB_Clicked');
    setStatusRes('STARTED');

    let errorForm = false;
    let swalMsg = '';

    if (songUrl.selectedId === '') {
      logEvent(firebaseAnalytics, 'CheckoutPage_IncompleteJB');
      errorForm = true;
      swalMsg =
        'Please pick a song, record your message, decide on a card and decide on a gift.';
      swal(
        <div>
          <p>{swalMsg}</p>
        </div>,
        {
          icon: 'info',
          closeOnEsc: false,
          closeOnClickOutside: false,
        },
      );
      isSending(false);
      return;
    }
    // console.log("recAudio.isSkipped", recAudio.isSkipped, "recAudio.wavAudioKey", recAudio.wavAudioKey)
    // if(recAudio.isSkipped === false && recAudio.wavAudioKey === '') {
    //   errorForm = true;
    //   swalMsg = 'Please record your message, decide on a card and decide on a gift.'
    //   swal(
    //     <div>
    //       <p>{swalMsg}</p>
    //     </div>,
    //     {
    //       icon: 'info',
    //       closeOnEsc: false,
    //       closeOnClickOutside: false,
    //     },
    //   )
    //   isSending(false);
    //   return;
    // }

    if (chooseEcard.typeSelected === 0 && chooseEcard.cardSkipped === false) {
      logEvent(firebaseAnalytics, 'CheckoutPage_IncompleteJB');
      errorForm = true;
      swalMsg = 'Please decide on a card and decide on a gift.';
      swal(
        <div>
          <p>{swalMsg}</p>
        </div>,
        {
          icon: 'info',
          closeOnEsc: false,
          closeOnClickOutside: false,
        },
      );
      isSending(false);
      return;
    }

    if (errorForm === true) {
      isSending(false);
      return;
    } else {
      var SDate = scheduledTime.types == 'schedule' && scheduledTime.date.ts;
      var STime = scheduledTime.types == 'schedule' && scheduledTime.time;
      var formateDate = Moment(SDate).format('yyyy-MM-DD');
      var ScheduleDT = formateDate + ' ' + STime;

      var formData = new FormData();
      formData.append('song_id', songUrl.selectedId);
      formData.append('receipt_email', recipient.email);
      formData.append('receipt_phone', recipient.phoneNo);
      formData.append('receipt_name', recipient.name);

      if (scheduledTime.types === 'schedule') {
        formData.append('delivery_date', ScheduleDT);
      }

      formData.append('recording', recordPath);
      if (card && card.cardSelected === true) {
        formData.append('card_id', card.cardId);
        formData.append('message', card.message);
      }

      if (photoCard && photoCard.cardSelected === true) {
        formData.append('photo', photoCard.photoLocation);
        formData.append('photocard_id', photoCard.cardId);
        formData.append('message', photoCard.message);
      }

      formData.append('is_web', 1);

      if (eGift && eGift.selectedEgift && eGift.selectedEgift.id) {
        formData.append('vendor_code', eGift.selectedEgift.vendorCode);
        formData.append('gift_price', eGift.selectedEgiftValue);

        getPaymentLink(formData);
      } else {
        if (credits <= 0) {
          swal(
            <div>
              <p>
                You don't have enough credits. Please make payment to send your
                JiveBird.
              </p>
            </div>,
            {
              icon: 'info',
              closeOnEsc: false,
              closeOnClickOutside: false,
            },
          ).then((willSuccess) => {
            if (willSuccess) {
              logEvent(firebaseAnalytics, 'EventPurchase');
              getPaymentLink(formData);
            }
          });
        } else {
          sendJB(formData);
        }
      }
    }
  };

  const getPaymentLink = (formData) => {
    var config = {
      method: 'post',
      url: `${process.env.BASE_API_PATH}/payment/stripe/save/`,
      headers: {
        Authorization:
          localStorage.getItem('authToken') != undefined
            ? `Token ${localStorage.getItem('authToken')}`
            : `Token ${JiveBirdReducer.TokenId}`,
        'Content-Type': 'multipart/form-data',
        // 'User-Agent': 'jivebird-web',
      },
      data: formData,
    };
    axios(config)
      .then((response) => {
        setInitiatePayment(true);
        setShow(true);
        setPaymentId(response.data.client_secret);
        setStatusRes('STARTED');
      })
      .catch((response) => {
        console.log('Error Resp Payment', response);
      });
  };

  const sendJB = (formData) => {
    var config = {
      method: 'post',
      url: `${process.env.BASE_API_PATH}/jbcards/upload/jb/`,
      headers: {
        Authorization:
          localStorage.getItem('authToken') != undefined
            ? `Token ${localStorage.getItem('authToken')}`
            : `Token ${JiveBirdReducer.TokenId}`,
        'Content-Type': 'multipart/form-data',
        // 'User-Agent': 'jivebird-web',
      },
      data: formData,
    };

    axios(config)
      .then((response) => {
        swal(
          <div>
            <img src={CongratsImg} className="congrats mb-3" />
            <div>
              <h5>JiveBird Sent</h5>
            </div>
            <div>
              <h5>Share with friends.</h5>
            </div>
            <div>
              <EmailShareButton
                subject="JiveBird"
                body="Click to view your JB"
                separator=": "
                url={`${process.env.REACT_APP_SOCIAL_LINK}${
                  response && response.data && response.data.access_code
                }`}
              >
                <EmailIcon />
              </EmailShareButton>
              <FacebookShareButton
                url={`${process.env.REACT_APP_SOCIAL_LINK}${
                  response && response.data && response.data.access_code
                }`}
                hashtag={'#jivebird'}
                className="Demo__some-network__share-button"
              >
                <FacebookIcon />
              </FacebookShareButton>
              <LinkedinShareButton
                url={`${process.env.REACT_APP_SOCIAL_LINK}${
                  response && response.data && response.data.access_code
                }`}
                title="JB SHARE Title"
                summary="View here"
                source="jivebird.com"
              >
                <LinkedinIcon />
              </LinkedinShareButton>
              <TwitterShareButton
                url={`${process.env.REACT_APP_SOCIAL_LINK}${
                  response && response.data && response.data.access_code
                }`}
                title="JiveBird"
                via="jbshare"
                hashtags={['jivebird', 'jbshare']}
              >
                <TwitterIcon />
              </TwitterShareButton>
              <WhatsappShareButton
                url={`${process.env.REACT_APP_SOCIAL_LINK}${
                  response && response.data && response.data.access_code
                }`}
                title="JiveBird"
                separator=": "
              >
                <WhatsappIcon />
              </WhatsappShareButton>
            </div>
          </div>,
          {
            closeOnEsc: false,
            closeOnClickOutside: false,
          },
        ).then((willSuccess) => {
          if (willSuccess) {
            window.location.reload();
          }
        });
      })
      .catch((response) => {
        console.log('Error Resp', response);
      });
  };

  const getPaymentStatus = () => {
    var checkData = new FormData();
    checkData.append('id', paymentId);
    var config = {
      method: 'post',
      url: `${process.env.BASE_API_PATH}/payment/stripe/check/`,
      headers: {
        Authorization:
          localStorage.getItem('authToken') != undefined
            ? `Token ${localStorage.getItem('authToken')}`
            : `Token ${JiveBirdReducer.TokenId}`,
        'Content-Type': 'multipart/form-data',
      },
      data: checkData,
    };
    axios(config)
      .then((response) => {
        setStatusRes(response.data.remittance_status);
      })
      .catch((response) => {});
  };

  const handleCancelPayment = () => {
    var checkData = new FormData();
    checkData.append('id', paymentId);
    checkData.append('client_secret', paymentId);
    var config = {
      method: 'post',
      url: `${process.env.BASE_API_PATH}/payment/stripe/cancel/`,
      headers: {
        'Content-Type': 'multipart/form-data',
      },
      data: checkData,
    };
    axios(config)
      .then((response) => {
        setStatusRes('CANCELLED');
        setPaymentId('');
      })
      .catch((response) => {});
  };

  const continuePayment = () => {
    var urlLink = 'https://www.jivebird.com/your-payment/?id=' + paymentId;
    windowObjectReference = window.open(
      urlLink,
      'Jivebird_Payment',
      windowFeatures,
    );
    setWindowObj(windowObjectReference);
  };

  const cancelPaymentByCode = () => {
    setInitiatePayment(false);
    windowObj.close();
  };

  return (
    <>
      {initiatePayment ? (
        <Modal
          id="fullScreenModalId"
          show={show}
          dialogClassName="fullscreen-modal"
        >
          <Modal.Body className="modalText">
            <h6>
              Your payment request has been initiated. Please click on
              "Continue" button below to be redirected to the payment page.
              Please do not press back button or close this page.
            </h6>
            {/* <i
              className="fa fa-spinner fa-spin fa-2x faClass"
              style={{ marginRight: '5px' }}
            /> */}
            <div className="cancelBtnDiv">
              <button
                type="button"
                id="cancelByCodeButton"
                style={{ display: 'none' }}
                className="btn btnPayment mr-4"
                onClick={() => cancelPaymentByCode()}
              >
                Cancel By Code Button
              </button>
              <button
                type="button"
                id="continueButton"
                className="btn btnPayment mr-4"
                onClick={() => continuePayment()}
              >
                Continue
              </button>
              <button
                type="button"
                id="cancelButton"
                className="btn btnPayment"
                onClick={() => handleCancelPayment()}
              >
                Cancel Payment
              </button>
            </div>
          </Modal.Body>
        </Modal>
      ) : (
        <div className="container-fluid p-0">
          <div className="d-flex eCardRow btns-div mt-3 sticky">
            <h1 className="mainHeading head1">
              {' '}
              JiveBird <span style={{ color: '#734385' }}>Summary</span>
            </h1>
            <div className="ml-auto head2">
              {localStorage.getItem('authToken') != undefined ||
              JiveBirdReducer.TokenId != undefined ? (
                <button
                  type="button"
                  name="send"
                  className="btn app-btn"
                  onClick={() => sendJive()}
                  disabled={creditLoadeding || sending}
                >
                  {eGift && eGift.selectedEgift && eGift.selectedEgift.id
                    ? 'Next'
                    : 'Send'}
                </button>
              ) : (
                <button
                  type="button"
                  name="next"
                  className="btn app-btn"
                  onClick={() => dispatch(changeTab({ tabId: 6 }))}
                >
                  Next
                </button>
              )}
            </div>
          </div>
          <div className="row">
            <div className="col-lg-7 col-md-12 mt-3 leftsection">
              <div className="d-flex mt-5">
                <img src={profile} />

                <div className="fontsizing ml-3">
                  <h2 className="changecolor ">Recipient</h2>

                  <h2>{recipient.name} </h2>
                </div>

                <button
                  type="button"
                  className="btn EditBtn ml-5 d-none d-sm-block"
                  onClick={() => setCurrentScreen(1)}
                >
                  Edit
                </button>
              </div>
              <div className="d-block d-sm-none">
                <button
                  type="button"
                  className="btn EditBtn1 ml-5"
                  onClick={() => setCurrentScreen(1)}
                >
                  Edit
                </button>
              </div>
              <div className="d-flex mt-3">
                <img
                  className="imgplay"
                  src={pausetoggle ? pause : play}
                  onClick={() => handlePlay()}
                />
                <audio
                  id="audioPlay"
                  ref={myAudio}
                  type="audio"
                  style={{ display: 'none' }}
                >
                  <source src={recordingLink} />
                  Your browser does not support the audio element.
                </audio>
                <audio id="beep2" controls style={{ display: 'none' }}>
                  <source src={songUrl.song} />
                  Your browser does not support the audio element.
                </audio>
                <div className="fontsizing ml-3">
                  <h2 className="changecolor">JiveBird</h2>
                  <h2>Play JiveBird (Message & Music)</h2>
                </div>
              </div>
              <div className="resizing">
                <button
                  type="button"
                  className="btn ReBtn mt-2 ml-5"
                  onClick={() => dispatch(changeTab({ tabId: 1 }))}
                >
                  Re-select Music
                </button>
                <button
                  type="button"
                  className="btn mt-2 ReBtn1 ml-5"
                  onClick={() => dispatch(changeTab({ tabId: 2 }))}
                >
                  Re-Record Message
                </button>
              </div>
            </div>
            <div className="col-lg-5 col-md-12 col-12 changecolor fontsizing mt-5">
              <h2 className="">eCard & eGift</h2>
              <div className="mt-3 cardgift">
                {/* E-Card */}
                {card && card.cardSelected === true ? (
                  <>
                    <div className="leftoverlay img-fluid">
                      <img className="ecardimg mr-4" src={card.cardUrl} />
                      <img
                        className="editimgleft"
                        src={edit}
                        onClick={() => dispatch(changeTab({ tabId: 3 }))}
                      />
                      <h6 className="cardText">{card.message}</h6>
                    </div>
                  </>
                ) : (
                  ''
                )}

                {/* E-Card With Photo */}
                {photoCard && photoCard.cardSelected === true ? (
                  <div className="leftoverlay img-fluid">
                    <img className="ecardimg mr-4" src={photoCard.cardUrl} />
                    <img
                      className="ecardimgInside mr-4"
                      src={photoCard.photoUrl}
                    />
                    <img
                      className="editimgleft"
                      src={edit}
                      onClick={() => dispatch(changeTab({ tabId: 3 }))}
                    />
                    <h6 className="cardText">{photoCard.message}</h6>
                  </div>
                ) : (
                  ''
                )}

                {eGift &&
                eGift.selectedEgift &&
                eGift.selectedEgift.hasOwnProperty('vendorLogo') ? (
                  <div className="rightoverlay img-fluid">
                    <img
                      className="egiftimg mt-3"
                      src={eGift.selectedEgift.vendorLogo}
                    />

                    <img
                      className="editimgright"
                      src={edit}
                      onClick={() => dispatch(changeTab({ tabId: 4 }))}
                    />
                  </div>
                ) : (
                  ''
                )}
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default JiveBirdSummaryTemp;
