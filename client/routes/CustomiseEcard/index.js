import React from 'react';
import CustomiseEcardTemp from './component';

const CustomiseEcard = (props) => {
  return <CustomiseEcardTemp {...props} />;
};

export default CustomiseEcard;
