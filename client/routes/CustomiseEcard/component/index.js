import React, { useState } from 'react';
import './style.css';
import { changeTab } from '../../CreateJiveBird/actions';
import { EditTextarea } from 'react-edit-text';
import 'react-edit-text/dist/index.css';
import styled from 'styled-components';
import { Rnd } from 'react-rnd';
import { saveMessage, selectCard } from '../../SelectEcard/actions';
import TextMessageEditor from './TextMessageEditor';
import SmileySelector from './SmileySelector';

const CustomiseEcardTemp = (props) => {
  const { dispatch, cardCategoryData } = props;
  const { message, cardUrl } = cardCategoryData;

  const [text, setText] = useState(message);
  const [selectedImg, setSelected] = useState(0);
  const [currentEditor, setCurrentEditor] = useState(0);
  const [emojisArr, setEmojisArr] = useState([]);
  const [imgArr, setArrr] = useState([]);

  const StyledRnd = styled(Rnd)`
    position: relative;
  `;

  const Container = styled.div`
    background-image: url(${cardUrl});
    background-size: 100% 100%;
  `;

  const handleText = (e) => {
    if (text.length > 100) {
      let trimmed = e.substring(0, 100);
      setText(trimmed);
    } else setText(e);
  };

  const handleGallery = () => {
    setCurrentEditor(1);
    let gallery = document.getElementById('gallery');
    gallery.click();
  };

  const handleFile = (event) => {
    let file = URL.createObjectURL(event.target.files[0]);
    let obj = {
      id: imgArr.length + 1,
      src: file,
      position: {
        x: 10,
        y: 10,
        width: 110,
        height: 120,
        rotation: 0,
      },
    };

    setArrr([...imgArr, obj]);
  };

  function onResize(event, direction, ref, delta, id) {
    // const { width, height } = ref.style;
    // let current = imgArr.findIndex(elm => elm.id === id);
    // let tempArr = [...imgArr];
    // tempArr[current]= {...tempArr[current], position: {
    //     ...tempArr[current].position,
    //     width:width,
    //     height:height
    // }}
    // setArrr(tempArr);
  }

  function onDragStop(e, d) {
    const { x, y } = d;

    let current = imgArr.findIndex((elm) => elm.id === selectedImg);
    let tempArr = [...imgArr];
    tempArr[current] = {
      ...tempArr[current],
      position: {
        ...tempArr[current].position,
        x: x,
        y: y,
      },
    };

    setArrr(tempArr);
  }

  const onEmojiClick = (event, emojiObject) => {
    let obj = {
      id: emojiObject.id,
      src: emojiObject.url,
      position: {
        x: 10,
        y: 10,
        width: 30,
        height: 30,
        rotation: 0,
      },
    };

    setEmojisArr([...emojisArr, obj]);
  };

  const handleSave = ({ value }) => {
    if (value.length > 100) {
      let trimmed = value.substring(0, 100);
      setText(trimmed);
      dispatch(saveMessage({ msg: trimmed }));
    } else {
      setText(value);
      dispatch(saveMessage({ msg: value }));
    }
  };

  return (
    <>
      <div className="container-fluid customContainer">
        <div className="d-flex eCardRow btns-div mt-3 sticky">
          <h1 className="mainHeading head1">
            Customise <span style={{ color: '#734385' }}>eCard</span>
          </h1>
          <div className="ml-auto head2">
            <button
              type="button"
              name="next"
              className="btn app-btn ml-4 mt-2"
              onClick={() => {
                dispatch(changeTab({ tabId: 4 }));
              }}
            >
              Next
            </button>
            <button
              type="button"
              name="reselect"
              className="btn app-btn mt-2"
              onClick={() => {
                dispatch(
                  selectCard({
                    id: '',
                  }),
                );
              }}
            >
              Re-Select
            </button>
          </div>
        </div>
        <div className="d-flex flex-column justify-content-center editor-container">
          <div className="editor-block">
            <i
              className="fas fa-camera-retro editor-icon"
              onClick={() => handleGallery()}
            ></i>
          </div>
          <input
            id="gallery"
            className="d-none"
            type="file"
            onChange={(e) => handleFile(e)}
          />
          <div className="editor-block" onClick={() => setCurrentEditor(2)}>
            <i className="fas fa-text-height editor-icon"></i>
          </div>
          <div className="editor-block" onClick={() => setCurrentEditor(3)}>
            <i className="fas fa-smile editor-icon"></i>
          </div>
          <div className="editor-elements">
            {currentEditor === 2 && <TextMessageEditor />}
            {currentEditor === 3 && <SmileySelector />}
          </div>
        </div>
        {/* <div
          className="d-flex align-content-center justify-content-center my-3"
          style={{ position: 'relative' }}>
          <img src={cardUrl} className="eCardWithMessage" />
          <EditTextarea
            name="textbox1"
            className="eCardMessage"
            placeholder="Click here to enter your&#10;personal greeting"
            value={text}
            onChange={(e) => handleText(e)}
            onSave={handleSave}
          />
        </div> */}
        <Container className="eCardWithMessage">
          {imgArr.map((imgs) => (
            <StyledRnd
              key={imgs.id}
              className="cardImage"
              default={imgs.position}
              onResize={(event, direction, ref, delta) =>
                onResize(event, direction, ref, delta, imgs.id)
              }
              onDragStop={onDragStop}
              bounds="parent"
              // lockAspectRatio={true}
            >
              <div
                key={imgs.id}
                style={{
                  width: '100%',
                  height: '100%',
                  backgroundImage: `url(${imgs.src})`,
                  backgroundSize: '100% 100%',
                }}
                onMouseEnter={() => setSelected(imgs.id)}
                onMouseLeave={() => setSelected(0)}
              ></div>
            </StyledRnd>
          ))}
        </Container>
      </div>
    </>
  );
};

export default CustomiseEcardTemp;
