import React from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

export default function ReactSldier({ dataset, handleSelect }) {
  let settings = {
    // className: "center",
    // centerMode: true,
    infinite: true,
    dots: false,
    centerPadding: '60px',
    slidesToShow: dataset.length > 5 ? 5 : dataset.length,
    speed: 500,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: dataset.length > 3 ? 3 : dataset.length,
          slidesToScroll: 1,
          infinite: true,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: dataset.length > 3 ? 3 : dataset.length,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  return (
    <Slider {...settings}>
      {dataset.map((cards) => (
        <div key={cards.id} onClick={() => handleSelect(cards.id)}>
          <img
            className="ecardFrame"
            style={{
              cursor: 'pointer',
              borderRadius: 10,
            }}
            src={`${process.env.BASE_API_PATH}/cards/download/${cards.id}`}
            alt="eCards"
          />
        </div>
      ))}
    </Slider>
  );
}
