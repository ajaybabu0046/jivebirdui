import React from 'react';
import PropTypes from 'prop-types';
import './style.css';
import { uploadFile } from 'react-s3';
import { selectPhotoCard } from '../../routes/EcardWithPhoto/actions';

const s3config = {
  bucketName: 'jivebirdbucketprod',
  region: 'eu-west-2',
  accessKeyId: 'AKIAWRPJOMVGRRNNDFWJ',
  secretAccessKey: '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',
  dirName: 'images/photocards',
};

export const ImagePreview = ({
  dataUri,
  isFullscreen,
  setDataUri,
  setSelected,
  dispatch,
  photoCardId,
}) => {
  let classNameFullscreen = isFullscreen ? 'demo-image-preview-fullscreen' : '';

  const handlePhotoImageUpload = (data) => {
    // const url = data;

    fetch(data)
      .then((res) => res.blob())
      .then((blob) => {
        var file = new File([blob], `${Date.now().toString()}_selfie.jpg`, {
          type: 'image/png',
        });

        if (file) {
          uploadFile(file, s3config).then((doc) => {
            localStorage.setItem('SelectedImageUrl', doc.location);
            localStorage.setItem('uploadImagePath', doc.key);
            dispatch(
              selectPhotoCard({ id: photoCardId, photoUrl: doc.location }),
            );
            setDataUri('');
            setSelected(true);
          });
        }
      });
  };

  return (
    <div className={'demo-image-preview ' + classNameFullscreen}>
      <img src={dataUri} />
      <div className="d-block mt-4">
        <i
          role="button"
          onClick={() => {
            setDataUri('');
          }}
          className="fas fa-times-circle fa-2x mr-5"
        ></i>
        <i
          role="button"
          onClick={() => handlePhotoImageUpload(dataUri)}
          className="fas fa-check-circle fa-2x"
        ></i>
        {/* <button type="button" className="btn btn-primary">Retake</button> */}
      </div>
    </div>
  );
};

ImagePreview.propTypes = {
  dataUri: PropTypes.string,
  isFullscreen: PropTypes.bool,
};

export default ImagePreview;
