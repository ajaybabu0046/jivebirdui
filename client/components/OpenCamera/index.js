import React, { useState } from 'react';
import Camera from 'react-html5-camera-photo';
import 'react-html5-camera-photo/build/css/index.css';
import swal from '@sweetalert/with-react';

import ImagePreview from './ImagePreview'; // source code : ./src/demo/AppWithImagePreview/ImagePreview

const CameraOpen = ({
  setModal2Visible,
  setOpen,
  setSelected,
  dispatch,
  photoCardId,
  setImg,
  setCropModalShow,
}) => {
  const [dataUri, setDataUri] = useState('');
  const [cameraWorking, setCameraWorking] = useState(true);

  function handleTakePhotoAnimationDone(dataUri) {
    setCropModalShow(true);
    setImg(dataUri);
    setDataUri(dataUri);
  }
  function handleCameraStart(stream) {
    console.log('Camera is starting...');
  }

  const isFullscreen = false;

  const handleCameraError = (error) => {
    swal(
      <div>
        <h5>Requested device not found</h5>
      </div>,
      {
        icon: 'info',
        dangerMode: true,
        closeOnEsc: false,
        closeOnClickOutside: false,
      },
    ).then((success) => {
      setModal2Visible(false);
      setOpen(false);
    });
  };

  return (
    <>
      {dataUri ? (
        <ImagePreview
          dataUri={dataUri}
          isFullscreen={isFullscreen}
          setDataUri={setDataUri}
          setSelected={setSelected}
          dispatch={dispatch}
          photoCardId={photoCardId}
        />
      ) : (
        <Camera
          onTakePhotoAnimationDone={handleTakePhotoAnimationDone}
          isFullscreen={isFullscreen}
          onCameraStart={(stream) => {
            handleCameraStart(stream);
          }}
          onCameraError={(error) => {
            setCameraWorking(false);
            handleCameraError(error);
          }}
        />
      )}
      {cameraWorking ? (
        <div className="d-flex justify-content-center mt-4">
          <button
            type="button"
            className="btn btnCancelCamera"
            onClick={() => {
              setModal2Visible(false);
              setOpen(false);
            }}
          >
            Cancel
          </button>
        </div>
      ) : (
        ''
      )}
    </>
  );
};

export default CameraOpen;
