import React from 'react';
import './style.css';

const SearchBox = ({ handleSearch }) => {
  return (
    <div className="input-group searchdiv col-lg-4 col-md-6 col-sm-12">
      <input
        type="text"
        name="search"
        className="form-control"
        tabIndex="1"
        placeholder="Search title, genre, occasion etc."
        onChange={(e) => handleSearch(e)}
      />
      <div className="input-group-append" style={{ height: 50 }}>
        <span className="input-group-text" role="button">
          <i className="fas fa-search fa-lg" style={{ cursor: 'pointer' }}></i>
        </span>
      </div>
    </div>
  );
};

export default SearchBox;
