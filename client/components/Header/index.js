import React from 'react';
import birdLogo from '@assets/logo.png';
import './style.css';

const Header = (props) => {
  const { commonHeader } = props;
  return (
    <>
      {!commonHeader ? (
        ''
      ) : (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarTogglerDemo01"
            aria-controls="navbarTogglerDemo01"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div
            className="collapse navbar-collapse myNavbar"
            id="navbarTogglerDemo01"
          >
            <a className="navbar-brand ml-5" role="button">
              <img style={{ width: '160px', height: '70px' }} src={birdLogo} />
            </a>
            <div className="form-inline ml-auto mr-5">
              <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
                <li className="nav-item">
                  <a className="nav-link" href="#">
                    <i className="fas fa-home fa-lg"></i>
                  </a>
                  <a className="nav-link" href="#">
                    Home
                  </a>
                </li>

                <i className="fas fa-chevron-right fa-lg bradcrumb"></i>

                <li className="nav-item">
                  <a className="nav-link" href="#">
                    <i className="fas fa-music fa-lg"></i>
                  </a>
                  <a className="nav-link" href="#">
                    Music
                  </a>
                </li>

                <i className="fas fa-chevron-right fa-lg bradcrumb"></i>

                <li className="nav-item">
                  <a className="nav-link" href="#">
                    <i className="fas fa-microphone fa-lg"></i>
                  </a>
                  <a className="nav-link" href="#">
                    Record
                  </a>
                </li>

                <i className="fas fa-chevron-right fa-lg bradcrumb"></i>

                <li className="nav-item">
                  <a className="nav-link" href="#">
                    <i className="far fa-credit-card fa-lg"></i>
                  </a>
                  <a className="nav-link" href="#">
                    Card
                  </a>
                </li>

                <i className="fas fa-chevron-right fa-lg bradcrumb"></i>

                <li className="nav-item">
                  <a className="nav-link" href="#">
                    <i className="fas fa-gift fa-lg"></i>
                  </a>
                  <a className="nav-link" href="#">
                    Gift
                  </a>
                </li>

                <i className="fas fa-chevron-right fa-lg bradcrumb"></i>

                <li className="nav-item">
                  <a className="nav-link" href="#">
                    <i className="fas fa-paper-plane fa-lg"></i>
                  </a>
                  <a className="nav-link" href="#">
                    Send
                  </a>
                </li>

                <li className="nav-item d-flex justify-content-center align-items-center">
                  <a className="nav-link" href="#">
                    <i className="fas fa-ellipsis-v fa-2x"></i>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      )}
    </>
  );
};
export default Header;
