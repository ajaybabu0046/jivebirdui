import React from 'react';
import { shape } from 'prop-types';
import Header from '../Header';

const Layout = (props) => {
  const { children, location } = props;
  const { pathname } = location;

  const checkIfLoginPage = () => {
    if (pathname === '/') {
      return {
        commonHeader: false,
      };
    }
    return {
      commonHeader: true,
    };
  };

  const { commonHeader } = checkIfLoginPage();
  return (
    <>
      {commonHeader && (
        <Header
          commonHeader={commonHeader}
          children={children}
          pathname={pathname}
        />
      )}
      {children}
    </>
  );
};

Layout.propTypes = {
  children: shape({}).isRequired,
  location: shape({}).isRequired,
};
export default Layout;
