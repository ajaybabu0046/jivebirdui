import { createActionTypes } from '@/lib/utils/helper';

export const toggleActions = createActionTypes('toggleAction', [
  'TOGGLE_ACCOUNT',
]);

export function toggleAccount(obj) {
  return {
    type: toggleActions.TOGGLE_ACCOUNT,
    payload: {
      ...obj,
    },
  };
}
