import React, { useState, useEffect } from 'react';
import './style.css';
import { changeTab } from '../../routes/CreateJiveBird/actions';
import { func } from 'prop-types';
import { connect } from 'react-redux';
import { useInjectReducer } from '../../reducers/injectReducer';
import { toggleReducer } from './reducer';
import { toggleAccount } from './action';

const MyAccountToggling = (props) => {
  const { toggleData, dispatch } = props;
  useInjectReducer({
    key: 'toggleReducer',
    reducer: toggleReducer,
  });

  const handlePersonalInfoToggle = (data) => {
    if (data == 'personalInfo') {
      dispatch(toggleAccount({ toggle: true })),
        dispatch(changeTab({ tabId: 12 }));
    } else {
      dispatch(toggleAccount({ toggle: false })),
        dispatch(changeTab({ tabId: 11 }));
    }
  };

  return (
    <div className="container d-flex justify-content-center align-items-center ">
      <div className="row">
        <div className="col myToggal">
          <span
            role="button"
            onClick={() => handlePersonalInfoToggle('history')}
            className={
              toggleData.toggleAccount == false ? 'history' : 'default'
            }
          >
            History
          </span>
          <span
            role="button"
            onClick={() => handlePersonalInfoToggle('personalInfo')}
            className={toggleData.toggleAccount ? 'personalInfo' : 'default'}
          >
            Personal Info
          </span>
        </div>
      </div>
    </div>
  );
};

MyAccountToggling.propTypes = {
  dispatch: func.isRequired,
};

MyAccountToggling.defaultProps = {
  toggleData: {
    toggleAccount: false,
  },
};

function mapStateToProps(props) {
  return {
    toggleData: props.toggleReducer,
  };
}

export default connect(mapStateToProps)(MyAccountToggling);
