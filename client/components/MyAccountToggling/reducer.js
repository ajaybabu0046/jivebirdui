import { toggleActions } from './action';

export const initialState = {
  toggleAccount: false,
};

export const toggleReducer = (state = initialState, action) => {
  const { TOGGLE_ACCOUNT } = toggleActions;

  switch (action.type) {
    case TOGGLE_ACCOUNT:
      return {
        ...state,
        toggleAccount: action.payload.toggle,
      };

    default:
      return state;
  }
};
