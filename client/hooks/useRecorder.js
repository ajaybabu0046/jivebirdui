import { useEffect, useState } from 'react';
import { uploadFile } from 'react-s3';

const s3config = {
  bucketName: 'jivebirdbucketprod',
  region: 'eu-west-2',
  accessKeyId: 'AKIAWRPJOMVGRRNNDFWJ',
  secretAccessKey: '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',
  dirName: 'music/jbs',
};

const useRecorder = () => {
  const [isRecording, setIsRecording] = useState(false);
  const [recorder, setRecorder] = useState(null);
  const [permissionGranted, setPermission] = useState(false);
  const [error, setError] = useState(false);

  useEffect(() => {
    // Lazily obtain recorder first time we're recording.
    if (recorder === null) {
      if (isRecording) {
        requestRecorder().then((response) => {
          if (response.hasOwnProperty('errorStatus')) {
            alert(response.errorMsg.message);
            setError(true);
            setIsRecording(false);
          } else {
            setRecorder(response);
            setPermission(true);
            setError(false);
          }
        });
      }
      return;
    }
  }, [recorder, isRecording]);

  const startRecording = () => {
    setError(false);
    setIsRecording(true);
  };

  const stopRecording = () => {
    setIsRecording(false);
    setRecorder(null);
    setPermission(false);
    setError(false);
  };

  return [
    startRecording,
    isRecording,
    error,
    permissionGranted,
    setIsRecording,
    stopRecording,
  ];
};

async function requestRecorder() {
  try {
    const stream = await navigator.mediaDevices.getUserMedia({ audio: true });
    return new MediaRecorder(stream);
  } catch (error) {
    let errorObj = {
      errorStatus: 0,
      errorMsg: error,
    };
    return errorObj;
  }
}
export default useRecorder;
